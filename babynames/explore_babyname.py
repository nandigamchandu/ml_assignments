"""Exploring babynames dataset."""
import pandas as pd
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt

# %%
# loading all years babynames files
df = pd.DataFrame()
file_path = '~/data/babynames'
for i in range(1880, 2011):
    file_name = 'yob'+str(i)+'.txt'
    file = file_path+'/'+file_name
    df1 = pd.read_csv(file, sep=',', names=['name', 'sex', 'count'])
    df1['year'] = i
    df = pd.concat([df, df1], ignore_index=True)

# %%
# know about no of observations, features and null values.
df.info()

# %%
#  no of unique values present in name, sex, year columns
df[['name', 'sex', 'year']].nunique()

# %%
# compare female births and male births
x = df.groupby('sex')['count'].sum()
plt.ylim(1.56e8, 1.65e8)
sns.barplot(x=x.index, y=x.values)

# %%
# polt births vs years
birth_year = df.groupby(['year'])['count'].sum()
sns.set_style("darkgrid")
birth_year.plot(title='births vs years')
plt.ylabel('Births')

# %%
# get top 5 child births years
birth_year.nlargest(5)

# %%
# male and female birth percentage vs year
male_female_per = df.groupby(['year', 'sex'])['count'].sum() * 100 / birth_year
male_female_per = male_female_per.unstack()
male_female_per.head()

# %%
# plot male and female births vs years
births = df.groupby(['year', 'sex'])['count'].sum().unstack()
sns.set_style("darkgrid")
births.plot(title='female and male births vs years')
plt.ylabel('Births')

# %%
# compare no of more female birth years and more male births years
x = np.where(male_female_per['F'] > male_female_per['M'], "Female", "Male")
x1 = pd.value_counts(x)
plt.ylim(40, 80)
sns.barplot(x=x1.index, y=x1.values)

# %%
# top five more frequent names
df.groupby(['name'])['count'].sum().sort_values(ascending=False).head(5)

# %%
# get top 5 female baby names
df[df['sex'] == 'F'].groupby('name')['count'].sum().nlargest(10)
# get top 5 male baby names
df[df['sex'] == 'M'].groupby('name')['count'].sum().nlargest(10)

# %%
# names given to both male and female
female_names = df[df['sex'] == 'F']['name']
male_names = df[df['sex'] == 'M']['name']
common_names = female_names[female_names.isin(male_names)].unique()
common_names

# %%
# top used name vs year
x = (df[['year', 'name', 'count']].sort_values(['year', 'count'],
                                               ascending=False))
x.groupby('year').head(1).set_index('year')

# %%
# top used male and female name vs year
x = (df[['year', 'name', 'sex', 'count']].sort_values(['year', 'count'],
                                                      ascending=False))
top_names = x.groupby(['year', 'sex']).head(1).set_index(['year', 'sex'])
top_male = top_names.unstack()[('name', 'M')].unique()
top_female = top_names.unstack()[('name', 'F')].unique()

# %%
# chance in frequnce of top male names over years
top_male_names = df[(df['name'].isin(top_male)) & (df['sex'] == 'M')]
(top_male_names.pivot(index='year', columns='name',
                      values='count').fillna(0).plot())

# %%
# chance in frequnce of top female names over years
top_female_names = df[(df['name'].isin(top_female)) & (df['sex'] == 'F')]
(top_female_names.pivot(index='year', columns='name',
                        values='count').fillna(0).plot())

# %%
# names occure only in one year
name = df.groupby('name')['year'].size()
one_time_name = name.index[name.values == 1]
one_time_name = df[df['name'].isin(one_time_name.values)].set_index('name')
one_time_name.head()

# %%
# name occure in one year with high frequence
one_time_name.loc[one_time_name['count'].idxmax()]
