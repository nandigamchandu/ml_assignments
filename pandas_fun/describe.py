"""Pandas describe method."""
import pandas as pd
import numpy as np

# Describing numerical series
s = pd.Series([1, 2, 3])
s.describe()

# Describe a categorical Series
s = pd.Series(['a', 'a', 'b', 'c'])
s.describe()
s = (pd.Series([np.datetime64("2000-01-01"),
                np.datetime64("2010-01-01"),
                np.datetime64("2010-01-01")]))
s.describe()

# By default only numeric fields are returned
df = pd.DataFrame({'object': ['a', 'b', 'c'],
                   'numeric': [1, 2, 3],
                   'categorical': pd.Categorical(['d', 'e', 'f'])})
df.describe()

# describe all columns of a DataFrame regardless of data type
df.describe(include='all')

# including only numeric columns in a DataFrame
df.numeric.describe()
df.describe(include=[np.number])
# including only string columns in dataframe
df.describe(include=[np.object])
# including only catergorical columns
df.describe(include=['category'])

# excluding object columns
df.describe(exclude=[np.object])
