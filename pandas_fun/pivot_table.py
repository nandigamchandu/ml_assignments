"""Practice pandas pivot_table method."""
# %%
import pandas as pd
import numpy as np

# %%
# Example 1
df = pd.DataFrame({"A": ["foo", "foo", "foo", "foo", "foo",
                         "bar", "bar", "bar", "bar"],
                   "B": ["one", "one", "one", "two", "two",
                         "one", "one", "two", "two"],
                   "C": ["small", "large", "large", "small",
                         "small", "large", "small", "small",
                         "large"],
                   "D": [1, 2, 2, 3, 3, 4, 5, 6, 7]})

df.pivot_table(index=["A", "B"], values="D", columns="C")

# %%
#  pivot without aggregation that can handle non-numeric data
table = dict((
    ("Item", ['Item0', 'Item0', 'Item1', 'Item1']),
    ('CType', ['Gold', 'Bronze', 'Gold', 'Silver']),
    ('USD',  ["1",  "2",  "3",  "4"]),
    ('EU',   ['1€', '2€', '3€', '4€'])
))
df = pd.DataFrame(table)

# %%
df.pivot(index='Item', columns="CType", values="USD")

# %%
# pivoting by multiple columns
df.pivot(index="Item", columns="CType")

# %%
# Common mistake in pivoting
# ("Itom0", "glod") has two values 1 and 3
# it will get ValueError: Index contains duplicate entries, cannot reshape
table = dict((
    ("Item", ['Item0', 'Item0', 'Item0', 'Item1']),
    ('CType', ['Gold', 'Bronze', 'Gold', 'Silver']),
    ('USD',  [1,  2,  3,  4]),
    ('EU',   ['1€', '2€', '3€', '4€'])
))
df = pd.DataFrame(table)
p = df.pivot_table(index='Item', columns='CType', values='USD', aggfunc=np.sum)
p
