"""Pandas melt method."""
import pandas as pd

df = pd.DataFrame({'A': list('abc'),
                   'B': [1, 2, 3],
                   'C': [2, 4, 6]})
pd.melt(df, id_vars=['A'], value_vars=['B'])
pd.melt(df, id_vars=['A'], value_vars=['B', 'C'])
# the names of variable and value columns can be customized
(pd.melt(df, id_vars=['A'], value_vars=['B', 'C'],
         var_name='col', value_name='myvalue'))

# if we have multi-index columns
df.columns = [list('ABC'), list('DEF')]
pd.melt(df, col_level=0, id_vars=['A'], value_vars=['B'])
pd.melt(df, id_vars=[('A', 'D')], value_vars=[('B', 'E')])
