"""Pandas index_sort method."""
import pandas as pd
import numpy as np

df = pd.DataFrame({'col1': ['A', 'A', 'B', np.nan, 'D', 'C'],
                   'col2': [2, 1, 9, 8, 7, 4],
                   'col3': [0, 1, 9, 4, 2, 3]})

# sort by col1
df.sort_values(by=['col1'])

# sort by multiple columns
df.sort_values(by=['col1', 'col2'])

# sort Descending
df.sort_values('col1', ascending=False)

# putting NAs first
df.sort_values('col1', ascending=False, na_position='first')
