"""Pandas cut method."""

import pandas as pd
import numpy as np

# the cut function can be useful for going
# from a continous variable to a categorical variable
pd.cut(np.array([0.2, 1.4, 2.5, 6.2, 9.7, 2.1]), 3, retbins=True)

pd.cut(np.array([0.2, 1.4, 2.5, 6.2, 9.7, 2.1]), 3,
       labels=['good', 'medium', 'bad'])

pd.cut(np.ones(5), 4, labels=False)

(pd.cut(np.array([0.2, 1.4, 2.5, 6.2, 9.7, 2.1]), 4, retbins=True,
        right=False))
