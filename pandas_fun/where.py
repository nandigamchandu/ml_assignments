"""Practice pandas where method."""
# %%
import pandas as pd
import numpy as np

# %%
# using where function on series
s = pd.Series(range(5))
s.where(s > 0)

# %%
# using where function on dataframe
df = pd.DataFrame(np.arange(10).reshape(-1, 2), columns=['A', 'B'])
m = df % 3 == 0
df.where(m, -df)

# %%
# df1.where(m, df2) == np.where(m, df1, df2)
df.where(m, -df) == np.where(m, df, -df)

# %%
# mask method
# if cond is False the element is used;
# otherwise the corresponding element from the dataframe other is used
df.where(m, -df) == df.mask(-m, -df)

# %%
df = (pd.DataFrame(
 {'AAA': [4, 5, 6, 7], 'BBB': [10, 20, 30, 40], 'CCC': [100, 50, -30, -50]}))
df

# %%
df_mask = (pd.DataFrame(
  {'AAA': [True] * 4, 'BBB': [False] * 4, 'CCC': [True, False] * 2}))
df.where(df_mask, -1000)

# %%
df['where'] = np.where(df['AAA'] > 5, 'high', 'low')
df

# %%
df['mask'] = np.where(df['AAA'] < 5, 'high', 'low')
df
