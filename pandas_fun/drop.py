"""Pandas drop method."""
import pandas as pd
import numpy as np

df = pd.DataFrame(np.arange(12).reshape(3, 4),
                  columns=['A', 'B', 'C', 'D'])

# Drop columns
df.drop(['B', 'C'], axis=1)
df.drop(columns=['B', 'C'])

# drop a row by index
# Default axis=0
df.drop([0, 1])

# Drop columns and/or rows of multiindex DataFrame
df = pd.DataFrame(
        {'i0': ['lama']*3 + ['cow']*3 + ['falcon']*3,
         'i1': ['speed', 'weigth', 'length']*3,
         'big': [45.0, 200.0, 1.5, 30.0, 250.0, 1.5, 320.0, 1.0, 0.3],
         'small': [30.0, 100.0, 1.0, 20.0, 150.0, 0.8, 250.0, 0.8, 0.2]})
df = df.set_index(['i0', 'i1'])
df.drop(index='cow', columns='small')
df.drop(index='length', level=1)
