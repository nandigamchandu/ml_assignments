"""Pandas replace method."""

import pandas as pd

# %%
# scalar to_reolace and value
s = pd.Series([0, 1, 3, 4])
s.replace(0, 5)

# %%
df = pd.DataFrame({'A': [0, 1, 2, 3, 4],
                   'B': [5, 6, 7, 8, 9],
                   'C': ['a', 'b', 'c', 'd', 'e']})
df.replace(0, 5)
# %%
# List-like 'to_replace'
df.replace([0, 1, 2, 3], 4)
df.replace([0, 1, 2, 3], [4, 3, 2, 1])

# dict-like 'to_replace'
df.replace({0: 10, 1: 100})
df.replace({'A': 0, 'B': 5}, 100)
df.replace({'A': {0: 100, 4: 400}})

# %%
# regular expression to_replace
df = pd.DataFrame({'A': ['bat', 'foo', 'bait'],
                   'B': ['abc', 'bar', 'xyz']})
df.replace(to_replace=r'^ba.$', value='new', regex=True)
df.replace({'A': r'^ba.$'}, {'A': 'new'}, regex=True)
df.replace(regex=r'^ba.$', value='new')
df.replace(regex={r'^ba.$': 'new', 'foo': 'xyz'})
df.replace(regex=[r'^ba.$', 'foo'], value='new')

# %%
# to_replace parameter must match the data type of the value being replace
s = pd.Series([10, 'a', 'a', 'b', 'a'])
s.replace({'a': None})  # equivalent to replace('a', None, method=None)
s.replace('a', None)  # equivalent to replace('a', None, method='pad')
