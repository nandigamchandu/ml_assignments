"""Practice pandas join method."""

import pandas as pd

left = pd.DataFrame({'A': ['A0', 'A1', 'A2'],
                     'B': ['B0', 'B1', 'B2']},
                    index=['K0', 'K1', 'K2'])
right = pd.DataFrame({'C': ['C0', 'C1', 'C2'],
                     'D': ['D0', 'D1', 'D2']},
                     index=['K0', 'K2', 'K3'])
# default how = left
left.join(right)
left.join(right, how='outer')
left.join(right, how='inner')
# join on columns
left = pd.DataFrame({'A': ['A0', 'A1', 'A2', 'A3'],
                     'B': ['B0', 'B1', 'B2', 'B3'],
                     'key': ['K0', 'K1', 'K0', 'K1']})
right = pd.DataFrame({'C': ['C0', 'C1'],
                     'D': ['D0', 'D1']},
                     index=['K0', 'K1'])
left.join(right, on='key')

# to join on multiple columns passed dataframe must have multiple index

left = pd.DataFrame({'A': ['A0', 'A1', 'A2', 'A3'],
                     'B': ['B0', 'B1', 'B2', 'B3'],
                     'key1': ['K0', 'K0', 'K1', 'K2'],
                     'key2': ['K0', 'K1', 'K0', 'K1']})
right = pd.DataFrame({'C': ['C0', 'C1', 'C2', 'C3'],
                      'D': ['D0', 'D1', 'D2', 'D3'],
                      'key1': ['K0', 'K1', 'K2', 'K2'],
                      'key2': ['Ko', 'K0', 'K0', 'K1']})
right = right.set_index(['key1', 'key2'])
left.join(right, on=['key1', 'key2'])

# join on multi-index
left = left.set_index(['key1', 'key2'])
left.join(right, how='inner')

# Overlapping value columns
left = pd.DataFrame({'A': ['A0', 'A1', 'A2', 'A3'],
                     'B': ['B0', 'B1', 'B2', 'B3'],
                     'key1': ['K0', 'K0', 'K1', 'K2'],
                     'key2': ['K0', 'K1', 'K0', 'K1']})
right = pd.DataFrame({'A': ['C0', 'C1', 'C2', 'C3'],
                      'B': ['D0', 'D1', 'D2', 'D3'],
                      'key1': ['K0', 'K1', 'K2', 'K2'],
                      'key2': ['Ko', 'K0', 'K0', 'K1']})
right = right.set_index(['key1', 'key2'])
left.join(right, on=['key1', 'key2'], lsuffix='_l', rsuffix='_r')

# joining multiple dataframe
left = pd.DataFrame({'d1': [1, 2, 3]}, index=['k0', 'k1', 'k2'])
right = pd.DataFrame({'d2': [4, 5, 6]}, index=['k4', 'k1', 'k3'])
right1 = pd.DataFrame({'d3': [7, 8, 9]}, index=['k0', 'k2', 'k4'])
left.join([right, right1])
