"""Practice pandas transform method."""

import pandas as pd
import numpy as np

df = pd.DataFrame(np.random.randn(10, 3), columns=['A', 'B', 'C'],
                  index=pd.date_range('1/1/2000', periods=10))
df.iloc[3:7] = np.nan
df.transform(lambda x: (x - x.mean()) / x.std())


# difference between aggregate, filter and tranform
df = pd.read_csv('~/data/tips.csv')

# 1) aggregate
df.groupby('day').mean()

# 2) pandas filter method
(df.groupby('day').filter(
     lambda x: x['total_bill'].mean() > 20))

#  3) transform
df['day_average'] = (df.groupby('day')['total_bill'].transform(
                        lambda x: x.mean()))
df['scaled_bill'] = (df.groupby('day')['total_bill'].transform(
                        lambda x: x/x.mean()))
