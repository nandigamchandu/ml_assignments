"""Pandas merge method."""
import pandas as pd

left = pd.DataFrame({'key': ['K0', 'K1', 'K2', 'K3'],
                     'A': ['A0', 'A1', 'A2', 'A3'],
                     'B': ['B0', 'B1', 'B2', 'B3']})

right = pd.DataFrame({'key': ['K0', 'K1', 'K2', 'K3'],
                      'C': ['C0', 'C1', 'C2', 'C3'],
                      'D': ['D0', 'D1', 'D2', 'D3']})
# default join is inner
pd.merge(left, right, on='key')

left = pd.DataFrame({'key1': ['K0', 'K0', 'K1', 'K2'],
                     'key2': ['K0', 'K1', 'K0', 'K1'],
                     'A': ['A0', 'A1', 'A2', 'A3'],
                     'B': ['B0', 'B1', 'B2', 'B3']})

right = pd.DataFrame({'key1': ['K0', 'K1', 'K1', 'K2'],
                      'key2': ['K0', 'K0', 'K0', 'K0'],
                      'C': ['C0', 'C1', 'C2', 'C3'],
                      'D': ['D0', 'D1', 'D2', 'D3']})

pd.merge(left, right, on=['key1', 'key2'])
pd.merge(left, right, on=['key1', 'key2'], how='left')
pd.merge(left, right, on=['key1', 'key2'], how='right')
pd.merge(left, right, on=['key1', 'key2'], how='outer')
pd.merge(left, right, left_on='key1', right_on='key2', how='outer')

# duplicate join keys in DataFrame
left = pd.DataFrame({'A': [1, 2], 'B': [1, 2]})
rigth = pd.DataFrame({'A': [4, 5, 6], 'B': [2, 2, 2]})

pd.merge(left, rigth, on='B', how='outer')

pd.merge(left, rigth, on='B', how='outer', validate='one_to_many')
pd.merge(left, rigth, on='B', how='outer', indicator='indicator_column')
