"""Pandas rename."""
import pandas as pd

s = pd.Series([1, 2, 3])
# rename Series
s.rename('my_name')

# function changes labels
s.rename(lambda x: x**2)

# mapping, chnges labels
s.rename({1: 3, 2: 5})

df = pd.DataFrame({'A': [1, 2, 3], 'B': [4, 5, 6]})

# change column name
df.rename(columns={'A': 'a', 'B': 'c'})
df.rename(columns={'A': 'a', "C": 'c'})

df.columns = [['A', 'B'], ['C', 'D']]
df.rename(level=1, columns={'C': 'e', 'D': 'f'})
df.rename(level=1, columns={'C': 'e', 'D': 'f'}, inplace=True)
