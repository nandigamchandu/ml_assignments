"""Pandas concat method."""
import pandas as pd

df1 = pd.DataFrame({'A': ['A0', 'A1', 'A2', 'A3'],
                    'B': ['B0', 'B1', 'B2', 'B3'],
                    'C': ['C0', 'C1', 'C2', 'C3'],
                    'D': ['D0', 'D1', 'D2', 'D3']},
                   index=[0, 1, 2, 3])

df2 = pd.DataFrame({'A': ['A4', 'A5', 'A6', 'A7'],
                    'B': ['B4', 'B5', 'B6', 'B7'],
                    'C': ['C4', 'C5', 'C6', 'C7'],
                    'D': ['D4', 'D5', 'D6', 'D7']},
                   index=[4, 5, 6, 7])

df3 = pd.DataFrame({'A': ['A8', 'A9', 'A10', 'A11'],
                    'B': ['B8', 'B9', 'B10', 'B11'],
                    'C': ['C8', 'C9', 'C10', 'C11'],
                    'D': ['D8', 'D9', 'D10', 'D11']},
                   index=[8, 9, 10, 11])

pd.concat([df1, df2, df3])
# Construct hierarchical index using the passed keys as the outermost level.
concat1 = pd.concat([df1, df2, df3], keys=['x', 'y', 'z'])
concat1.loc['z']

# default join is outer
df4 = pd.DataFrame({'B': ['B2', 'B3', 'B6', 'B7'],
                    'D': ['D2', 'D3', 'D6', 'D7'],
                    'F': ['F2', 'F3', 'F6', 'F7']},
                   index=[2, 3, 6, 7])
pd.concat([df1, df4], axis=1)
pd.concat([df1, df4], axis=1, join='inner')
pd.concat([df1, df4], axis=1, join_axes=[df1.index])
pd.concat([df1, df4])
# ignoring index on the concatenation axis
pd.concat([df1, df4], ignore_index=True)

# Concatenating with mixed ndims
s1 = pd.Series(['X0', 'X1', 'X2', 'X3'], name='X')
pd.concat([df1, s1])
pd.concat([df1, s1], axis=1)
pd.concat([df1, s1], ignore_index=True)

# concat Series
s3 = pd.Series([0, 1, 2, 3], name='foo')
s4 = pd.Series([4, 5, 6, 7])
s5 = pd.Series([8, 9, 10, 11])
pd.concat([s3, s4, s5], axis=1)
pd.concat([s3, s4, s5], axis=1, keys=['x', 'y', 'z'])

# we can also pass dict to concat
# keys will be used as keys
piece = {'x': df1, 'y': df2, 'z': df3}
pd.concat(piece)
pd.concat(piece, keys=['z', 'x'])
