"""Pandas IndexSlice class practices."""

import pandas as pd

df = pd.read_csv("~/data/matches.csv")
df.head()
df = df.set_index(['id', 'season'])

# Using default slice command
df.loc[(slice(None), slice(2008, 2009)), 'city']

# Using the indexSlice class
idx = pd.IndexSlice
# Example 1
x = df.loc[idx[:, 2007:2010], 'city']

x = df.loc[idx[:, 2008:2015:2], :]
x.index.get_level_values(1).unique()
df = df.reset_index()
# set index
df = df.set_index(['team1', 'id', 'season'])
# sorting index
df1 = df.sort_index()

# Example 2
df1.loc[idx['Sunrisers Hyderabad', :, 2016:2018], 'team2']
