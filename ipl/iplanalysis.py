"""Exploring ipldataset."""

# %%
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# %%
# loading data
matches = pd.read_csv('~/data/matches.csv')
deliveries = (pd.read_csv('~/data/deliveries.csv')
                .assign(batsman_runs=lambda x: x['batsman_runs'].fillna(0)))

# %%
# percentage of null null values
matches.isnull().sum()
null_per = matches.isnull().sum() * 100 / matches.apply(len)
# 1) based on percent of null we can drop umpire3 columns
# 2) city, winner, player_of_matches, umpire1 and umpire2
#    has very less missing valuesself.
matches[matches.umpire1.isnull()]
# i d0 not find any reason why they missing,
# umpire1 and umpire2 miss for same obsevation
matches[matches.winner.isnull()]
# no reason, both winner and player_of_match miss value for the
# same observations
matches[matches.city.isnull()]
# city has missing values because 2014 7 matches are played in
# dubai international cricket stadium.

# %%
# check all matches played outside india.
matches.loc[matches.season == 2014, 'venue'].value_counts()
# some matches played in india some outside india

# %%
# 2014 season matches played on india ground
india_ground = (matches.loc[matches.season != 2014, 'venue']
                       .value_counts()
                       .index)
x = np.all([matches.season == 2014, matches.venue.isin(india_ground)], axis=0)
matches.loc[x, 'venue'].value_counts()
india_ground

# %%
matches.nunique()
# season, team1, team2, toss_winner, toss_decision, result
# dl_applies, winner, city and venue all are Categorical data
matches1 = (matches.assign(date=lambda x: pd.to_datetime(x['date']))
                   .assign(season=lambda x: pd.Categorical(x['season']))
                   .assign(result=lambda x: pd.Categorical(x['result']))
                   .assign(dl_applied=lambda x: pd.Categorical(
                                                            x['dl_applied']))
                   .drop(columns='umpire3')
                   .assign(won_bat_fld=lambda x: np.where(
                                matches['winner'] == matches['team1'],
                                'bat', 'field')))

# %%
# check which matches win decision taken based on dl applie
matches_win_dlapplied = matches1[matches1['dl_applied'] == 1]
sns.countplot(matches_win_dlapplied['won_bat_fld'])
plt.title('First field or bat team win when dl applied')
sns.despine()

# %%
# which ground favor which team
x = matches1.groupby(['winner', 'venue']).size().unstack().T
sns.set_style('white')
x.plot.bar(stacked=True, figsize=(10, 8))
sns.despine()

# %%
# count no of matches win by win against other team
matches1['losser'] = (np.where(matches1['winner'] == matches1['team1'],
                      matches1['team2'], matches1['team1']))
matches1.loc[matches1['winner'].isnull(), 'losser'] = np.nan
x = matches1.groupby(['winner'])['losser'].value_counts().unstack(level=0)
x.plot.bar(stacked=True, figsize=(10, 8))
sns.despine()

# %%
# which team is best against other team based on win percentage
x1 = matches1.groupby(['winner', 'losser']).size()
x1.index = x1.index.rename(['team1', 'team2'])
x2 = matches1.groupby(['losser', 'winner']).size()
x2.index = x1.index.rename(['team1', 'team2'])
matches_played = x1 + x2
x1 = matches_played.unstack()
y = (x*100)/x1
y.plot.bar(stacked=True, figsize=(10, 8))

# %%
# which team don't play atleast one matche against other team?
not_play = x1.isnull().stack().rename('notplayed')
not_play = not_play[not_play]
level0 = not_play.index.get_level_values(0)
level1 = not_play.index.get_level_values(1)
x = (not_play[level0 != level1].unstack()+1).fillna(0)
ax = sns.heatmap(x)
cbar = ax.collections[0].colorbar
cbar.set_ticks([0, 2])
cbar.set_ticklabels(['played', 'notplayed'])

# %%
# merge matches dataset and deliveries dataset
col = ['id', 'season', 'winner', 'losser']
ipl = (deliveries.merge(matches1[col], left_on='match_id', right_on='id')
                 .drop(columns=['id']))
# %%
# hard hitting ability = no of 4's, 6's / balls played by batsman
ball_by_player = ipl['batsman'].value_counts()
four_six_by_ply = ipl.loc[ipl['batsman_runs'] >= 4, 'batsman'].value_counts()
batsman_mat = pd.DataFrame()
batsman_mat['hard_hitting'] = four_six_by_ply / ball_by_player

# %%
# player who does not hit foures and sixes
batsman_mat[batsman_mat['hard_hitting'].isnull()].index

# %%
# finding good finisher = not out innings / total innings played
innings_by_ply = (ipl.groupby(['match_id', 'batsman'])
                     .size().index.get_level_values(1)
                     .value_counts())
out_innings = (ipl.groupby(['match_id', 'player_dismissed'])
                  .size().index.get_level_values(1)
                  .value_counts())
batsman_mat['finishing'] = (innings_by_ply - out_innings) / innings_by_ply

# %%
# finding players fast scoring ability = total runs / balls played by batsman
play_score = ipl.groupby(['batsman'])['batsman_runs'].sum()
batsman_mat['fast_scoring'] = play_score / ball_by_player

# %%
# finding player consistancy = Total runs / no of times out
batsman_mat['consistancy'] = play_score / out_innings

# %%
# player who is not able to score atleat one run
out_innings[~out_innings.index.isin(play_score.index)]
andre_nel = ipl[ipl['bowler'] == 'A Nel']
andre_nel['total_runs'].sum()
andre_nel['over'].nunique()
# he never get chance for batting
# he played only one match for mumbai indians
# he is a bowler and gave 31 runs for 3 overs

# player who does not out atlest one time
play_score[~play_score.index.isin(out_innings.index)]

# %%
# player unable to score atleast one run
zero_run_ply = batsman_mat[batsman_mat['fast_scoring'] == 0].index
# all this players are bowler except one

# bats man unable to score atleast one run
zero_run_ply[~zero_run_ply.isin(ipl['bowler'])]

# bowlers unable to score atleast one run
zero_score_bowler = zero_run_ply[zero_run_ply.isin(ipl['bowler'])]

# %%
# bowler| season| team| runs given| balls| no of matches played
x = ipl[ipl['bowler'].isin(zero_run_ply)]


def matchs_played(x):
    """Matches."""
    return x.unique().size


(x.groupby(['bowler', 'season', 'bowling_team'])
  .agg({'total_runs': np.sum, 'ball': 'count', 'match_id': matchs_played})
  .rename(columns={'total_runs': 'runs_given', 'ball': 'balls',
                   'match_id': 'no_matches'}))


# %%
# Runs between wickets =
# (Total runs - (fours + sixes)) / (Total balls played - boundaries)
ipl['batsman_runs'].unique()
ipl['runs_bw_wkt'] = ipl['batsman_runs']
ipl['runs_bw_wkt'].replace({6: 0}, inplace=True)
ipl['runs_bw_wkt'] = np.where(ipl['runs_bw_wkt'] < 4, ipl['runs_bw_wkt'],
                              ipl['runs_bw_wkt']-4)

runs_bw_wkt_ply = ipl.groupby('batsman')['runs_bw_wkt'].sum()
boundaries = ipl['batsman_runs']
boundaries[boundaries < 4] = np.nan
ipl['boundaries'] = boundaries.notnull()
boun_ply = ipl.groupby(['batsman'])['boundaries'].sum()
runs_bw_wkt_ply.nlargest(10)
ball_by_player['CH Gayle']
batsman_mat['runs_bw_wkts'] = runs_bw_wkt_ply / (ball_by_player - boun_ply)
