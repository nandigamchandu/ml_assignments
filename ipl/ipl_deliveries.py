"""Study deliveries."""

# %%
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# %%
# importing deliveries data
df = pd.read_csv('~/data/deliveries.csv')

# importing id and season columns from matches data set
ipl = pd.read_csv('~/data/matches.csv', usecols=['id', 'season'])

# join deliveries and matches data on match id
df1 = (df.merge(ipl, left_on='match_id', right_on='id',
                how='inner', left_index=True))

# %%
# observe no of observations, features and check for nulls
df1.shape
df1.info()
df1.isnull().sum()
df1['dismissal_kind'].unique()

# %%
# total no of matches played
df1['match_id'].nunique()

# %%
# find the no of teams participate
df1['batting_team'].nunique()

# %%
# no of teams participate vs season
df1.groupby('season')['batting_team'].nunique()

# %%
# find total no of players
play = pd.concat([df1.batsman, df1.non_striker, df1.bowler])
play.nunique()

# %%
# top 10 high runs batsman
top10_batsman = (df1.groupby(['batsman'])['batsman_runs'].sum().sort_values(
                                                    ascending=False).head(10))
plt.xticks(rotation=90)
sns.barplot(x=top10_batsman.index, y=top10_batsman.values)

# %%
# most run batsman vs year
x = df1.groupby(['season', 'batsman'])['batsman_runs'].sum().reset_index()
x = (x.sort_values(['season', 'batsman_runs'], ascending=False).groupby(
                          ['season']).head(1).set_index(['season', 'batsman']))
most_run_batsman = x
most_run_batsman

# %%
# most run batsman outs vs seasons
most_run_batsman_outs = (df1.groupby(
                            ['season'])['player_dismissed'].value_counts())
most_run_batsman_outs[most_run_batsman.index]

# %%
# most run batsman batting average
x = most_run_batsman_outs[most_run_batsman.index].to_frame(name='batsman_runs')
most_run_batsman / x

# %%
# most batsman runs vs seasons
x = df1.groupby(['season', 'match_id', 'batsman'])['batsman_runs'].sum()
x = x.reset_index()
x = (x.sort_values(['season', 'batsman_runs'], ascending=False).groupby(
                    ['season']).head(1).set_index(['season', 'batsman']))
x

# %%
# most centuries vs seasons
x = df1.groupby(['season', 'match_id', 'batsman'])['batsman_runs'].sum() // 100
x = x.reset_index()
x = (x.sort_values(['season', 'batsman_runs'], ascending=False).groupby(
                    ['season', 'batsman'])['batsman_runs'].sum().reset_index())
(x.sort_values(['season', 'batsman_runs'],
               ascending=False).groupby('season').head(1))

# %%
# most wickets by bowler vs seasons
bowler = ['caught', 'bowled', 'lbw', 'caught and bowled', 'stumped']
x = (df1[df1['dismissal_kind'].isin(bowler)].groupby(
        ['season', 'bowler'])['player_dismissed'].count().reset_index())
(x.sort_values(['season', 'player_dismissed'],
               ascending=False).groupby('season').head(1))

# %%
# more run given by bowler vs season
x = (df1.groupby(
     ['season', 'bowler'])[['noball_runs', 'batsman_runs', 'wide_runs']].sum(
                                                   ).sum(axis=1).reset_index())
x.sort_values(['season', 0], ascending=False).groupby('season').head(1)
