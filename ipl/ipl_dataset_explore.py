"""Ipl dataset explore."""

# %%
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

# %%
data = pd.read_csv('~/data/matches.csv')
data = data.drop(['umpire3'], axis='columns')

# %%
# first batting team using where function
x = (~np.logical_xor(data['toss_decision'] == 'bat',
                     data['toss_winner'] == data['team1']))
data1 = data
data1['bat_first_team'] = np.where(x, data['team1'], data['team2'])
data1.head(10)

# %%
# check is team1 first batting team
np.all(data1['team1'] == data1['bat_first_team'])

# %%
# add a column to indicate whether a winning team batted or fielded first
data1['bat_field_win'] = (np.where(data1['bat_first_team'] == data1['winner'],
                                   'bat', 'field'))
data1.head(10)

# %%
# win count of first batting and first fielding
wincount = data1['bat_field_win'].value_counts()
# plot of win count when batting first vs fielding first
plt.ylabel('win count')
plt.title('win count of first batting and first fielding')
sns.barplot(x=wincount.index, y=wincount.values)

# %%
# win percentage of batting first and fielding first
win_per = data1['bat_field_win'].value_counts()*100/len(data1.bat_field_win)
plt.title('win percentage of batting first and fielding first')
plt.ylabel('win percentage')
sns.barplot(x=win_per.index, y=win_per.values)

# %%
# plot of win count when batting first vs fielding first for every team
win_team = data1.groupby('winner')['bat_field_win'].value_counts()
plt.xticks(rotation=90)
sns.barplot(x=win_team.index.get_level_values(0), y=win_team.values,
            hue=win_team.index.get_level_values(1))

# %%
# matches vs venue
x = data1.groupby('venue').size()
plt.xticks(rotation=90)
plt.ylabel('matches')
sns.barplot(x=x.index, y=x.values)

# %%
# win percentage of first batting and first fielding vs venue
x = data1.groupby(['venue'])['bat_field_win'].count()
ven_win = data1.groupby(['venue'])['bat_field_win'].value_counts() * 100 / x
plt.figure(figsize=(8, 7))
plt.xticks(rotation=90)
plt.ylabel('win percentage')
sns.barplot(x=ven_win.index.get_level_values(0), y=ven_win.values,
            hue=ven_win.index.get_level_values(1))

# %%
# top 5 more matches between teams
x = data1.groupby(['team1'])['team2'].value_counts()
x = x.rename('matches')
(x.reset_index().sort_values(['team1', 'matches'],
                             ascending=False).groupby('team1').head(1))
# %%
# matches vs umpire1
data1.umpire1.value_counts().head()

# %%
# percentage of results(normal, tie, noresult)
x = data1['result'].value_counts() * 100 / data1['result'].count()
plt.ylabel('percentage')
plt.title('percentage vs match results')
sns.barplot(x=x.index, y=x.values)

# %%
# which team won toss more times
data1['toss_winner'].value_counts()

# %%
# Largest victories (by runs)
(data1[['season', 'team1', 'team2', 'win_by_runs']].sort_values(
                                      'win_by_runs', ascending=False).head())
