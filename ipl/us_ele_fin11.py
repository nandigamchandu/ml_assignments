"""EDA on 2012 US election finance dataset."""

# %%
import pandas as pd
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt

# %%
#  import data
data = pd.read_csv('/home/nandigamchandu/data/usa.csv')
df = data
df.shape

#  %%
df.info()

# no of nulls in each column
df.isnull().sum()

# %%
# finding no of unique values present in each column
df.nunique()

# %%
df['cmte_id'].unique()
df['cand_id'].unique()
df['cand_nm'].unique()
df['contbr_st'].unique()
df['contb_receipt_dt'].unique()
df['receipt_desc'].unique()
df['memo_cd'].unique()
df['form_tp'].unique()
df['file_num'].unique()

# %%
# convert data type of date str to datetime64
df['contb_receipt_dt1'] = pd.to_datetime(df['contb_receipt_dt'])

# %%
df.loc[df['contb_receipt_amt'].idxmax()]
# %%
# finding range of contb_receipt_dt
df['contb_receipt_dt1'].min(), df['contb_receipt_dt1'].max()

# %%
# contb_receipt_amt received by each candidate
x = (df.groupby(['cand_nm'])['contb_receipt_amt'].sum().sort_values(
                                                        ascending=False))
# plot barplot for contb_receipt_amt vs candidate
plt.xticks(rotation=90)
sns.barplot(x=x.index, y=x.values)

# %%
# no of contb from each state vs candidate
cont_sta_cn = (df.groupby(
               ['cand_nm', 'contbr_st'])['cand_id'].agg({'n_contb': 'count'}))
cont_sta_cn

# %%
# most contributes state for all candidates
x = (df.groupby(
       ['cand_nm', 'contbr_st'])['contb_receipt_amt'].sum().reset_index())
x.sort_values(['cand_nm', 'contb_receipt_amt'],
              ascending=False).groupby(['cand_nm']).head(1)

# %%
# most contb_receipt_amt to candidate from each state
df.groupby(['cand_nm', 'contbr_st'])['contb_receipt_amt'].sum()
x = (df.groupby(
     ['cand_nm', 'contbr_st'])['contb_receipt_amt'].sum().reset_index())
contb_state = (x.sort_values(['contbr_st', 'contb_receipt_amt'],
                             ascending=False).groupby(['contbr_st']).head(1))

# %%
# count no of most contributed state for candidates
x = contb_state['cand_nm'].value_counts()
plt.xticks(rotation=90)
plt.title('count of most contb amt received from each state vs candidate')
sns.barplot(x=x.index, y=x.values)

# %%
# Top 10 contb_receipt_amt vs occupation
x = df.groupby(['contbr_occupation'])['contb_receipt_amt'].sum()
top_amt_occ = x.sort_values(ascending=False).head(10)

# %%
# Top 10 contributors by occupation to candidates
x = (df[df['contbr_occupation'].isin(top_amt_occ.index)].groupby(
                  ['contbr_occupation', 'cand_nm'])['contb_receipt_amt'].sum())
sns.set_style('darkgrid')
x.unstack().plot(kind='bar', stacked=True, figsize=(10, 6))
plt.legend(loc='side')

# %%
# line plot for contb_receipt amount vs month
y_m = df['contb_receipt_dt1'].apply(lambda x: x.strftime('%Y-%b'))
x = df.groupby(y_m, sort=True)['contb_receipt_amt'].sum()
plt.xticks(rotation=90)
sns.pointplot(x=x.index, y=x.values)
plt.title('contb receipt amt vs month')

# %%
# bar plot for contribution amount receipt and refund by each candidate
rec = np.where(df['contb_receipt_amt'] > 0, 'receipt', 'refund')
df1 = df
df1['rec_ref'] = rec
x = df1.groupby(['cand_nm', 'rec_ref'])['contb_receipt_amt'].sum().unstack()
x['refund'] = x['refund'] * -1
x.plot(kind='bar', stacked=True, figsize=(10, 6))
