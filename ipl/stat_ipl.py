"""EDA on ipl dataset."""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# %%
# importing datasets
deliveries = pd.read_csv("~/data/deliveries.csv")
ipl = pd.read_csv("~/data/matches.csv", usecols=['id', 'season', 'venue'])
df = deliveries.merge(ipl, left_on='match_id', right_on='id')

# %%
# top ten most sixs
x = df[df['batsman_runs'] == 6]['batsman'].value_counts().head(10)
plt.xticks(rotation=90)
sns.barplot(x=x.index, y=x.values)

# %%
# players of a team
df_bat = df.groupby(['season', 'batting_team'])['batsman'].unique()
df_bat.index = df_bat.index.rename('team', level=1)
df_bowl = df.groupby(['season', 'bowling_team'])['bowler'].unique()
df_bowl.index = df_bowl.index.rename('team', level=1)
team = (pd.concat([df_bat, df_bowl], axis=1).apply(
        lambda x: set(x.batsman).union(set(x.bowler)), axis=1))['batsman']
team.rename('players')

# %%
# matches played by each player
bats = df[['id', 'batsman', 'season']].rename(columns={'batsman': 'player'})
bowler = df[['id', 'bowler', 'season']].rename(columns={'bowler': 'player'})
field = df[['id', 'fielder', 'season']].rename(columns={'fielder': 'player'})
match_played = pd.concat([bats, bowler, field])
(match_played.groupby(
            'player')['id'].nunique().sort_values(ascending=False)).head(10)

# %%
# matches played by each player season wise
x = match_played.groupby(['season', 'player'])['id'].nunique()
# most matches played by player season wise
(x.reset_index().sort_values(['season', 'id'],
                             ascending=False).groupby(['season']).head(1))

# %%
# top 5 Most six innings
x = (df.loc[df['batsman_runs'] == 6,
            ['season', 'id', 'batsman', 'batsman_runs']])
x.groupby(['season', 'id', 'batsman'])['batsman_runs'].count(
  ).sort_values(ascending=False).head(5)

# %%
# overs bowl by each player
x = (df.groupby(
          ['id', 'bowler'])['over'].nunique().rename('overs').reset_index())
overs = x.groupby(['bowler'])['overs'].sum()
overs.sort_values(ascending=False).head()

# %%
# most no dot balls
dots = df[df['total_runs'] == 0]
(dots.groupby('bowler')['total_runs'].count().sort_values(
                                               ascending=False).head())

# %%
# top 5 runs vs venue
df.groupby('venue')['total_runs'].sum().sort_values(ascending=False).head()
# top 5 wickets vs venue
(df.groupby(
    'venue')['player_dismissed'].count().sort_values(ascending=False).head())

# %%
# most maidens vs player
x = df[['id', 'inning', 'bowler', 'total_runs', 'over', 'ball']]
x = (x.groupby(['id', 'inning', 'over']).agg(
                                         {'ball': 'count', 'total_runs': sum}))
maiden = x[np.all([x['ball'] >= 6, x['total_runs'] == 0], axis=0)]
df_mad = df.set_index(['id', 'inning', 'over'])
play_maid = (df_mad.loc[maiden.index, 'bowler'].reset_index().drop_duplicates(
                      )['bowler'].value_counts().sort_values(ascending=False))

# %%
# total no of maiden overs
play_maid.sum()

# %%
# no extra balls
no_extra_balls = np.all([df['wide_runs'] == 0, df['noball_runs'] == 0], axis=0)
df1 = df[no_extra_balls]

# %%
# top 10  fastest fifties
x = df1.groupby(['id', 'batsman']).apply(lambda x: x.batsman_runs.cumsum())
f_50 = x <= 50
x = df1.groupby(['id', 'batsman']).apply(lambda x: x.batsman_runs)
x = x[f_50].reset_index()
x = x.groupby(['id', 'batsman']).agg({'batsman_runs': ['count', np.sum]})
(x[x[('batsman_runs', 'sum')] == 50].sort_values(
                                      ('batsman_runs', 'count')).head(10))

# %%
# top 10 fastest centuries
x = df1.groupby(['id', 'batsman']).apply(lambda x: x.batsman_runs.cumsum())
f_100 = x <= 100
x = df1.groupby(['id', 'batsman']).apply(lambda x: x.batsman_runs)
x = x[f_100].reset_index()
x = x.groupby(['id', 'batsman']).agg({'batsman_runs': ['count', np.sum]})
(x[x[('batsman_runs', 'sum')] == 100].sort_values(
                                      ('batsman_runs', 'count')).head(10))

# %%
# fastest century vs season
x = (df1.groupby(
         ['season', 'id', 'batsman']).apply(lambda x: x.batsman_runs.cumsum()))
f_100 = x <= 100
x = df1.groupby(['season', 'id', 'batsman']).apply(lambda x: x.batsman_runs)
x = x[f_100].reset_index()
x = (x.groupby(['season', 'id', 'batsman']).agg(
                                          {'batsman_runs': ['count', np.sum]}))
x = (x[x[('batsman_runs', 'sum')] == 100].reset_index().sort_values(
                                        ['season', ('batsman_runs', 'count')]))
x.groupby('season').head(1)
