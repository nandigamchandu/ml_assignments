"""this module is to study ipl dataset."""

# %%
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt


# %%
# loading and checking nulls for ipl data set.
data = pd.read_csv('~/data/matches.csv')
data.head()
data.info()
data.isnull().sum()
data = data.drop(['umpire3'], axis='columns')

# %%
# barplot for no of playerof match awards for each player
d1 = data['player_of_match'].value_counts().head(10)
plt.xticks(rotation=90)
plt.yticks([0, 2, 4, 6, 8, 10, 12, 14, 16, 18])
x = sns.barplot(x=d1.index, y=d1.values)

# %%
# total no of matches played by each team
match = data['team1'].value_counts() + data['team2'].value_counts()
# barplot for total no of matches played by each team
plt.xticks(rotation=90)
sns.barplot(x=match.index, y=match.values)

# %%
# total no of matches played by eact teeam in the year 2017
data2017 = data[data['season'] == 2017]
data2017['team1'].value_counts() + data2017['team2'].value_counts()

# %%
# no of matches won by each team
df = data['winner'].value_counts()
per = (df/match)*100
ipl = pd.DataFrame()
ipl['matches'] = match
ipl['matches_win'] = df
ipl['win_per'] = per
ipl.sort_values(by='win_per', ascending=False)


# %%
def first_played(df):
    """Function."""
    if df['toss_decision'] == 'bat':
        return df['toss_winner']
    else:
        if df['toss_winner'] != df['team1']:
            return df['team1']
        else:
            return df['team2']


data.apply(first_played, axis=1)
data1 = data
data1['bat_first_team'] = data.apply(first_played, axis=1)
data1.tail(25)

# %%
# add a column to indicate whether a winning team batted or fielded first
data1['bat_fielding'] = (np.where(data1['bat_first_team'] == data1['winner'],
                                  'bat', 'field'))
data1.head(10)

# %%
# win count of first batting and first fielding
wincount = data1['bat_fielding'].value_counts()
wincount

# %%
# win percentage of batting first and fielding first
data1['bat_fielding'].value_counts()*100/len(data1.bat_fielding)

# %%
# plot of win count when batting first vs fielding first
x = sns.barplot(x=wincount.index, y=wincount.values)

# %%
# plot of win count when batting first vs fielding first for every team
win_team = data1.groupby('winner')['bat_fielding'].value_counts()
plt.xticks(rotation=90)
sns.barplot(x=win_team.index.get_level_values(0), y=win_team.values,
            hue=win_team.index.get_level_values(1))

# %%
# no of matches played by each team per season
season_bat = data1.groupby(['season'])['team1'].value_counts()
season_bowl = data1.groupby(['season'])['team2'].value_counts()
season_bowl.index.rename('team1', level=1, inplace=True)
season_bowl + season_bat

# %%
# matches vs venue
data1.info()
x = data1.groupby('venue').size()
plt.xticks(rotation=90)
sns.barplot(x=x.index, y=x.values)
plt.ylabel('matches')

# %%
# toss_decision vs seasons
x = data1.groupby(['season', 'toss_decision']).size()
(sns.barplot(x=x.index.get_level_values(0), y=x.values,
             hue=x.index.get_level_values(1)))
plt.ylabel('count')
x

# %%
# win vs toss_decision
x = data1.groupby('toss_decision')['bat_fielding'].value_counts()
(sns.barplot(x=x.index.get_level_values(0), y=x.values,
             hue=x.index.get_level_values(1)))

# %%
# toss_decision vs venue
data1.result.unique()
data1[['result', 'dl_applied']]

data1[data1['season'] == 2017]['id']
