
"""Tidy data in python."""

# %%
# importing module
import pandas as pd
import datetime

# %%
# loading data
df = pd.read_csv('/home/nandigamchandu/data/tidy_data/pew-raw.csv')

# %%
# To unpivots use melt
formatted_df = pd.melt(df, ['religion'],
                       var_name='income', value_name='freq')
formatted_df = formatted_df.sort_values('religion')

# %%
df = pd.read_csv('~/data/tidy_data/billboard.csv', encoding="mac_latin2")
# melting
id_vars = df.columns[:7].tolist()
df = pd.melt(df, id_vars=id_vars, var_name="week", value_name="rank")
df = df.dropna()
df['week'] = df['week'].str.extract('(\d+)', expand=False).astype(int)
df['rank'] = df['rank'].astype(int)

# Cleaning out unnecessary rows

# Creating "date colums"
df['date'] = (pd.to_datetime(df['date.entered']) +
              pd.to_timedelta(df['week'], unit='w') -
              pd.DateOffset(weeks=1))
df = (df[["year", "artist.inverted", "track",
          "time", "genre", "week", "rank", "date"]])
df = df.sort_values(["year", "artist.inverted", "track", "week", "rank"])
df.head(10)

# %%
# Multiple type in one table
billboard = df
songs_col = df.columns[:5]
songs = billboard[songs_col].drop_duplicates()
songs = songs.reset_index(drop=True)
songs["song_id"] = songs.index
ranks = (pd.merge(billboard, songs,
                  on=["year", "artist.inverted", "track", "time", "genre"]))
ranks = ranks[['song_id', 'date', 'rank']]

# %%
# Multiple variables stored in one column
df = pd.read_csv("~/data/tidy_data/tb-raw.csv")
df = (pd.melt(df, id_vars=["country", "year"], value_name="cases",
              var_name="sex_and_age"))
tmp_df = df['sex_and_age'].str.extract('(\D)(\d+)(\d{2})')
tmp_df.columns = ['sex', 'age_lower', 'age_upper']
tmp_df['age'] = tmp_df['age_lower'] + "-" + tmp_df['age_upper']

df = pd.concat([df, tmp_df], axis=1)
df = df.dropna()
df = df.drop(['sex_and_age', 'age_lower', 'age_upper'], axis=1)

# %%
# Variables are stored in both rows and columns
df = pd.read_csv("~/data/tidy_data/weather-raw.csv")
df = (pd.melt(df, id_vars=['id', 'year', 'month', 'element'],
              var_name='day_raw'))

df['id'] = "MX17004"
df['day'] = df['day_raw'].str.extract('\D(\d+)', expand=False)
df.head()
df[["year", "month", "day"]] = (df[["year", "month", "day"]].apply(
                                  lambda x: pd.to_numeric(x, errors='ignore')))


# %%
def date_gen(raw):
    """Create date from year, month and day columns."""
    date = datetime.date(year=int(raw['year']),
                         month=int(raw['month']), day=int(raw['day']))
    return date


df['date'] = df.apply(date_gen, axis=1)
df = df.drop(['month', 'year', 'day_raw', 'day'], axis=1)
df = df.dropna()
df.set_index(['id', 'date', 'element']).unstack().reset_index()


# %%
# One type in multiple table

path = "~/data/tidy_data/"
last = "-baby-names-illinois.csv"
frame = pd.DataFrame()
df_list = []
for i in range(2014, 2016):
    file_ = path + str(i) + last
    df = pd.read_csv(file_, index_col=None, header=0)
    df.columns = map(str.lower, df.columns)
    df["year"] = i
    df_list.append(df)
df = pd.concat(df_list)

df.head()
