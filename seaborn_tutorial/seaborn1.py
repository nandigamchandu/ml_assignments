"""Seaborn1."""

# %%
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
np.random.seed(sum(map(ord, "aesthetics")))


# %%
def sinplot(flip=1):
    """Plot offset sinewaves."""
    x = np.linspace(0, 14, 100)
    for i in range(1, 7):
        plt.plot(x, np.sin(x+i*0.5)*(7-i)*flip)


sinplot()

# %%
sns.set()
sinplot()
# to control style: axes_style(), set_style()
# to scale plot: plotting_context(), set_context()

# %%
# Seaborn figure style
sns.set_style("whitegrid")
data = np.random.normal(size=(20, 6)) + np.arange(6) / 2
sns.boxplot(data=data)

# %%
sns.set_style("dark")
sinplot()

# %%
sns.set_style("white")
sns.set_style('ticks')
sinplot()

# %%
# Removing axes spine
sinplot()
sns.despine()

# %%
# trim parameter
f, ax = plt.subplots()
sns.violinplot(data=data)
sns.despine(offset=10, trim=True)

# %%
# Temporarily setting figure style
with sns.axes_style("darkgrid"):
    plt.subplot(211)
    sinplot()
plt.subplot(212)
sinplot(-1)

# %%
# to see all axis setting, it will return a dict
sns.axes_style()

# %%
sns.set_style('darkgrid')
sinplot()

# %%
sns.set_style('darkgrid', {"axes.facecolor": "0.8"})
sinplot()

# %%
sns.set_style('darkgrid', {"axes.facecolor": ".0", "axes.grid": False})
sinplot()

# %%
# set to defalut values
sns.set()
sns.set_style('darkgrid')
sinplot(-1)

# %%
# Scaling plot elements
# return default parameter values in dict
sns.plotting_context()

# %%
# tring all scaling
sns.set_context("paper")
sinplot()

sns.set_context('talk')
sinplot()

sns.set_context('poster')
sinplot()

# %%
# changing default values
sns.set_context("notebook", font_scale=1.5, rc={"lines.linewidth": 2.5})
sinplot()

# %%
sns.set()
