"""Plotting on data-aware grids."""
# %%
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from scipy import stats
# %%
sns.set(style="ticks")
np.random.seed(sum(map(ord, "axis_grids")))
tips = sns.load_dataset("tips")

# %%
g = sns.FacetGrid(tips, col='time')
# provide it with a plotting function and the name of variable
g = sns.FacetGrid(tips, col="time")
g.map(plt.hist, 'tip')

# %%
# to make related plot
g = sns.FacetGrid(tips, col="sex", hue="smoker")
g.map(plt.scatter, "total_bill", "tip", alpha=.7)
g.add_legend()

# %%
# option for controlling the look
g = sns.FacetGrid(tips, row="smoker", col="time", margin_titles=True)
g.map(sns.regplot, "size", "total_bill", color='.3',
      fit_reg=False, x_jitter=.1)

# %%
# set size of the figure
g = sns.FacetGrid(tips, col="day", size=4, aspect=.5)
g.map(sns.barplot, "sex", "total_bill")

# %%
# pass parameter gridspec module. The can be used to
# draw attention to a particular facet by increasing size.
# It’s particularly useful when visualizing distributions of datasets
# with unequal numbers of groups in each facet.

titanic = sns.load_dataset("titanic")
titanic = titanic.assign(deck=titanic.deck.astype(object)).sort_values("deck")
g = sns.FacetGrid(titanic, col="class", sharex=False,
                  gridspec_kws={"width_ratios": [5, 3, 3]})
g.map(sns.boxplot, "deck", "age")

# %%
# o specify an ordering of any facet dimension
# with the appropriate *_order parameter
ordered_days = tips.day.value_counts().index
g = sns.FacetGrid(tips, row="day", row_order=ordered_days,
                  size=1.7, aspect=4,)
g.map(sns.distplot, "total_bill", hist=False, rug=True)

# %%
# customize the color of hue
# we can use dictionary that maps the names of values in the hue
pal = dict(Lunch="seagreen", Dinner="gray")
g = sns.FacetGrid(tips, hue="time", palette=pal, size=5)
g.map(plt.scatter, "total_bill", "tip", s=50, alpha=.7,
      lw=.5, edgecolor="white")
g.add_legend()

# %%
# parameter hue_kws
# where keys are the names of plotting function keyword arguments and
# values are lists of keyword values, one for each level of the hue variable
g = sns.FacetGrid(tips, hue="sex", palette="Set1", size=5,
                  hue_kws={"marker": ["^", "v"]})
g.map(plt.scatter, "total_bill", "tip", s=100, linewidth=.5, edgecolor="white")
g.add_legend()

# %%
# use of col_warp parameter is to control columns.
# it is useful if we have many levels of one variable
attend = sns.load_dataset("attention").query("subject <= 12")
g = sns.FacetGrid(attend, col="subject", col_wrap=4, size=2, ylim=(0, 10))
g.map(sns.pointplot, "solutions", "score", color=".3", ci=None)

# %%
# we can set axis labels using set_axis_labels, ticks, space bw plots
g = sns.FacetGrid(tips, row="sex", col="smoker", margin_titles=True, size=2.5)
g.map(plt.scatter, "total_bill", "tip",
      color="#334488", edgecolor="white", lw=.5)
g.set_axis_labels("Total bill (US Dollars)", "Tip")
g.set(xticks=[10, 30, 50], yticks=[2, 6, 10])
g.fig.subplots_adjust(wspace=.02, hspace=.02)

# %%
# we can get underling matplotlib figure and axes object by using attributes
# fig and axes
g = sns.FacetGrid(tips, col='smoker', size=4)
g.map(plt.scatter, 'total_bill', 'tip', s=50, lw=1)
ax = g.axes
ax[0, 0].plot((0, 50), (0, 0.2*50), c='0.2')
ax[0, 1].plot((0, 50), (0, 0.2*50), c='0.2')
g.set(xlim=(0, 60), ylim=(0, 14))


# %%
# Mapping custom functions onto the grids
def quatile_plot(x, **kwargs):
    """Probplot."""
    qntls, xr = stats.probplot(x, fit=False)
    plt.scatter(xr, qntls, **kwargs)


g = sns.FacetGrid(tips, col='sex', size=4)
g.map(quatile_plot, "total_bill")


# %%
# To make bivariate plot
def qqplot(x, y, **kwargs):
    """Qqplot."""
    _, xr = stats.probplot(x, fit=False)
    _, yr = stats.probplot(y, fit=False)
    plt.scatter(xr, yr, **kwargs)


g = sns.FacetGrid(tips, col='smoker', size=4)
g.map(qqplot, 'total_bill', 'tip')

g = sns.FacetGrid(tips, hue='time', col='sex', size=4)
g.map(qqplot, 'total_bill', 'tip')

# %%
g = sns.FacetGrid(tips, hue='time', col='sex', size=4,
                  hue_kws={'marker': ['s', 'D']})
g.map(qqplot, 'total_bill', 'tip', s=40, edgecolor='w')

# %%
# Plotting pairwise relationships in a dataset
iris = sns.load_dataset('iris')
g = sns.PairGrid(iris)
g.map(plt.scatter)

# %%
# setting plot for diagonal and off diagonal
g = sns.PairGrid(iris)
g.map_diag(plt.hist)
g.map_offdiag(plt.scatter)

# %%
# usage of hue parameter
g = sns.PairGrid(iris, hue='species')
g.map_diag(plt.hist)
g.map_offdiag(plt.scatter)
g.add_legend()

# %%
# control input dataframe columns
g = sns.PairGrid(iris, vars=['sepal_length', 'sepal_width'], hue='species')
g.map(plt.scatter)
g.add_legend()

# %%
# set plots for lower, upper and diagonal
g = sns.PairGrid(iris)
g.map_diag(sns.kdeplot, legend=False)
g.map_upper(plt.scatter)
g.map_lower(sns.kdeplot, cmap='Blues_r')
g.add_legend()

# %%
g = sns.PairGrid(tips, y_vars=['tip'], x_vars=['total_bill', 'size'], size=4)
g.map(sns.regplot, color='0.3')

# %%
sns.pairplot(iris, hue='species')
sns.pairplot(iris, hue='species', diag_kind="kde")
