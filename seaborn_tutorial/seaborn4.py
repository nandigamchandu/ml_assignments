"""Visualizing linear relationships."""

# %%
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt

# %%
np.random.seed(sum(map(ord, "regression")))
tips = sns.load_dataset('tips')
sns.set(color_codes=True)
sns.set_style('darkgrid')

# %%
# Function to draw linear regression models
# regplot aceept x and y variables in a variety of formats np.array, series or
# reference to variables in a pandas dataframe
sns.regplot(x="total_bill", y="tip", data=tips)
sns.lmplot(x="total_bill", y="tip", data=tips)

# one variable is discrete
sns.lmplot(x='size', y='tip', data=tips)
# to avoid overlap jitter parameter can be used
sns.lmplot(x='size', y='tip', data=tips, x_jitter=0.05)
# estimate of central tendency along with a confidence interval
sns.lmplot(x='size', y='tip', data=tips, x_estimator=np.mean)

# %%
# Fitting different kinds of models
anscombe = sns.load_dataset('anscombe')
sns.lmplot(x="x", y="y", data=anscombe.query("dataset == 'I'"),
           ci=None, scatter_kws={"s": 80})

# we can fit to higher-order relationships by using order parameter
sns.lmplot(x="x", y="y", data=anscombe.query("dataset == 'II'"),
           ci=None, scatter_kws={"s": 80}, order=2)

# presence of outlier
sns.lmplot(x="x", y="y", data=anscombe.query("dataset == 'III'"),
           ci=None, scatter_kws={"s": 80})

# by using robust regression, which uses a different loss function to
# downweight relatively large residuals
sns.lmplot(x="x", y="y", data=anscombe.query("dataset == 'III'"),
           robust=True, ci=None, scatter_kws={"s": 80})

# when y variable is binary
tips["big_tip"] = (tips.tip / tips.total_bill) > .15
sns.lmplot(x="total_bill", y="big_tip", data=tips,
           y_jitter=.03)

# to fit a logistic regression
sns.lmplot(x="total_bill", y="big_tip", data=tips,
           logistic=True, y_jitter=.03)

# residplot check simple regression model is appropriate for a dataset
sns.residplot(x="x", y="y", data=anscombe.query("dataset == 'I'"),
              scatter_kws={"s": 80})

# if there is structure in residuals,
# simple linear regression is not appropriate
sns.residplot(x="x", y="y", data=anscombe.query("dataset == 'II'"),
              scatter_kws={"s": 80})

# %%
# Conditioning on other variable
# regplot show single relationship
# lmplot combins regplot with FacetGrid
sns.lmplot(x='total_bill', y='tip', hue='smoker', data=tips)

# customize
sns.lmplot(x='total_bill', y='tip', hue='smoker', data=tips,
           markers=['o', 'x'], palette='Set1')

sns.lmplot(x='total_bill', y='tip', hue='smoker', data=tips, col='time')
sns.lmplot(x='total_bill', y='tip', hue='smoker',
           data=tips, row='sex', col='time')

# %%
# controling the size and shape of the plot
f, ax = plt.subplots(figsize=(5, 6))
sns.regplot(x='total_bill', y='tip', data=tips, ax=ax)

# lmplot is use FacetGrid. So figure size can control using size and aspect
sns.lmplot(x='total_bill', y='tip', col='day',
           data=tips, col_wrap=2, size=3)

sns.lmplot(x='total_bill', y='tip', col='day', data=tips, aspect=0.5)

# %%
# plotting a regression in other context
sns.jointplot(x='total_bill', y='tip', data=tips, kind='reg')

sns.pairplot(tips, x_vars=['total_bill', 'size'], y_vars=['tip'],
             size=5, aspect=0.8, kind='reg')

# Conditioning on an additional
# categorical variable is bulit into pairplot using the hue parameter
sns.pairplot(tips, x_vars=['total_bill', 'size'], y_vars=['tip'],
             hue='smoker', size=5, aspect=0.8, kind='reg')
