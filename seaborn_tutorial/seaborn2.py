"""Visualizing the distribution of a dataset."""

# %%
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats, integrate

# %%
np.random.seed(sum(map(ord, "distributions")))

# plotting univariate distributions
x = np.random.normal(size=100)
sns.distplot(x)

# plot hist plots
sns.distplot(x, kde=False, rug=True)

# increasing bins
sns.distplot(x, kde=False, rug=True, bins=20)

# kernel density estimation
sns.distplot(x, hist=False, rug=True)

# %%
support = np.linspace(-4, 4, 200)
mean = np.random.normal(0, 1, size=30)
sd = 0.5
sns.set_style('darkgrid')
kernels = []
for x in mean:
    kernel = stats.norm(x, sd).pdf(support)
    kernels.append(kernel)
    plt.plot(support, kernel, 'r')
sns.rugplot(mean, color="0.2", linewidth=3)

density = np.sum(kernels, axis=0)
density /= integrate.trapz(density, support)
plt.plot(support, density)

# %%
# bandwidth parameter of the Kde control
# how tightly the estimation is fit to the data
sns.kdeplot(mean)
sns.kdeplot(mean, bw=0.2, label='bw:0.2')
sns.kdeplot(mean, bw=2, label='bw:2')

# %%
sns.kdeplot(mean, shade=True, cut=0)
sns.rugplot(mean)

# %%
# visually evaluate how closely it corresponds to the observed data
x = np.random.gamma(6, size=200)
sns.distplot(x, fit=stats.gamma)

# %%
# Plotting bivariate distributions
mean = [-1, 1]
cov = [(1, 0.8), (0.8, 1)]
data = np.random.multivariate_normal(mean, cov, 200)
df = pd.DataFrame(data, columns=["x", "y"])
sns.jointplot(x="x", y='y', data=df)

# %%
mean = [-1, 2]
cov = [(1, -0.8), (-0.8, 1)]
data = np.random.multivariate_normal(mean, cov, 1000)
df = pd.DataFrame(data, columns=["x", "y"])
sns.jointplot(x="x", y="y", data=df, kind="hex")

# %%
# jointplot return jointgrid which is used to add more layers
# in following plot scatter plot layer is added
mean = [-1, 2]
cov = [(1, 0.6), (0.6, 1)]
data = np.random.multivariate_normal(mean, cov, 40)
df = pd.DataFrame(data, columns=["x", "y"])
g = sns.jointplot(x="x", y="y", data=df, kind="kde")
g.plot_joint(plt.scatter, c='w', s=30, linewidth=1, marker='+')

# %%
# combine kdeplot and rugplots of bivariate
f, ax = plt.subplots(figsize=(6, 6))
sns.kdeplot(df.x, df.y, ax=ax)
sns.rugplot(df.x, color='g', ax=ax)
sns.rugplot(df.y, vertical=True, ax=ax)

# %%
# visualizing pairwise relationship in a dataset
iris = sns.load_dataset("iris")
sns.pairplot(iris)

# %%
# to change diagonal and off diagonal plots of pairplot
g = sns.PairGrid(iris)
g.map_diag(sns.kdeplot)
g.map_offdiag(sns.kdeplot)
