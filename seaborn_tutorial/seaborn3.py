"""Plotting categorical data."""

# %%
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# %%
sns.set(style="whitegrid", color_codes=True)
np.random.seed(sum(map(ord, "categorical")))
titanic = sns.load_dataset("titanic")
tips = sns.load_dataset("tips")
iris = sns.load_dataset('iris')

# %%
# scatterplot to the case where one of the variable is categorical
sns.stripplot(x="day", y="total_bill", data=tips)
# to avoid overlap of points by using jitter parameter
sns.stripplot(x="day", y="total_bill", data=tips, jitter=True)

# %%
#  an algorithm that avoid overlapping point
sns.swarmplot(x='day', y='total_bill', data=tips)

# it's also possible to add nested categorical variable using hue parameter
sns.swarmplot(x='day', y='total_bill', hue='sex', data=tips)
# change orientation
sns.swarmplot(y='day', x='total_bill', hue='sex', data=tips)

# swarplot plot in categorical order
# string type catogories will be plotted in the order they appear in dataframe
# categories that look numerical will be sorted.
sns.swarmplot(x='size', y='total_bill', data=tips)

# %%
# Distributions of observations with categories
sns.boxplot(x='day', y='total_bill', hue='time', data=tips)
tips['weekend'] = tips['day'].isin(['Sat', 'Sun'])
sns.boxplot(x='day', y='total_bill', hue='weekend', data=tips, dodge=False)

# %%
# violinplots combine boxplot and kernel density estination
sns.violinplot(y='day', x='total_bill', hue='time', data=tips)

# to split the violine when hue parameter has only two levels
sns.violinplot(x='day', y='total_bill', hue='sex', data=tips, split=True)
# to show each individual observation instead of boxplot
(sns.violinplot(x='day', y='total_bill', hue='sex', data=tips,
                split=True, inner="stick"))

# %%
# combine vilinplot with swarmplot or boxplot
sns.violinplot(x='day', y='total_bill', data=tips, inner=None)
sns.swarmplot(x='day', y='total_bill', data=tips, color='w', alpha=0.5)

# %%
# Stattical estimatrion with categories
# barplot
sns.barplot(x="sex", y="survived", hue="class", data=titanic)
# countplot
sns.countplot(x='deck', data=titanic, palette="Greens_d")
sns.countplot(x='deck', hue='class', data=titanic, palette="Greens_d")

# %%
# point plots
plt.figure(figsize=(8, 10))
plt.subplot(211)
sns.barplot(x="sex", y="survived", hue="class", data=titanic)
plt.subplot(212)
sns.pointplot(x='sex', y='survived', hue='class', data=titanic)

# %%
plt.figure(figsize=(8, 10))
plt.subplot(211)
sns.pointplot(x="class", y="survived", hue="sex", data=titanic)
plt.subplot(212)
(sns.pointplot(x="class", y="survived", hue="sex", data=titanic,
               palette={'male': "g", 'female': 'm'}, markers=["^", "o"],
               linestyles=['-', '--']))

# %%
# plotting 'wide-form' data
sns.boxplot(data=iris, orient='h')
# %%
sns.violinplot(x=iris.species, y=iris.sepal_length)

# %%
sns.countplot(y='deck', data=titanic, color='c')

# %%
# Drawing multi-panel categorical plots
sns.factorplot(x='day', y='total_bill', hue='smoker', data=tips)
sns.factorplot(x='day', y='total_bill', hue='smoker', data=tips, kind='bar')
(sns.factorplot(x='day', y='total_bill', col='time',
                hue='smoker', data=tips, kind='bar'))
# to change size and shape use size and aspect
(sns.factorplot(x='time', y='total_bill', col='day',
                hue='smoker', data=tips, kind='box', size=4, aspect=0.5))

# %%
g = sns.PairGrid(tips,
                 x_vars=["smoker", "time", "sex"],
                 y_vars=["total_bill", "tip"],
                 aspect=0.75, size=3.5)
g.map(sns.violinplot, palette="pastel")
