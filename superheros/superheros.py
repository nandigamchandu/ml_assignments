"""EDA of superheros dataset."""

# %%
# Importing requare modules
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# %%
# import dataset
heroes_info = pd.read_csv('~/data/superheros/heroes_information.csv')
hero_pow = pd.read_csv('~/data/superheros/super_hero_powers.csv')

# %%
# shape of dataframes
heroes_info.shape
hero_pow.shape

# %%
# no of unique values present in each column
heroes_info.nunique()
hero_pow.nunique()

# %%
# check all columns of hero_pow dataset as not have 2 unique values or
# not except hero_names column
np.sum(~hero_pow.nunique()[1:] == 2)

# %%
# unique value present in gender column
heroes_info['Gender'].unique()
heroes_info['Alignment'].unique()
heroes_info['Skin color'].unique()
heroes_info['Publisher'].unique()
heroes_info['Race'].unique()

# %%
# check frequency of the value for less nunqiue
heroes_info['Skin color'].value_counts()
# most of the superheros skin color is not mentioned
heroes_info['Gender'].value_counts()
heroes_info['Alignment'].value_counts()
heroes_info['Hair color'].value_counts()
heroes_info['Race'].value_counts()

# %%
# check of null values
heroes_info.isnull().sum()
hero_pow.isnull().sum()  # no missing values

# %%
# why weight has only two missing value ?
heroes_info[heroes_info['Weight'].isnull()]

# who are superheros not come from any publisher?
heroes_info.loc[heroes_info['Publisher'].isnull(), 'name']

# %%
heroes_info['Gender'].value_counts()
# superheroes who are neither male nor female
heroes_info.loc[heroes_info['Gender'] == '-', 'name']

# %%
# is any duplicates  present in dataset
heroes_info.duplicated().sum()

# %%
# check how many -ve height and -ve weight
heg_nev = heroes_info['Height'] < 0
wei_nev = heroes_info['Weight'] < 0
hei_wei_nev = np.all([heg_nev, wei_nev], axis=0)
np.sum(heg_nev)
np.sum(wei_nev)
np.sum(hei_wei_nev)

# %%
# superheros missed in superheros power dataset
miss_heroes = (heroes_info.loc[~heroes_info['name'].isin(
                                            hero_pow['hero_names']), 'name'])
miss_heroes.size

# %%
# more no of super heroes come from which publisher
hp = heroes_info['Publisher'].value_counts()
plt.xticks(rotation=90)
sns.barplot(hp.index, hp.values)

# %%
# hero having more powers
hero_pow['no_of_pow'] = hero_pow.iloc[:, 1:].sum(axis=1)
sup_has_max_pow = hero_pow['no_of_pow'].max()
hero_pow.loc[hero_pow['no_of_pow'] == sup_has_max_pow, 'hero_names']

# %%
# no of superheros has only one power
min_no_pow = hero_pow['no_of_pow'].min()
np.sum(hero_pow['no_of_pow'] == min_no_pow)


# %%
# most no of power super hero from each publishers
def top_hero(x):
    """Super hero."""
    ind = x['no_of_pow'].idxmax()
    max = x['no_of_pow'].max()
    return x['name'][ind], max


def min_npow_hero(x):
    """Super hero."""
    ind = x['no_of_pow'].idxmax()
    max = x['no_of_pow'].min()
    return x['name'][ind], max


hero = heroes_info.merge(hero_pow, left_on='name', right_on='hero_names')
hero.groupby('Publisher')['name', 'no_of_pow'].agg(top_hero)
# least no of powers supre heros from each publisher
hero.groupby('Publisher')['name', 'no_of_pow'].agg(min_npow_hero)
# most power super hreo from each gender
hero.groupby('Gender')['name', 'no_of_pow'].agg(top_hero)
# least no of power super heros from each gender
hero.groupby('Gender')['name', 'no_of_pow'].agg(min_npow_hero)

# %%
# most common powers for superheros
x = hero_pow.drop(columns=['no_of_pow']).set_index('hero_names').stack()
power_count = x.groupby(level=1).sum()
power_count = power_count.rename('no_heroes_had')
cp = power_count.nlargest(10)
# unique power
uni_pow = power_count[power_count == power_count.min()]
# bar plot for top 10 common powers
plt.xticks(rotation=90)
sns.barplot(cp.index, cp.values)


# %%
# super heroes having unique power
xx = x.reset_index()
xx = xx[xx[0]].drop(columns=0)
xx = xx[xx['level_1'].isin(uni_pow.index)].rename(columns={'level_1': 'power'})
xx

# %%
# unique power super heros published by
(heroes_info.loc[heroes_info['name'].isin(
                                     xx['hero_names']), ['name', 'Publisher']])

# %%
# publisher having only one super hero
one_hero_pub = heroes_info['Publisher'].value_counts()
one_hero_pub = one_hero_pub[one_hero_pub == 1]
(heroes_info.loc[heroes_info['Publisher'].isin(
                                 one_hero_pub.index), ['name', 'Publisher']])

# %%
# distribution plots of weigth and height
fig, ax = plt.subplots(1, 2, figsize=(10, 5))
sns.distplot(heroes_info['Weight'].dropna(), bins=40, ax=ax[0])
sns.distplot(heroes_info['Height'], bins=40, ax=ax[1])

# %%
# bar plot for different hair colors
heroes_info['Hair color'].unique()
heroes_info['Hair color'].value_counts()
heroes_info[heroes_info['name'] == 'Superman']
heroes_info['Hair color'] = heroes_info['Hair color'].str.replace('black',
                                                                  'Black')
heroes_info['Hair color'] = heroes_info['Hair color'].str.replace('blond',
                                                                  'Blond')
hair_colors = heroes_info['Hair color'].value_counts()
plt.xticks(rotation=90)
sns.barplot(x=hair_colors.index, y=hair_colors.values)
