"""Scopes."""

import builtins
import first
import thismod

# Global scope X
X = 99


def func():
    """Local scope."""
    X = 88
    return X


def func1(Y):
    """Y and Z are local,X is global."""
    Z = X + Y
    return Z


X = 15
func(20)
dir(builtins)  # return all bulitins names
zip
builtins.zip
zip is builtins.zip
# Note:
# 1) local scope may override variables of the same name
# in both the gobal and bulit-in scope
# 2) global names may override bulit-in


open = 'spam'
open('data.txt')  # This will return error
X = 88


def func():
    """Local variable."""
    X = 99
    return X


func()
print(X)  # prints 88: unchanged

# we can directly see all builtin names
# dir(__builtin__ ) instead of (import builtin)

#
X = 88


def func():
    """Global."""
    global X
    X = 99


print(X)  # prints 88
func()
print(X)  # prints 99
y, z = 1, 2


def all_global():
    """Variables are global."""
    global x
    x = y + z


all_global()
x
# Notes:
# 1) In general function rely on arguments and return values instead of gobals

# Cross-file changes
# 1) program Design: minimize cross-file changes
# 2) we can change variables in another file directly
print(first.X)
first.X = 88
print(first.X)

thismod.var
thismod.test()
thismod.var

# scopes and nested functions
X = 99


def f1():
    """Function."""
    X = 88

    def f2():
        """Nested Function."""
        print(X)
    f2()


f1()
# f2 is temporary function that lives only
# during the execution of the enclosinf f1

# Enclosing scopes are sometime also called nested scopes
# Nested scoped example
# Factory functions : closures
X = 99


def f1():
    """Enclose function."""
    X = 88

    def f2():
        """Nested function."""
        print(X)
    return f2


action = f1()
action()  # prints 88


def maker(N):
    """Closure function."""
    def action(X):
        return X ** N
    return action


# 1) Retaining Enclosing Scope state with defaults
# 2) Avoid nesting defs within defs

# Recommended format
def f1():
    """F1 callinf f2."""
    x = 88
    f2(x)


def f2(x):
    """Func2."""
    print(x)


f1()

# General notation of closures functions


def maker1(N):
    """Closure function."""
    return lambda X: X ** N


h = maker(3)
h(4)
g = maker(2)
g(2)
h(2)


def func():
    """Closure function."""
    x = 4
    action = (lambda n: x ** n)
    return action


x = func()
print(x(2))

# loop variables may require defaults , not scopes


def makeaction():
    """Closure function."""
    acts = []
    for i in range(5):
        acts.append(lambda x: i ** x)
    return acts


acts = makeaction()
acts
acts[0](2)
acts[1](2)
acts[2](2)
acts[3](2)
acts[4](2)

# This doesn't quit work, though because the enclosing scope variable is looked
# up when the nested function are later called. This is the one case where we
# still have to explicitly retain enclosing scope values with default
# arguments, rather than enclosing scope references


def makeactions():
    """Closure function."""
    acts = []
    for i in range(5):
        acts.append(lambda x, i=i: i ** x)
    return acts


acts = makeactions()
acts[0][2]
acts[0](2)
acts[1](2)
acts[2](2)
acts[3](2)
acts[4](2)

# arbitrary scope nesting


def f1():
    """Outter most function."""
    x = 99

    def f2():
        """Nest1."""
        def f3():
            """Nest2."""
            print(x)
        f3()
    f2()


f1()
# python will search the local scopes of all enclosing defs,
# from inner to outter

# The nonlocal statement in 3.x

nonlocal X  # we can't set nonlocal scope at moduler level

# the nonlocal statement mostly serves to allow names in enclosing
# scopes to be changed rather than just referenced.


def tester(start):
    """Enclose function."""
    state = start

    def nested(label):
        """Nest function."""
        print(label, state)
        state += 1
    return nested


F = tester(0)
F('spam')
# we get following error
# UnboundLocalError: local variable 'state' referenced before assignment


def tester(start):
    """Enclose function."""
    state = start

    def nested(label):
        """Nest function."""
        nonlocal state
        print(label, state)
        state += 1
    return nested


F = tester(0)
F('spam')
F('ham')
F('egg')
G = tester(42)
G('spam')
G('egg')
F('bacon')

# Boundary cases
# we cann't create nonlocal variables dynamically


def tested(start):
    """Enclosing function."""
    def nested(label):
        """Nest function."""
        nonlocal state
# we get following error
# no binding for nonlocal 'state' found


def tester(start):
    """Enclose function."""
    def nested(label):
        global state
        state = 0
        print(label, state)
    return nested


F = tester(0)
F('abc')
state
# nonlocals are not looked up in the enclosing modular's global scope or
# the bulit-in scope
spam = 99


def tester():
    """Enclose function."""
    def nested():
        """Nest function."""
        nonlocal spam
# no binding for nonlocal 'spam' found
# nonlocal names are still not visible outside the enclosing function


# other way to get nonlocal
def tester(start):
    """Enclose function."""
    def nested(label):
        """Nest function."""
        print(label, nested.state)
        nested.state += 1
    nested.state = start
    return nested


F = tester(0)
F('spam')
F('ham')
F.state
