"""Thismod."""

var = 98


def local():
    """Change local var."""
    var = 0
    var


def global1():
    """Global var."""
    global var
    var += 1


def global2():
    """Modify global."""
    var = 0
    var
    import thismod
    thismod.var += 1


def global3():
    """Modify global."""
    var = 0
    var
    import sys
    glob = sys.modules['thismod']
    glob.var += 1


def test():
    """Test."""
    print(var)
    local()
    global1()
    global2()
    global3()
    print(var)
