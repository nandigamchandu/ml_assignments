"""Generations and Comprehensions."""
import math

res = []
for x in 'spam':
    res.append(ord(x))

res = list(map(ord, 'spam'))
res
res = [ord(x) for x in 'spam']
res

# list comprehensions become more convenient though, when we wish to apply
# an arbitrary expression to an iterable instead of a function

[x ** 2 for x in range(10)]
list(map((lambda x: x ** 2), range(10)))

# addind tests and nested loops: filter

[x for x in range(5) if x % 2 == 0]
list(filter((lambda x: x % 2 == 0), range(5)))
res = []
for x in range(5):
    if x % 2 == 0:
        res.append(x)
res

# apply function after filter operation
list(map((lambda x: x**2), filter((lambda x: x % 2 == 0), range(10))))
[x ** 2 for x in range(10) if x % 2 == 0]

# general structure of list comprehensions looks like this

# [expression for target1 in iterable1 if condition1
#             for target2 in iterable2 if condition2...
#             for targetN in iterableN if conditionN]

# for clauses are nested within a list comprehension, they work like equivalent
# nested for loop statements

res = [x + y for x in [0, 1, 2] for y in [100, 200, 300]]
res = []
for x in [0, 1, 2]:
    for y in [100, 200, 300]:
        res.append(x+y)

[x + y + z for x in 'spam' if x in 'sm'
 for y in 'SPAM' if y in ('P', 'A')
 for z in '123' if z > '1']

# List comprehensions and matrixes
M = [[1, 2, 3],
     [4, 5, 6],
     [7, 8, 9]]

N = [[2, 2, 2],
     [3, 3, 3],
     [4, 4, 4]]

# List comprehension are powerful tools for processing such structures,
# though, because they are automatically scan rows and columns for us
[row[1] for row in M]
[M[row][1] for row in (0, 1, 2)]
[M[row][row] for row in (0, 1, 2)]

# changing such martix in place requried two for loop if shape id diff
L = [[1, 2, 3], [4, 5, 6]]
for i in range(len(L)):
    for j in range(len(L[i])):
        L[i][j] += 10

[col + 10 for row in M for col in row]
# for loop equivalent
res = []
for row in M:
    for col in row:
        res.append(col + 10)


[[col + 10 for col in row] for row in M]
# for loop equivalent
res = []
for row in M:
    tmp = []
    for col in row:
        tmp.append(col + 10)
    res.append(tmp)

[M[row][col] for row in range(3) for col in range(3)]
[M[row][col] * N[row][col] for row in range(3) for col in range(3)]

# Gererator functions and expressions

# 1) generator functions are coded as normal def statements , but use yield
# statements to return result one at a time, suspending and resuming their
# stae between each.
# 2) genererator expression are similar to the list comprehensions,
# but they return an object that produce result on demand instead of
# buliding a result list


# Generator Functions: yield versus return
def gensequare(N):
    """Generator."""
    for i in range(N):
        yield i ** 2


for i in gensequare(5):
    print(i, end=':')

x = gensequare(4)
x
next(x)
next(x)
next(x)
next(x)
next(x)
y = gensequare(5)
iter(y) is y
next(y)


def bulidsquare(n):
    """Func with return statement."""
    res = []
    for i in range(n):
        res.append(i ** 2)
    return res


for x in bulidsquare(5):
    print(x, end=':')

# we could use any of the for loop, map, or list comprehension techniques
for x in [n ** 2 for n in range(5)]:
    print(x, end=':')

for x in map((lambda n: n**2), range(5)):
    print(x, end=':')


def ups(line):
    """Generator."""
    for sub in line.split(','):
        yield sub.upper()


tuple(ups('aaa, bbb, ccc'))


# extended generator function protocol: send versus next
def gen():
    """Generator."""
    for i in range(10):
        X = yield i
        print(X)


G = gen()
next(G)
G.send(11)
G.send(88)
next(G)
next(G)
next(G)
next(G)
next(G)
next(G)
next(G)
next(G)

# Generator expressions

[x ** 2 for x in range(4)]  # List comprehension
(x ** 2 for x in range(4))  # Generator expression
# list comprehension
list(x ** 2 for x in range(4))

for num in (x ** 2 for x in range(4)):
    print('%s, %s' % (num, num / 2.0))

(x + '\n' for x in 'aaa,bbb,ccc'.split(','))
a, b, c = (x + '\n' for x in 'aaa,bbb,ccc'.split(','))

''.join(x.upper() for x in 'aaa,bbb,ccc'.split(','))

sum(x ** 2 for x in range(4))
sorted(x ** 2 for x in range(4))

# generator expressions versus map
list(map(abs, (-1, -2, 3, 4)))
list(abs(x) for x in (-1, -2, 3, 4))

# makes a pointless list
line = 'aaa,bbb,ccc'
# List comprehension
''.join([x.upper() for x in line.split(',')])
# generates result
''.join(x.upper() for x in line.split(','))
# map
''.join(map(str.upper, line.split(',')))

# Nested comprehensions
[x * 2 for x in [abs(x) for x in (-1, -2, 3, 4)]]
# Nested maps
list(map(lambda x: x * 2, map(abs, (-1, -2, 3, 4))))
# Nested generators
list(x * 2 for x in (abs(x) for x in (-1, -2, 3, 4)))

# nested combination
list(map(math.sqrt, (x ** 2 for x in range(4))))

# Unnested
list(abs(x) * 2 for x in (-1, -2, 3, 4))
list(math.sqrt(x ** 2) for x in range(4))

# gnerator expressions versus filter
line = 'aa bbb c'
# generator with if condition
''.join(x for x in line.split() if len(x) > 1)
# similar to filter
''.join(filter(lambda x: len(x) > 1, line.split()))
''.join(x.upper() for x in line.split() if len(x) > 1)
''.join(map(str.upper, filter(lambda x: len(x) > 1, line.split())))

# for loop equivalent
res = ''
for x in line.split():
    if len(x) > 1:
        res += x.upper()

# Generator functions versus Generator expressions
# Generator expression
G = (c * 4 for c in 'SPAM')
list(G)


# Generator function
def timesfour(S):
    """Generator."""
    for c in S:
        yield c * 4


G = timesfour('spam')
list(G)
line = 'aa bbb c'
''.join(x.upper() for x in line.split() if len(x) > 1)


def gensub(line):
    """Generator."""
    for x in line.split():
        if len(x) > 1:
            yield x.upper()


''.join(gensub(line))

# Generators are single-iterion objects
G = (c * 4 for c in 'SPAM')
iter(G) is G
i1 = iter(G)
i1.next()
i1.__next__()
i1.__next__()
i2 = iter(G)
next(i2)
next(i2)
next(i2)


G = timesfour('SPAM')
i1 = iter(G)
next(i1)
next(i1)
i2 = iter(G)
next(i2)
next(i2)
next(i2)


def both(N):
    """Generator."""
    for i in range(N):
        yield i


list(both(5))


def both(N):
    """Generator."""
    for i in range(N):
        yield i
    for i in (x ** 2 for x in range(N)):
        yield i


list(both(5))


# extended syntax for the yield statement that allows delegation
# to the subgenerator with from generator clause.
def both(N):
    """Generator."""
    yield from range(N)
    yield from (x ** 2 for x in range(N))


':'.join(str(i) for i in both(5))
# Generation in built-in type, tools and classes
D = {'a': 1, 'b': 2, 'c': 3}
x = iter(D)
next(x)
next(x)
next(x)
next(x)
for key in D:
    print(key, D[key])

# above is automatic iteration tools using for loop


# generator and function application
def f(a, b, c):
    """Function."""
    print('%s, %s, and %s' % (a, b, c))


f(0, 1, 2)
f(*range(3))
f(*(i for i in range(3)))
# this applies to dictionaries and views
D = dict(a='Bob', b='dev', c=40.5)
f(**D)
f(*D)
f(a='Bod', d='dev', c=40.5)

# 1) unpack dict: key=value
# 2) unpack keys iterator
# Normal keywords
# unpack view iterator: iterable

# using * to unpack the results forced from generator expression return values
f(*D.values())
print(range(5))
print(*(range(5)))
print(*(x.upper() for x in 'spam'))

# Scrambling sequences
L, S = [1, 2, 3], 'spam'
for i in range(len(S)):
    S = S[1:] + S[:1]
    print(S, end=' ')

for i in range(len(L)):
    L = L[1:] + L[:1]
    print(L, end=' ')

for i in range(len(S)):
    X = S[i:] + S[:i]
    print(X, end=' ')


# simple functions
def scramble(seq):
    """Function."""
    res = []
    for i in range(len(seq)):
        res.append(seq[i:] + seq[:i])
    return res


scramble('spam')


def scramble(seq):
    """Use list comprehension."""
    return [seq[i:] + seq[:i] for i in range(len(seq))]


scramble('spam')
for x in scramble((1, 2, 3)):
    print(x, end=' ')


# generator function
def scramble(seq):
    """Generator."""
    for i in range(len(seq)):
        yield seq[i:] + seq[:i]


scramble('spam')
list(scramble('spam'))
for x in scramble((1, 2, 3)):
    print(x, end=' ')

# Generator expressions
G = (S[i:] + S[:i] for i in range(len(S)))
list(G)

F = (lambda seq: (seq[i:] + seq[:i] for i in range(len(seq))))
list(F((1, 2, 3)))
for x in F((1, 2, 3)):
    print(x, end=' ')
