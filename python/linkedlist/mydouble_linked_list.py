"""Implement doubly-linked list in Python."""
import copy
import numpy as np


class Node:
    """Node implementation for doubly-linked list."""

    def __init__(self, v=None, next=None, prev=None):
        """Construct a node."""
        self.value = v
        self.next = next
        self.prev = prev


class DoublyLinkedList:
    """Implementation of doubly-linked list."""

    def __init__(self, head=None, tail=None, length=0):
        """Construct a doubly-linked list."""
        self.head = head
        self.tail = tail
        self.length = length

    def is_empty(self):
        """Return True if the list is empty; False otherwise."""
        return self.length == 0

    def prepend(self, v):
        """Prepend given value to the linked list."""
        new_cell = Node(v, self.head, None)

        # prepend when the list is empty
        if self.is_empty():
            self.head = new_cell
            self.tail = new_cell
            self.length = 1
            return self

        # prepend when the list is not empty
        self.head.prev = new_cell
        self.head = new_cell
        self.length = self.length + 1
        return self

    def seek_from_end(self, i):
        """Return value at index i from the end of the list."""
        if i < 0 or i >= self.length:
            return -1
        n = self.tail
        if i == 0:
            return n.value
        while i > 0:
            i -= 1
            n = n.prev
        return n.value

    def assign(self, v, i):
        """Assign value at given index."""
        if i < 0 or i >= self.length:
            return -1
        n = self.head
        while i > 0:
            i -= 1
            n = n.next
        n.value = v

    def insert(self, v, i):
        """Insert value at given index."""
        new_cell = Node(v, None, None)
        n = self.head
        if i < 0 or i >= self.length:
            return -1
        if i > 0:
            while i > 1:
                i -= 1
                n = n.next
            # n.next.prev = new_cell
            new_cell.next = n.next
            new_cell.prev = n
            n.next = new_cell
            new_cell.next.prev = new_cell
        else:
            self.prepend(v)
        self.length += 1

    def remove(self, i):
        """Remove value at given index."""
        if i < 0 or i >= self.length:
            return -1
        n = self.head
        if i > 0:
            while i > 1:
                i -= 1
                n = n.next
            if n.next.next is not None:
                n.next = n.next.next
                n.next.prev = n
            else:
                n.next = None
                self.tail = n
        else:
            n.next.prev = None
            self.head = n.next
        self.length -= 1

    def assign_from_end(self, v, i):
        """Assign value at index from end."""
        n = self.tail
        if i < 0 or i >= self.length:
            return -1
        while i > 0:
            i -= 1
            n = n.prev
        n.value = v

    def simple_traverse(self):
        """Traverse and print only node values."""
        n = self.head
        print('Head->', end='')
        while n is not None:
            print(f"{n.value}->", end='')
            n = n.next
        print('Tail')

    def simple_traverse_from_end(self):
        """Traverse from end and print only node values."""
        n = self.tail
        print('Tail->', end='')
        while n is not None:
            print(f"{n.value}->", end='')
            n = n.prev
        print('Head')

    def append(self, v):
        """Appen for the doublelinkedlist."""
        n = self.tail
        new_cell = Node(v, None, n)
        if self.is_empty():
            self.head = new_cell
            self.tail = new_cell
            self.length = 1
            return self
        n.next = new_cell
        self.tail = new_cell
        self.length += 1

    def slice_extract(self, start, end):
        """Slice and extract from index start to end."""
        n = self.head
        len = self.length
        i = 1
        dl = DoublyLinkedList()
        if start > end or start < 1 or end < 1 or start > len or end > len:
            return -1
        while i <= self.length+1:
            if i >= start and i <= end:
                dl.append(n.value)
            if i > end:
                return dl
            n = n.next
            i += 1

    def myrange(self, start, end):
        """Return list containing all integers within a given range."""
        dl = DoublyLinkedList()
        if end < start:
            return -1
        for i in range(start, end+1):
            dl.append(i)
        return dl

    def rotate(self, shift):
        """Rotate by n shift."""
        len = self.length
        copy1 = copy.deepcopy(self)
        if abs(shift) > 0:
            shift = shift - (shift//len) * len
        else:
            return copy1
        n = copy1.head
        copy1.head.prev = copy1.tail
        copy1.tail.next = copy1.head
        len = copy1.length
        i = 1
        while i <= len:
            if i == shift + 1:
                n.prev.next = None
                copy1.tail = n.prev
                n.prev = None
                copy1.head = n
                return copy1
            i += 1
            n = n.next

    def rnd_select(self, size):
        """Select n random values from list."""
        if size < 0:
            return -1
        n = self.head
        len = self.length
        sel = np.random.randint(1, len+1, size)
        dl = DoublyLinkedList()
        for ii in sel:
            n = self.head
            for i in range(1, len+1):
                if i == ii:
                    dl.append(n.value)
                n = n.next
        return dl

    def remove_at(self, i):
        """Remove value at i location."""
        copy1 = copy.deepcopy(self)
        copy1.remove(i-1)
        return copy1


class TestDoublelinkedList(object):
    """Test suit for DoubleLinkedList."""

    def test_slice_extract(self):
        """Test slice_extract() functionality."""
        dl = DoublyLinkedList()
        dl.prepend(5)
        dl.prepend(45)
        dl.prepend(75)
        dl.prepend(95)
        dl.prepend(38)
        dl.prepend(20)
        assert dl.slice_extract(4, 2) == -1
        assert dl.slice_extract(-1, 5) == -1
        assert dl.slice_extract(-8, -1) == -1
        assert dl.slice_extract(8, 10) == -1
        assert dl.slice_extract(4, 8) == -1
        sl = dl.slice_extract(2, 5)
        assert sl.head.value == 38
        assert sl.tail.value == 45
        sl = dl.slice_extract(3, 3)
        assert sl.head.value == 95
        assert sl.tail.value == 95

    def test_rotate(self):
        """Test rotate() functionality."""
        dl = DoublyLinkedList()
        dl.prepend(6)
        dl.prepend(5)
        dl.prepend(4)
        dl.prepend(3)
        dl.prepend(2)
        dl.prepend(1)
        r = dl.rotate(3)
        assert r.head.value == 4
        assert r.head.next.value == 5
        assert r.tail.value == 3
        r = dl.rotate(-2)
        assert r.head.value == 5
        assert r.head.next.value == 6
        assert r.tail.value == 4
        r = dl.rotate(8)
        assert r.head.value == 3
        assert r.tail.value == 2
        r = dl.rotate(-15)
        assert r.head.value == 4
        assert r.tail.value == 3
        r = dl.rotate(0)
        assert r.head.value == 1
        assert r.tail.value == 6

    def test_myrange(self):
        """Test myrange() functionality."""
        dl = DoublyLinkedList()
        assert dl.myrange(6, 2) == -1
        rng = dl.myrange(5, 9)
        assert rng.head.value == 5
        assert rng.tail.value == 9
        rng = dl.myrange(-1, 2)
        assert rng.head.value == -1
        assert rng.tail.value == 2

    def test_rnd_select(self):
        """Test rmd_select() functionality."""
        dl = DoublyLinkedList()
        dl.prepend(5)
        dl.prepend(45)
        dl.prepend(75)
        dl.prepend(95)
        dl.prepend(38)
        dl.prepend(20)
        rnd = dl.rnd_select(2)
        assert rnd.length == 2
        rnd = dl.rnd_select(0)
        assert rnd.is_empty() is True
        assert dl.rnd_select(-8) == -1
        rnd = dl.rnd_select(10)
        assert rnd.length == 10

    def test_remove_at(self):
        """Test remove_at() functionality."""
        dl = DoublyLinkedList()
        dl.prepend(5)
        dl.prepend(45)
        dl.prepend(75)
        dl.prepend(95)
        dl.prepend(38)
        dl.prepend(20)
        rm = dl.remove_at(1)
        assert rm.head.value == 38
        rm = dl.remove_at(6)
        assert rm.tail.value == 45
        rm = dl.remove_at(3)
        assert rm.head.next.next.value == 75


def main():
    """To print all results."""
    dl = DoublyLinkedList()
    dl.prepend(66)
    dl.prepend(53)
    dl.prepend(53)
    dl.prepend(45)
    dl.prepend(34)
    dl.prepend(22)
    dl.prepend(10)
    print('simple_traverse')
    dl.simple_traverse()
    print('slice and extract from index 4 to 5')
    dl.slice_extract(4, 5).simple_traverse()
    print('slice and extract from index 2 to 6')
    dl.slice_extract(2, 6).simple_traverse()
    print('range between 2, 9')
    DoublyLinkedList().myrange(2, 9).simple_traverse()
    print('anti clockwise rotate by 4')
    dl.rotate(-4).simple_traverse()
    print('clockwise rotate by 4')
    dl.rotate(4).simple_traverse()
    print('randomly select 3 values')
    dl.rnd_select(3).simple_traverse()
    print('remove at 7')
    dl.remove_at(7).simple_traverse()
    print('remove at 1')
    dl.remove_at(1).simple_traverse()
    print('remove at 3')
    dl.remove_at(3).simple_traverse()


main()
