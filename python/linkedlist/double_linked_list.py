"""Implement doubly-linked list in Python."""


class Node:
    """Node implementation for doubly-linked list."""

    def __init__(self, v=None, next=None, prev=None):
        """Construct a node."""
        self.value = v
        self.next = next
        self.prev = prev


class DoublyLinkedList:
    """Implementation of doubly-linked list."""

    def __init__(self, head=None, tail=None, length=0):
        """Construct a doubly-linked list."""
        self.head = head
        self.tail = tail
        self.length = length

    def is_empty(self):
        """Return True if the list is empty; False otherwise."""
        return self.length == 0

    def prepend(self, v):
        """Prepend given value to the linked list."""
        new_cell = Node(v, self.head, None)

        # prepend when the list is empty
        if self.is_empty():
            self.head = new_cell
            self.tail = new_cell
            self.length = 1
            return self

        # prepend when the list is not empty
        self.head.prev = new_cell
        self.head = new_cell
        self.length = self.length + 1
        return self

    def seek_from_end(self, i):
        """Return value at index i from the end of the list."""
        if i < 0 or i >= self.length:
            return -1
        n = self.tail
        if i == 0:
            return n.value
        while i > 0:
            i -= 1
            n = n.prev
        return n.value

    def assign(self, v, i):
        """Assign value at given index."""
        if i < 0 or i >= self.length:
            return -1
        n = self.head
        while i > 0:
            i -= 1
            n = n.next
        n.value = v

    def insert(self, v, i):
        """Insert value at given index."""
        new_cell = Node(v, None, None)
        n = self.head
        if i < 0 or i >= self.length:
            return -1
        if i > 0:
            while i > 1:
                i -= 1
                n = n.next
            # n.next.prev = new_cell
            new_cell.next = n.next
            new_cell.prev = n
            n.next = new_cell
            new_cell.next.prev = new_cell
        else:
            self.prepend(v)
        self.length += 1

    def remove(self, i):
        """Remove value at given index."""
        if i < 0 or i >= self.length:
            return -1
        n = self.head
        if i > 0:
            while i > 1:
                i -= 1
                n = n.next
            if n.next.next is not None:
                n.next = n.next.next
                n.next.prev = n
            else:
                n.next = None
                self.tail = n
        else:
            n.next.prev = None
            self.head = n.next
        self.length -= 1

    def assign_from_end(self, v, i):
        """Assign value at index from end."""
        n = self.tail
        if i < 0 or i >= self.length:
            return -1
        while i > 0:
            i -= 1
            n = n.prev
        n.value = v

    def simple_traverse(self):
        """Traverse and print only node values."""
        n = self.head
        print('Head->', end='')
        while n is not None:
            print(f"{n.value}->", end='')
            n = n.next
        print('Tail')

    def simple_traverse_from_end(self):
        """Traverse from end and print only node values."""
        n = self.tail
        print('Tail->', end='')
        while n is not None:
            print(f"{n.value}->", end='')
            n = n.prev
        print('Head')


class TestDoublelinkedList(object):
    """Test suite for DoubleLinkedList."""

    def test_seek_from_end(self):
        """Test see_from_end functionality."""
        dl = DoublyLinkedList()
        dl.prepend(5)
        dl.prepend(45)
        dl.prepend(75)
        dl.prepend(95)
        dl.prepend(38)
        dl.prepend(20)
        assert dl.seek_from_end(0) == 5
        assert dl.seek_from_end(3) == 95
        assert dl.seek_from_end(5) == 20
        assert dl.seek_from_end(-1) == -1
        assert dl.seek_from_end(99) == -1

    def test_assign(self):
        """Test assign functionality."""
        dl = DoublyLinkedList()
        dl.prepend(89)
        dl.prepend(71)
        dl.prepend(78)
        dl.prepend(60)
        dl.prepend(10)
        dl.assign(8, 0)
        assert dl.seek_from_end(4) == 8
        dl.assign(6, 2)
        assert dl.seek_from_end(2) == 6
        dl.assign(45, 4)
        assert dl.seek_from_end(0) == 45
        assert dl.assign(81, -7) == -1
        assert dl.assign(8, 5) == -1
        assert dl.assign(48, 94) == -1

    def test_insert(self):
        """Test insert functionality."""
        dl = DoublyLinkedList()
        dl.prepend(89)
        dl.prepend(71)
        dl.prepend(78)
        dl.prepend(60)
        dl.prepend(10)
        dl.insert(24, 0)
        assert dl.seek_from_end(5) == 24
        dl.insert(45, 2)
        assert dl.seek_from_end(4) == 45
        dl.insert(9, 6)
        assert dl.seek_from_end(0) == 89
        assert dl.seek_from_end(1) == 9
        assert dl.insert(78, -8) == -1
        assert dl.insert(8, 9) == -1

    def test_remove(self):
        """Test remove functionality."""
        dl = DoublyLinkedList()
        dl.prepend(54)
        dl.prepend(78)
        dl.prepend(7)
        dl.prepend(74)
        dl.prepend(48)
        dl.prepend(17)
        dl.remove(0)
        assert dl.seek_from_end(dl.length-1) == 48
        dl.remove(2)
        assert dl.seek_from_end(2) == 74
        assert dl.length == 4
        dl.remove(3)
        assert dl.seek_from_end(0) == 78
        assert dl.remove(-1) == -1
        assert dl.remove(4) == -1

    def test_assign_from_end(self):
        """Test assign_from_end functionality."""
        dl = DoublyLinkedList()
        dl.prepend(54)
        dl.prepend(78)
        dl.prepend(7)
        dl.prepend(74)
        dl.prepend(48)
        dl.assign_from_end(40, 0)
        assert dl.seek_from_end(0) == 40
        dl.assign_from_end(50, 3)
        assert dl.seek_from_end(3) == 50
        dl.assign_from_end(25, 4)
        assert dl.seek_from_end(4) == 25
        assert dl.seek_from_end(5) == -1
        assert dl.seek_from_end(-4) == -1
        assert dl.seek_from_end(48) == -1


def main():
    """Try to print."""
    dl = DoublyLinkedList()
    dl.prepend(54)
    dl.prepend(78)
    dl.prepend(7)
    dl.prepend(45)
    dl.prepend(21)
    dl.prepend(92)
    dl.prepend(48)
    dl.simple_traverse()
    dl.remove(2)
    dl.simple_traverse()
    dl.simple_traverse_from_end()
    dl.insert(19, 2)
    dl.insert(85, 5)
    dl.simple_traverse()
    dl.simple_traverse_from_end()


main()
