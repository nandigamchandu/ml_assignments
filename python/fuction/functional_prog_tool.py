"""Functional programming tools."""

import functools
import operator

# python blends support for multiple programming paradigms:
# 1) procedural
# 2) object-oriented and
# 3) functional

# quick survey of bulit-in functions that apply other functions to iterables
# automatically. mapping functions over iterables: map

counters = [1, 2, 3, 4]
updated = []

for x in counters:
    updated.append(x+10)

updated


def inc1(x):
    """Increment."""
    return x + 10


list(map(inc1, counters))

# we make more general use of it by passing in user-define
# function to be applied to each item in the list.
list(map((lambda x: x + 3), counters))


# implement map function
def mymap(func, seq):
    """Map function."""
    res = []
    for x in seq:
        res.append(func(x))
    return res


list(map(inc1, [1, 2, 3, 4]))
mymap(inc1, [1, 2, 3, 4])

# map expects as N-argument function fron N sequences
pow(2, 3)
list(map(pow, [1, 2, 3, 4], [2, 2, 2, 2]))

# thw map call is similar to the list comprehension expressions
list(map(inc1, [1, 2, 3]))
[inc1(x) for x in [1, 2, 3, 4]]
# wrapping a comprehension in parentheses instead of square brackets
# creates an object that generates values on request. Selecting items
# in iterables: filter

list(range(-5, 5))
list(filter((lambda x: x > 0), range(-5, 5)))
res = []
for x in range(-5, 5):
    if x > 0:
        res.append(x)

res
[x for x in range(-5, 5) if x > 0]

# Combining items in iterables: reduce
# The functional reduce present functools
functools.reduce((lambda x, y: x + y), [1, 2, 3, 4])
functools.reduce((lambda x, y: x * y), [1, 2, 3, 4])
# By default, the first item in the sequence intializes the starting value.
L = [1, 2, 3, 4]
res = L[0]
for x in L[1:]:
    res = res + x

res

# implementation of reduce function


def myreduce(function, sequence):
    """Implement reduce function."""
    tally = sequence[0]
    for next in sequence[1:]:
        tally = function(tally, next)
    return tally


myreduce((lambda x, y: x+y), [1, 2, 3, 4])
myreduce((lambda x, y: x*y), [1, 2, 3, 4, 5])
functools.reduce(operator.add, [2, 4, 6])
functools.reduce((lambda x, y: x+y), [2, 4, 5])
functools.reduce((lambda x, y: x+y), [2, 4, 6])
