"""Anonymous functions."""
import sys
# 1) lambda arg1, arg2,....argN: expression using arguments
# 2) The lambda's body is similar to
# what you'd put in a def body's return statement


def func(x, y, z):
    """Function."""
    return x+y+z


func(2, 3, 4)
# lambda function

f = (lambda x, y, z: x+y+z)
f(2, 3, 4)

# Default work on lambda arguments
x = (lambda a='fee', b='fie', c='foe': a + b + c)
x()
x('wee')


# code in lambda body also follows
# the same scope lookup rules as code inside a def.
def knights():
    """Enclose function."""
    title = 'Sir'
    action = (lambda x: title + ' ' + x)
    return action


act = knights()
msg = act('robin')
msg
act

# lambda is also commonly used to code jump tables
L = [lambda x: x**2, lambda x: x**3, lambda x: x**4]
for f in L:
    print(f(2))

L[2](3)
# def wom't work inside a list literal
# Multiway branch switches: the finale
key = 'got'
{'already': (lambda: 2+2),
 'got': (lambda: 2*4),
 'one': (lambda: 2**6)}[key]()

lower = (lambda x, y: x if x < y else y)
lower('aa', 'bb')

# if we need to perform loops within a lambda
showall = (lambda x: list(map(sys.stdout.write, x)))
t = showall(['spam\n', 'toast\n', 'egg\n'])
showall = (lambda x: [sys.stdout.write(line) for line in x])
t = showall(['spam\n', 'toast\n', 'egg\n'])
showall = (lambda x: [print(line, end="") for line in x])
t = showall(['spam\n', 'toast\n', 'egg\n'])
showall = (lambda x: print(*x, sep='', end=''))
t = showall(['spam\n', 'toast\n', 'egg\n'])

# lambdas can be nested too
# to improve readability avoid nested lambda
((lambda x: (lambda y: x + y))(99))(4)
