"""Class mechanics."""

from abc import ABCMeta, abstractmethod

#  General form

# class name(superclass, ...):
#     attr = Value
#     def method(self, ...):
#         self.attr = value


class ShareData:
    """Example1."""

    spam = 42


x = ShareData()
y = ShareData()
x.spam, y.spam
# we can change attribute through either instance or class

# 1) through class
ShareData.spam = 99
x.spam, y.spam, ShareData.spam
# 2) through instance
y.spam = 11
x.spam, y.spam, ShareData.spam
# note: assignments to instance attributes create or
# change the name in the instance, rather than in the shared class


class MixedNames:   # Define class
    """Example2."""

    data = 'spam'  # Assign class attr

    def __init__(self, value):  # assign method name
        """Contructor."""
        self.data = value  # assign instance attr

    def display(self):
        """Deplay data."""
        print(self.data, MixedNames.data)  # instance attr, class attr


# make two instance objects
x = MixedNames(1)
y = MixedNames(2)

# calling display method
x.display()
y.display()

# methods

# method call made through an instance
# 1) instance.method(args ...)
# method call made through an class
# 2) class.meth(instance, args...)


class NextClass:
    """Example3."""

    message = None

    def printer(self, text):
        """Printer."""
        self.message = text
        print(self.message)


x = NextClass()
x.printer('instance call')
y = NextClass()
y.printer('new instance call')
NextClass.printer(x, 'class call1')
NextClass.printer(y, 'class call2')

# calling superclass constructors


class Super:
    """Supper class."""

    def __init__(self, x):
        """Super class constructor."""
        pass


class Sub(Super):
    """SubClass."""

    def __init__(self, x, y):
        """Subclass constructor."""
        Super.__init__(self, x)
        pass


i = Sub(1, 2)

# Other method call possibility
# 1) methods are instance metods in the absence of any special code
# 2) Normally, an in-stance must always be passed to a method—whether
#    automatically when it is called through an instance,
#    or manually when you call through a class.

# Inheritance


class Super:
    """Super class."""

    def method(self):
        """Behavior."""
        print('in Super.method')


class Sub(Super):
    """Sub class."""

    def method(self):
        """Override method."""
        print('starting sub.method')  # Add action here
        Super.method(self)  # Run default ation
        print('ending sub.method')


x = Super()
x.method()

x = Sub()
x.method()


# Class interface techniques
class Super:
    """Super class."""

    def method(self):
        """Deflaut behavior."""
        print('in Super.method')

    def delegate(self):
        """Method."""
        self.action()  # Expected to be define


class Inheritor(Super):
    """Sub class."""

    pass


class Replacer(Super):
    """Sub class replace method functionality."""

    def method(self):
        """Replace method completely."""
        print('in Replacer.method')


class Extender(Super):
    """Sub class extend method functionality."""

    def method(self):
        """Extend method behavior."""
        print('starting Extender.method')
        Super.method(self)
        print('ending Extender.method')


class Provider(Super):
    """Sub class fill required method."""

    def action(self):
        """Fill in a required method."""
        print('in Provider.action')


print('--------------Class interface techniques----------')
Inheritor().method()
Replacer().method()
Extender().method()

x = Provider()
x.delegate()


# Abstract superclasses
class Super:
    """Super class."""

    def delegate(self):
        """Delegate."""
        self.action()

    def action(self):
        """Implement action function."""
        raise NotImplementedError('action must be define')


class Sub(Super):
    """Sub class."""

    def action(self):
        """Implement action function."""
        print('spam')


print('----------Abstract super class-----------------')
x = Sub().delegate()

# Abstract superclasses in python 3.X


class Super(metaclass=ABCMeta):
    """Super class with abstract method."""

    def delegate(self):
        """Method."""
        self.action()

    @abstractmethod
    def action(self):
        """Implement in sub class."""
        pass


class Sub(Super):
    """Subclass."""

    def action(self):
        """Implement action method."""
        print('spam')


print('-------Abstract super class-----')
x = Sub()
x.delegate()

# Coded this way, a class with an abstract method cannot be instantiated
