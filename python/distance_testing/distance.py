import operator as op

def map1(func, v1, v2):
    arr = []
    for i in range(max(len(v1), len(v2))):
        arr.append(func(v1[i], v2[i]))
    return arr    

def manhattan_dist1(v1, v2):
    ''' The Manhattan distance function computes the distance that would 
    be traveled to get from one data point to the other if a grid-like 
    path is followed. The Manhattan distance between two items is the sum 
    of the differences of their corresponding components.'''
    return sum(map(abs, map1(op.sub, v1, v2)))

def euclidean_dist(v1, v2):
    '''The Euclidean distance between points p and q is the length of the line segment connecting them'''
    return (sum(map(lambda x: x ** 2, map1(op.sub, v1, v2)))) ** 0.5

def mean(v1):
    ''' The mean is the average of the numbers: a calculated "central" value of a set of numbers. '''
    return sum(v1) / len(v1)

def sd(v):
    '''The Standard Deviation is a measure of how spread out numbers are.'''
    return (sum(map(lambda x : (x - mean(v)) ** 2, v)) / len(v)) ** 0.5

def pearson_correlation(v1, v2):
    '''In statistics, the Pearson correlation coefficient
    (PCC, pronounced /ˈpɪərsən/), also referred to as the Pearson's r, 
    Pearson product-moment correlation coefficient (PPMCC) or bivariate 
    correlation, is a measure of the linear correlation between two 
    variables arr1 and arr2'''

    arr1_standard_units = list(map(lambda x: (x - mean(v1)) / sd(v1), v1))
    arr2_standard_units = list(map(lambda x: (x - mean(v2)) / sd(v2), v2))
    return sum(map1(op.mul, arr1_standard_units, arr2_standard_units)) / len(v1)

def chebyshev_dist(v1, v2):
    return max(map(abs, map1(op.sub, v1, v2)))

