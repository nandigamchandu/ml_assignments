import unittest as ut
import distance as dist
class TestEuclidean(ut.TestCase):
    
    def test1(self):
        v1 = [74, 66, 68, 69, 73, 70, 60, 63, 67, 70, 70, 70, 75, 62, 75]
        v2 = [193, 133, 155, 147, 175, 128, 100, 128, 170, 182, 178, 118, 227, 115, 211]
        self.assertAlmostEqual(dist.euclidean_dist(v1,v2), 365.2752387)
    def test2(self):
        v1 = [74, 66, 68, 69, 73, 70, 60, 63, 67, 70, 70, 70, 75, 62, 75]
        v2 = [193, 133, 155, 147, 175, 128, 100, 128, 170, 182, 178, 118, 227, 115, 211]
        self.assertEqual(dist.euclidean_dist(v1, v2), 365.2752386)
    def test3(self):
        v1 = [74, 66, 68, 69, 73, 70, 60, 63, 67, 70, 70, 70, 75, 62, 75, 76]
        v2 = [193, 133, 155, 147, 175, 128, 100, 128, 170, 182, 178, 118, 227, 115, 211]
        with self.assertRaises(IndexError):
            dist.euclidean_dist(v1, v2)
    def test4(self):
        v1 = [74, 66, 68, 69, 73, 70, 60, 63, 67, 70, 70, 70, 75, 62, 75]
        v2 = [193, 133, 155, 147, 175, 128, 100, 128, 170, 182, 178, 118, 227, 115, 211]
        with self.assertRaises(IndexError):
            dist.euclidean_dist(v1, v2)
if __name__ == '__main__':
    ut.main()