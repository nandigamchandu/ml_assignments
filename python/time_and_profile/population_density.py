import pandas as pd

# function to read files
def read_files(population, area, statenames):
    stateabbrevs = pd.read_csv(statenames, index_col=['abbreviation'])
    stateareas = pd.read_csv(area, index_col= ['state'])
    statepopulation= pd.read_csv(population, index_col= ['state/region', 'ages'])
    infor = pd.merge(stateabbrevs, stateareas ,left_on='state', right_index=True)
    return statepopulation, infor

# function  to rank state based on population density
def population_density(statepopulation,infor, year):
    temp= statepopulation[statepopulation['year'] == year]
    temp1= temp.unstack()['population'].drop('under18', axis='columns')
    temp2= pd.merge(temp1, infor, left_index=True, right_index=True)
    temp2['rank']=temp2['total'] / infor['area (sq. mi)']
    return temp2.reset_index().set_index(['index', 'state', 'total', 'area (sq. mi)']).rank(method='first', ascending=False).sort_values('rank')

# main function 
def population_density_rank(population, area, statenames, year):
    statepopulation, infor = read_files(population, area, statenames)
    return population_density(statepopulation, infor, year)

# rank US states and territories by their 2010 population density
print(population_density_rank('state-population.csv', 'state-areas.csv', 'state-abbrevs.csv', 2010).head())

#                                                    rank
#index state                total     area (sq. mi)      
#DC    District of Columbia 605125.0  68              1.0
#NJ    New Jersey           8802707.0 8722            2.0
#RI    Rhode Island         1052669.0 1545            3.0
#CT    Connecticut          3579210.0 5544            4.0
#MA    Massachusetts        6563263.0 10555           5.0