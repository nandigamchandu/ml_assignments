import pandas as pd

def population_density_rank():
    pop= pd.read_csv('state-population.csv', index_col= ['year','ages'])
    abbrevs = pd.read_csv('state-abbrevs.csv', index_col=['state'])
    areas = pd.read_csv('state-areas.csv', index_col=['state'])
    pop= pop.loc[2010].loc['total'].set_index('state/region')
    temp1= pd.concat([abbrevs,areas], axis=1)
    temp1.loc['Puerto Rico', 'abbreviation'] = 'PR'
    temp2 = pd.merge(temp1, pop, left_on = 'abbreviation', right_index= True)
    temp2['density'] = temp2['population'] / temp2['area (sq. mi)']
    temp2.sort_values('density', ascending=False)
    return temp2