"""Exception Coding."""

import seaborn as sns
import numpy as np
# The try/except/else statement

# try:
#     statements  # Run this main action first
# except name1:
#     statements  # Run if name1 exception rise
# except (name2, name2):
#     statements  # Run any of the these exception occur
# except name4 as var:
#     statements  # Run if name4 is raised, assign instance raised to var
# except:
#     statements  # Run for all other exceptions raised
# else:
#     statements

sep = '-' * 45 + '\n'

print(sep + 'EXCEPTION RAISED AND CAUGHT')
try:
    x = 'spam'[99]
except IndexError:
    print('except run')
finally:
    print('finally run')
print('after run')


print(sep + 'NO EXCEPTION RAISED')
try:
    x = 'spam'[3]
except IndexError:
    print('except run')
finally:
    print('finally run')
print('after run')

print(sep + "NO EXCEPTION RAISED, WITH ELSE")
try:
    x = 'spam'[3]
except IndexError:
    print('except run')
else:
    print('else run')
finally:
    print('finally run')
print('after run')

print(sep + 'EXCEPTION RAISED BUT NOT CAUGHT')
try:
    x = 1 / 0
except IndexError:
    print('except run')
finally:
    print('finally run')
print('after run')

# The raise statement
# to trigger exception explicitly

raise IndexError  # Class (instance created)
raise IndexError()  # Instance (created in statement)

exc = IndexError()  # Create instance ahead of time
raise exc
excs = [IndexError, TypeError]
raise excs[0]


# Example: passes a exception class contructor
# arguments that become available in the hadler
class MyExc(Exception):
    """Use exception."""

    pass


try:
    raise MyExc('spam')
except MyExc as X:
    print(X)


# Scopes and try except variables
try:
    1 / 0
except Exception as x:
    print(x)

# note: localizes the exeception reference name to except block

X = 99
try:
    1 / 0
except Exception as X:
    print(X)
# note: 1) localizes and removes on exit
# 2) Because of this, you should generally use
# unique variable names in your try staement's

X = 99
{X for X in 'spam'}
X
# note: localizes only: not removed


# if we need to reference the exception instance after try statement,
# simply assign it to another name that won't be automatically removed
try:
    1 / 0
except Exception as x:
    print(x)
    saveit = x
print(saveit)

# Propagating Exceptions with raise
try:
    raise IndexError('spam')  # Exception remember arguments
except IndexError:
    print('propagating')
    raise                     # Reraise most recent exception

# Exception chaining: raise from
try:
    1 / 0
except Exception as E:
    raise TypeError('Bad') from E  # Eplicitly chained exceptions

try:
    1 / 0
except Exception:
    zero  # implicity chained exceptions


# Assert statement
# assert can be thought of as a conditional raise statement.

assert True, 'exception raise'  # exception raise is optional
assert False, 'exception raise'

raise AssertionError('exception raise')
# note: python -O main.py to run in optimized mode and disable assert.

# with/as Context Managers
# 1)with/as statement is alternative to common try/finally
# 2)with is in large part intended for specifying terminatio-time or cleanup
# activities that must run regerdless of wheather an exception occurs during a
# processing step.

# format
# with expression [as variable]:
#     with-block

with open('lumberjack.txt') as myfile:
    for line in myfile:
        print(line)

# after this with statement has run, the context management machinery guartees
# the file object referenced by myfile is automatically closed.

# similar effect using try/finally

myfile = open('lumberjack.txt')
try:
    for line in myfile:
        print(line)
finally:
    myfile.close()

# After with statement setting are restored to what it was before.
x = np.random.randn(100, 2)
sns.regplot(x[:, 0], x[:, 1], fit_reg=False)
with sns.axes_style("darkgrid"):
    sns.regplot(x[:, 0], x[:, 1], fit_reg=False)
sns.regplot(x[:, 0], x[:, 1], fit_reg=False)
# todo the same with try/finally, we need to save the context before
# and restore it manually after the nested block

# Multiple Context managers

# with A() as a, B() as b:
#      ...statement...

# equivalent to

# with A() as a:
#   with B() as b:
#       ...statements...

with open('text1.txt') as f1, open('text2.txt', 'w') as f2:
    for line in f1:
        f2.write(line.upper())

print(open('text2.txt').read())
