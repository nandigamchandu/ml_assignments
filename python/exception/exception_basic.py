"""Exception basics."""

# exceptions are processed by four statements.
# 1) try/except
# 2) try/finally
# 3) raise
# 4) assert
# 5) with/as

# Default exception handler


def fetcher(obj, index):
    """Function."""
    return obj[index]


x = 'spam'
fetcher(x, 3)

# catching Exceptions
try:
    fetcher(x, 4)
except IndexError:
    print('got exception')

# try statements not only catch exception,
# but also recover from them


def catcher():
    """Catcher."""
    try:
        fetcher(x, 4)
    except IndexError:
        print('got exception')
    print('continuing')


catcher()

# Raising exceptions
try:
    raise IndexError
except IndexError:
    print('got exeception')

# assert statement can be used to trigger Exceptions.
# assert False, 'exception'

# User-define Exceptions


class AlreadyGotOne(Exception):
    """User define exception."""

    pass


def grail():
    """Raise exception."""
    raise AlreadyGotOne


try:
    grail()
except AlreadyGotOne:
    print('got exception')


# Termination Actions

#  try/finally comination specifies termination actions that always execute
# "on the way out" regardless of whether an exception occurs in the
# try block or not

try:
    fetcher(x, 3)
finally:
    print('after fetch')

#  it is similar to
fetcher(x, 3)
print('after fetch')

# But if fetcher raise a exception print statement never execute


def after():
    """Termination."""
    try:
        fetcher(x, 4)
    finally:
        print('after fetch')
    print('after try?')


after()


def after():
    """Termination."""
    try:
        fetcher(x, 3)
    finally:
        print('after fetch')
    print('after try?')


after()

# The with/as statement runs an object's
# context management logic to guarantee that termination action occur

# Always close file on exit
with open('lumberjack.txt', 'w') as file:
    file.write('The larch!\n')
