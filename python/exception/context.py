"""The context managgement protocool."""

# The expression is evaluted, resulting in an object known as a context Manager
# that must have __enter__ and __exit__ methods.


class TraceBlock:
    """Context manager."""

    def message(self, arg):
        """Message."""
        print('running ' + arg)

    def __enter__(self):
        """Enter."""
        print('starting with block')
        return self

    def __exit__(self, exc_type, exc_value, exc_tb):
        """Exit."""
        if exc_type is None:
            print('exited normally\n')
        else:
            print('raise as exception!' + str(exc_type))
            return False


if __name__ == '__main__':
    with TraceBlock() as action:
        action.message('test1')
        print('reached')

    with TraceBlock() as action:
        action.message('test2')
        raise TypeError
        print('not reached')
