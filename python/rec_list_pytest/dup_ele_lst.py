"""Duplicate elements in the list."""


def dup_ele(lst):
    """Duplicate element."""
    if lst:
        res = []
        for i in lst:
            res = res + [i, i]
        return res


# Test cases for dup_ele functiom
def test_dup_ele_1():
    """Empty list as input."""
    assert dup_ele([]) is None


def test_dup_ele_2():
    """Single element list as input."""
    assert dup_ele(['a']) == ['a', 'a']


def test_dup_ele_3():
    """Two same elements list as input."""
    assert dup_ele(['a', 'a']) == ['a', 'a', 'a', 'a']


def test_dup_ele_4():
    """Two different elements in a list as input."""
    assert dup_ele(['a', 'b']) == ['a', 'a', 'b', 'b']


def test_dup_ele_5():
    """Multiple elements in a list as input."""
    x = [1, 'a', 'b', 3, 'c']
    res = [1, 1, 'a', 'a', 'b', 'b', 3, 3, 'c', 'c']
    assert dup_ele(x) == res
