"""Collect consecutive duplicates of list elements into sublists."""


def pair_con_same_ele(lst):
    """Collect consecutive duplicates of list elements into sublists."""
    result = []
    if not lst:
        return None
    result.append(lst[0:1])
    for i in range(1, len(lst)):
        if bool(set(lst[i:i + 1]) & set(result[-1:][0])):
            result[-1] = result[-1] + lst[i:i+1]
        else:
            result.append(lst[i:i+1])
    return result


# Test cases for pair_con_same_ele function
def test_pair_con_same_ele_1():
    """Empty list as input."""
    x = []
    assert pair_con_same_ele(x) is None


def test_pair_con_same_ele_2():
    """List having consecutive duplicates."""
    x = ['a', 'a', 'b', 'b', 'c', 'a', 'a']
    assert pair_con_same_ele(x) == [['a', 'a'], ['b', 'b'], ['c'], ['a', 'a']]


def test_pair_con_same_ele_3():
    """List with out consecutive duplicates.."""
    x = ['a', 'b', 'c', 'a']
    assert pair_con_same_ele(x) == [['a'], ['b'], ['c'], ['a']]
