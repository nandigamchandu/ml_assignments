"""Split a list into two parts; the length of the first part is given."""


def split(lst, nth):
    """Split at given position."""
    if lst:
        if len(lst) > nth and nth > 0:
            res = []
            res.append(lst[:nth])
            res.append(lst[nth:])
            return res
        else:
            return lst


# Testing split Function
def test_split_1():
    """Empty list as input."""
    assert split([], 1) is None


def test_split_2():
    """Single element as input split at position."""
    assert split(['a'], 0) == ['a']


def test_split_3():
    """Single element as input split at position."""
    assert split(['a'], 1) == ['a']


def test_split_4():
    """Two elements as input split at zero position."""
    assert split(['a', 'a'], 0) == ['a', 'a']


def test_split_5():
    """Two elements as input split at first position."""
    assert split(['a', 'a'], 1) == [['a'], ['a']]


def test_split_6():
    """Two elements as input split at more than len of list."""
    assert split(['a', 'a'], 3) == ['a', 'a']


def test_split_7():
    """More elements in list as input."""
    res = [['a', 'd', 'c'], ['b', 'e']]
    assert split(['a', 'd', 'c', 'b', 'e'], 3) == res
