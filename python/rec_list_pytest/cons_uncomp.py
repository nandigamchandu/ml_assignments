"""Uncompressed the given list."""


def cons_uncom(lst):
    """Uncompres."""
    if lst:
        res = []
        for i in lst:
            if type(i) == tuple:
                count = i[0]
                while count > 0:
                    res.append(i[1])
                    count -= 1
            else:
                res.append(i)
        return res


def test_cons_uncom_1():
    """Empty list a input."""
    assert cons_uncom([]) is None


def test_cons_uncom_2():
    """Testing."""
    x = [(3, 'a'), 'b', (2, 'c'), 'd']
    res = ['a', 'a', 'a', 'b', 'c', 'c', 'd']
    assert cons_uncom(x) == res


def test_cons_uncom_3():
    """Testing."""
    x = ['a', 'b', 'c', 'd']
    assert cons_uncom(x) == x


def test_cons_uncom_4():
    """Testing."""
    x = [(3, 'a'), (2, 'c')]
    res = ['a', 'a', 'a', 'c', 'c']
    assert cons_uncom(x) == res
