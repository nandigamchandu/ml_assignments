"""Find the last element of a list using recursion."""


def last_ele_list(lst):
    """Return last element of list."""
    if lst:
        if len(lst) == 1:
            return lst[0]
        return last_ele_list(lst[1:])


# Test cases for last_ele_list function
def test_last_ele_list_1():
    """Empty list as input."""
    x = []
    assert last_ele_list(x) is None


def test_last_ele_list_2():
    """Single element list as input."""
    x = [1]
    assert last_ele_list(x) == 1


def test_last_ele_list_3():
    """Two elements list as input."""
    x = [1, 2]
    assert last_ele_list(x) == 2


def test_last_ele_list_4():
    """More elements list as input."""
    x = [1, 2, 3, 4, 5]
    assert last_ele_list(x) == 5
