"""Implementing stack in python using LinkedList."""

import mylinkedlist as ll


class Stack:
    """Stack class implementation."""

    def __init__(self, v):
        """Construct stack."""
        self.cell = ll.LinkedCell(v)

    def push(self, v):
        """Add value at top of stack."""
        self.cell.append(v)
        return self

    def top(self):
        """Return value at top of the stack."""
        top_pos = self.cell.length - 1
        return self.cell.seek(top_pos)

    def pop(self):
        """Return and remove value at top of the stack."""
        top_pos = self.cell.length - 1
        x = self.cell.seek(top_pos)
        self.cell.remove(top_pos)
        return x


class TestStack(object):
    """Test suite for Stack."""

    def test_push(self):
        """Test push() functionality."""
        s1 = Stack(5)  # position 0
        assert s1.cell.value == 5
        s1.push(10)  # position 1
        assert s1.cell.next.value == 10
        s1.push(15)  # position 2
        assert s1.cell.next.next.value == 15
        s1.push(20)  # position 3
        assert s1.cell.next.next.next.value == 20
        s1.push(10)  # position 4
        assert s1.cell.next.next.next.next.value == 10

    def test_top(self):
        """Test top() functionality."""
        s1 = Stack(6)
        assert s1.top() == 6
        s1.push(45)
        assert s1.top() == 45
        s1.push(50)
        assert s1.top() == 50
        s1.push(30)
        assert s1.top() == 30
        s1.push(40)
        assert s1.top() == 40

    def test_pop(self):
        """Test pop() functionality."""
        s1 = Stack(89)
        s1.push(45)
        assert s1.pop() == 45
        assert s1.top() == 89
        s1.push(78)
        s1.push(45)
        assert s1.pop() == 45
        assert s1.top() == 78


def main():
    """Try to print."""
    s1 = Stack(5)
    s1.push(6)
    s1.push(8)
    s1.push(10)
    print('head->10,8,6,5')
    print('pop value is: {}'.format(s1.pop()))
    print('top value is: {}'.format(s1.top()))


main()
