"""Find the last but one element of a list using recursion."""


def last_but_one(lst):
    """Return last element of list."""
    if lst and len(lst) != 1:
        if len(lst) == 2:
            return lst[0]
        return last_but_one(lst[1:])


# Test cases for last_ele_list function
def test_last_but_one_1():
    """Empty list as input."""
    x = []
    assert last_but_one(x) is None


def test_last_but_one_2():
    """Single element list as input."""
    x = [1]
    assert last_but_one(x) is None


def test_last_but_one_3():
    """Two elements list as input."""
    x = [1, 2]
    assert last_but_one(x) == 1


def test_last_but_one_4():
    """More elements list as input."""
    x = [1, 2, 3, 4, 5]
    assert last_but_one(x) == 4
