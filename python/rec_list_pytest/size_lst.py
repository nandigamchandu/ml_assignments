"""Find the number of elements of a list using recursion."""


def size_lst(lst):
    """Return size of list."""
    if not lst:
        return 0
    return 1 + size_lst(lst[1:])


# Test case for size_lst function
def test_size_lst_1():
    """Empty list as input."""
    x = []
    assert size_lst(x) == 0


def test_size_lst_2():
    """One element is list as input."""
    x = [1]
    assert size_lst(x) == 1


def test_size_lst_3():
    """Two elements in list as input."""
    x = [1, 'a']
    assert size_lst(x) == 2


def test_size_lst_4():
    """More elements in list as input."""
    x = [1, 2, 3, 4, 5]
    assert size_lst(x) == 5
