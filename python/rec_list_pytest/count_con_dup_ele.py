"""Consecutive duplicates of elements are encoded as lists (N E)."""
import pair_con_same_ele as pcd


def count_con_same_ele(lst):
    """Consecutive duplicates of elements are encoded as lists (N E).

    where N is the number of duplicates of the element E
    """
    res = []
    if not lst:
        return None
    x = pcd.pair_con_same_ele(lst)
    for i in x:
        res.append((len(i), i[0]))
    return res


# Test case for count_con_same_ele function
def test_count_con_same_ele_1():
    """Empty list as input."""
    x = []
    assert count_con_same_ele(x) is None


def test_count_con_same_ele_2():
    """List having consecutive duplicates."""
    x = ['a', 'a', 'b', 'b', 'c', 'a', 'a']
    assert count_con_same_ele(x) == [(2, 'a'), (2, 'b'), (1, 'c'), (2, 'a')]


def test_count_con_same_ele_3():
    """List with out consecutive duplicates.."""
    x = ['a', 'b', 'c', 'a']
    assert count_con_same_ele(x) == [(1, 'a'), (1, 'b'), (1, 'c'), (1, 'a')]
