"""Replicate the elements of a list a given number of times."""


def rep_ele_ntimes(lst, ntimes):
    """Replocate the elements of a list ntimes."""
    if lst:
        res = []
        for i in lst:
            count = ntimes
            while count > 0:
                res.append(i)
                count -= 1
        return res


# Testing rep_ele_ntimes function
def test_rep_ele_ntimes_1():
    """Empty list as input."""
    assert rep_ele_ntimes([], 3) is None


def test_rep_ele_ntimes_2():
    """Single element as input."""
    assert rep_ele_ntimes(['a'], 4) == ['a', 'a', 'a', 'a']


def test_rep_ele_ntimes_3():
    """Two elements as input."""
    assert rep_ele_ntimes(['a', 'b'], 2) == ['a', 'a', 'b', 'b']


def test_rep_ele_ntimes_4():
    """More elements in list."""
    x = ['a', 1, 'b', 'c']
    res = ['a', 'a', 1, 1, 'b', 'b', 'c', 'c']
    assert rep_ele_ntimes(x, 2) == res
