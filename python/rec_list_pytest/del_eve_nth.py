"""Delete every nth element in a given list."""
import pytest


# %%
def del_eve_nth(lst, nth):
    """Delete nth element."""
    if lst:
        res = []
        for i in range(len(lst)):
            if (i + 1) % nth != 0:
                res.append(lst[i])
        return res


# Testing del_nth_ele
def test_del_eve_nth_1():
    """Empty list as input."""
    assert del_eve_nth([], 2) is None


def test_del_eve_nth_2():
    """Every second element drop."""
    assert del_eve_nth([1, 2, 3, 4, 5, 6], 2) == [1, 3, 5]


def test_del_eve_nth_3():
    """Every third element drop."""
    x = [1, 2, 3, 4, 5, 6, 7]
    assert del_eve_nth(x, 3) == [1, 2, 4, 5, 7]


def test_del_eve_nth_4():
    """Dropping every zeroth element."""
    with pytest.raises(ZeroDivisionError):
        del_eve_nth([1, 2, 3, 4, 5], 0)
