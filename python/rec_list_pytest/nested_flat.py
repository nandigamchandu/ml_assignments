"""Flatten a nested list structure."""


def flatten(lst):
    """Return plane list."""
    if lst:
        result = []
        for i in lst:
            if type(i) == list:
                result = result + flatten(i)
            else:
                result.append(i)
        return result


# Testing flatten function
class TestClass(object):
    """Test Class."""

    def test_flatten_1(self):
        """Empty list as input."""
        x = []
        assert flatten(x) is None

    def test_flatten_2(self):
        """List with out nested list structure."""
        x = [1, 'a', 3, 'a', 5, 6]
        assert flatten(x) == [1, 'a', 3, 'a', 5, 6]

    def test_flatten_3(self):
        """List with nested list structure."""
        x = [1, 2, [3, 4, [5, 6]], 7]
        assert flatten(x) == [1, 2, 3, 4, 5, 6, 7]

    def test_flatten_4(self):
        """List with list as element."""
        x = [1, 'a', [2, 3, 'b'], [1], ['a', 'b', 'c']]
        assert flatten(x) == [1, 'a', 2, 3, 'b', 1, 'a', 'b', 'c']
