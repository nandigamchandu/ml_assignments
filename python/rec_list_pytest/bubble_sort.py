"""Bubble sort."""


def bubble_sort(lst):
    """Bubble sort."""
    if lst:
        for j in range(len(lst)):
            for i in range(1, len(lst)):
                if lst[i-1] > lst[i]:
                    temp = lst[i-1]
                    lst[i-1] = lst[i]
                    lst[i] = temp
        return lst


# test cases for bubble sort fuction
def test_bubble_sort_1():
    """Empty lst as input."""
    assert bubble_sort([]) is None


def test_bubble_sort_2():
    """Two same elements list as input."""
    assert bubble_sort([9, 9]) == [9, 9]


def test_bubble_sort_3():
    """Two different elements list as input."""
    assert bubble_sort([1, 9]) == [1, 9]


def test_bubble_sort_4():
    """Sortted list as input."""
    x = [1, 2, 3, 4, 5]
    assert bubble_sort(x) is x


def test_bubble_sort_5():
    """Unsortted list as input."""
    x = [5, 3, 1, 4, 2]
    res = [1, 2, 3, 4, 5]
    assert bubble_sort(x) == res


def test_bubble_sort_6():
    """Repeating elements in list as input."""
    x = [10, 3, 3, 6, 2, 9, 9, 1]
    res = [1, 2, 3, 3, 6, 9, 9, 10]
    assert bubble_sort(x) == res
