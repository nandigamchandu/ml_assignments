"""Linked List implementation in Python."""


class LinkedCell:
    """Node implementation for linked list."""

    def __init__(self, v, n=-1):
        """Construct for LinkedCell."""
        self.value = v
        self.next = n
        self.length = 1

    def prepend(self, v):
        """Prepend given value to the linked list."""
        newc = LinkedCell(v)
        newc.next = self
        newc.length = self.length + 1
        return newc

    def len(self):
        """Calculate length of the linked list."""
        length = 0
        n = self
        while n != -1:
            length = length + 1
            n = n.next
        return length

    def seek(self, i):
        """Return the value at index."""
        n = self
        if i < 0 or i >= self.length:
            return -1
        while i > 0:
            i = i - 1
            n = n.next
        return n.value

    def traverse(self):
        """Traverse the linked list and print node contents."""
        n = self
        while n != -1:
            (print('value: {}, next: {}, length: {}'
                   .format(n.value, n.next, n.length)))
            n = n.next

    def simple_traverse(self):
        """Traverse and print only node values."""
        n = self
        while n != -1:
            print('{} -> '.format(n.value), end='')
            n = n.next
        print('End')

    def assign(self, nv, i):
        """Assign value at index."""
        n = self
        if i < 0 or i >= self.length:
            return -1
        while i > 0:
            i = i - 1
            n = n.next
        n.value = nv

    def insert(self, v, i):
        """Insert at index."""
        n = self
        ncell = LinkedCell(v)
        if i < 0 or i >= self.length:
            return -1
        while i > 1:
            i = i - 1
            n.length += 1
            n = n.next
        ncell.next = n.next
        ncell.length = n.length
        n.length += 1
        n.next = ncell

    def remove(self, i):
        """Remove at index."""
        n = self
        if i < 0 or i >= self.length:
            return -1
        if i == 0:
            n.length -= 1
            n.value = n.next.value
            n.next = n.next.next
        else:
            while i > 1:
                i = i - 1
                n.length -= 1
                n = n.next
            n.next = n.next.next
            n.length = n.length - 1

    def append(self, v):
        """Append to linked list."""
        ncell = LinkedCell(v)
        n = self
        len = n.length
        while len > 1:
            n.length += 1
            n = n.next
            len = len - 1
        n.length += 1
        n.next = ncell


class TestLinkedCell(object):
    """Test suite for LinkedCell."""

    def test_insert(self):
        """Test insert() functionality."""
        l1 = LinkedCell(11)
        l1 = l1.prepend(76)
        l1 = l1.prepend(96)
        l1 = l1.prepend(2)
        l1 = l1.prepend(55)
        l1.insert(108, 3)
        assert l1.length == 6
        assert l1.seek(3) == 108
        l1.insert(98, 5)
        assert l1.length == 7
        assert l1.seek(5) == 98
        assert l1.insert(78, -9) == -1
        assert l1.insert(8, 98) == -1

    def test_remove(self):
        """Test remove() functionality."""
        l1 = LinkedCell(11)
        l1 = l1.prepend(76)
        l1 = l1.prepend(96)
        l1 = l1.prepend(2)
        l1 = l1.prepend(55)
        l1 = l1.prepend(108)
        l1.remove(0)
        assert l1.length == 5
        assert l1.seek(0) == 55
        l1.remove(2)
        assert l1.length == 4
        assert l1.seek(2) == 76
        l1.remove(3)
        assert l1.length == 3
        assert l1.seek(3) == -1
        assert l1.seek(l1.len()-1) == 76
        assert l1.remove(-7) == -1
        assert l1.remove(78) == -1

    def test_append(self):
        """Test append() functionality."""
        l1 = LinkedCell(11)
        l1 = l1.prepend(76)
        l1 = l1.prepend(96)
        l1 = l1.prepend(2)
        l1 = l1.prepend(55)
        l1.append(17)
        assert l1.length == 6
        l1.append(48)
        assert l1.seek(l1.len()-2) == 17
        assert l1.seek(l1.len()-1) == 48


def main():
    """Try to print."""
    l1 = LinkedCell(11)
    l1 = l1.prepend(76)
    l1 = l1.prepend(96)
    l1 = l1.prepend(2)
    l1 = l1.prepend(55)
    l1.simple_traverse()
    l1.append(45)
    print('append 45')
    l1.simple_traverse()
    l1.remove(1)
    print('remove cell at position 1')
    l1.simple_traverse()
    l1.insert(47, 4)
    print('insert 47 at index 4')
    l1.simple_traverse()
    l1.assign(99, 1)
    print('assign 99 at index 1')
    l1.simple_traverse()


main()
