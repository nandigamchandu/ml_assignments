"""Implementation of insertion sort."""


def insertion_sort(lst):
    """Sort given list using insertion algorithm."""
    if lst:
        for i in range(1, len(lst)):
            x = lst[i]
            insert_pos = i
            for j in range(i-1, -1, -1):
                if lst[j] > x:
                    lst[j+1] = lst[j]
                    insert_pos = j
            lst[insert_pos] = x
        return lst


def test_insertion_sort_1():
    """Empty list as input."""
    assert insertion_sort([]) is None


def test_insertion_sort_2():
    """Two same element list as input."""
    assert insertion_sort([5, 5]) == [5, 5]


def test_insertion_sort_3():
    """Two different element list as input."""
    assert insertion_sort([99, 50]) == [50, 99]


def test_insertion_sort_4():
    """Unsoerted list with repeating, and -ve elements as input."""
    x = [5, 8, 45, 8, -1, -10, 81, 26, -10, 108, -50]
    res = [-50, -10, -10, -1, 5, 8, 8, 26, 45, 81, 108]
    assert insertion_sort(x) == res


def test_insertion_sort_5():
    """Sorted list as input."""
    x = [-46, -24, -24, -1, 0, 10, 19, 47, 88, 88, 99, 108]
    assert insertion_sort(x) == x


def test_insertion_sort_6():
    """Nearly sorted list as input."""
    x = [-24, -1, 0, 10, 47, 19, 88, 88, 99]
    res = [-24, -1, 0, 10, 19, 47, 88, 88, 99]
    assert insertion_sort(x) == res


def test_insertion_sort_7():
    """Insertion sort."""
    x = [4, 5, 4, 4, 4, 4, 5, 5, 4]
    res = [4, 4, 4, 4, 4, 4, 5, 5, 5]
    assert insertion_sort(x) == res
