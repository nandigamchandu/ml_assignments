"""Eliminate consecutive duplicates of list elements."""


def del_con_dup(lst):
    """Remove consecutive duplicates."""
    result = []
    if lst:
        for i in range(len(lst)):
            if not bool(set(lst[i]) & set(result[-1:])):
                result.append(lst[i])
        return result


# Testing del_con_dup Function
def test_del_con_dup_1():
    """Empty list as input."""
    x = []
    assert del_con_dup(x) is None


def test_del_con_dup_2():
    """List having duplicate elements as input."""
    x = ['a', 'a', 'a', 'b', 'b', 'c', 'a', 'a']
    assert del_con_dup(x) == ['a', 'b', 'c', 'a']


def test_del_con_dup_3():
    """List with out duplicates as input."""
    x = ['a', 'b', 'c', 'a', 'd']
    assert del_con_dup(x) == ['a', 'b', 'c', 'a', 'd']
