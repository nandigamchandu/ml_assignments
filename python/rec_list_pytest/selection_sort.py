"""Implementation of selection sort."""


def selection_sort(lst):
    """Sort list using selection sort."""
    if lst:
        for i in range(len(lst)):
            max_pos = 0
            for j in range(len(lst)-i):
                if lst[max_pos] < lst[j]:
                    max_pos = j
            temp = lst[max_pos]
            lst[max_pos] = lst[j]
            lst[j] = temp
        return lst


def test_selection_sort_1():
    """Empty list as input."""
    assert selection_sort([]) is None


def test_selection_sort_2():
    """Single element list as input."""
    assert selection_sort([99]) == [99]


def test_selection_sort_3():
    """Two different elements list as input."""
    assert selection_sort([89, 33]) == [33, 89]


def test_selection_sort_4():
    """Two same element list as input."""
    assert selection_sort([99, 99]) == [99, 99]


def test_selection_sort_5():
    """Unsorted list as input."""
    x = [-8, 99, 49, 2, 70, 5, 12, 2, 108]
    res = [-8, 2, 2, 5, 12, 49, 70, 99, 108]
    assert selection_sort(x) == res


def test_selection_sort_6():
    """Sorted list as input."""
    x = [-90, -45, 0, 0, 9, 54, 78, 89, 90]
    assert selection_sort(x) == x


def test_selection_sort_7():
    """Nearly sorted list as input."""
    x = [1, 5, 5, 7, 9, 8, 15, 20]
    res = [1, 5, 5, 7, 8, 9, 15, 20]
    assert selection_sort(x) == res
