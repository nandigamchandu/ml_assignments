"""Find out whether a list is a palindrome."""


def palindrome(lst):
    """Return given lst is palindrom or not."""
    if not lst:
        return False
    elif lst[0:] == lst[-1::-1]:
        return True
    return False


# Testing palindrome Function
def test_palindrome_1():
    """Empty list as input."""
    x = []
    assert palindrome(x) is False


def test_palindrome_2():
    """One element list as input."""
    x = [1]
    assert palindrome(x) is True


def test_palindrome_3():
    """Two different element list as input."""
    x = [1, 2]
    assert palindrome(x) is False


def test_palindrome_4():
    """Two same element list as input."""
    x = [1, 1]
    assert palindrome(x) is True


def test_palindrome_5():
    """Testing palindrome."""
    x = [1, 2, 3, 4, 5]
    assert palindrome(x) is False


def test_palindrome_6():
    """Testing palindrome."""
    x = [1, 2, 3, 2, 1]
    assert palindrome(x) is True


def test_palindrome_7():
    """Near to palindrome."""
    x = [1, 'a', 2, 'b', 'c', 'd', 2, 'a', 1]
    assert palindrome(x) is False
