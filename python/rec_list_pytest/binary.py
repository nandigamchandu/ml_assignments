"""Binary search."""


def binary_search(lst, t):
    """Function."""
    lt = 0
    rt = len(lst) - 1
    m = 0
    if lst:
        while lst[m] != t:
            if rt < lt:
                return False
            m = (lt + rt) // 2
            if lst[m] < t:
                lt = m + 1
            elif lst[m] > t:
                rt = m - 1
        return True


# testing binary search function
def test_binary_search_1():
    """Empty list as input."""
    assert binary_search([], 'c') is None


def test_binary_search_2():
    """List having target element."""
    x = [1, 5, 9, 16, 18, 22, 26, 32]
    assert binary_search(x, 9) is True


def test_binary_search_3():
    """List without target element."""
    x = [1, 5, 9, 16, 18, 22, 26, 32]
    assert binary_search(x, 10) is False


def test_binary_search_4():
    """Alphabets as elements of list."""
    x = ['a', 'c', 'e', 'g', 'h', 'm', 'u', 'z']
    assert binary_search(x, 'h') is True
