"""Implementing stack in python using LinkedList."""

import mylinkedlist as ll


class Stack:
    """Stack class implementation."""

    def __init__(self, v):
        """Construct stack."""
        self.cell = ll.LinkedCell(v)

    def push(self, v):
        """Add value at top of stack."""
        self.cell = self.cell.prepend(v)
        return self

    def top(self):
        """Return value at top of the stack."""
        return self.cell.seek(0)

    def pop(self):
        """Return and remove value at top of the stack."""
        x = self.cell.seek(0)
        self.cell = self.cell.remove(0)
        return x


class TestStack(object):
    """Test suite for Stack."""

    def test_push(self):
        """Test push() functionality."""
        s1 = Stack(5)
        s1.push(10)
        s1.push(15)
        s1.push(20)
        s1.push(10)
        assert s1.cell.value == 10
        assert s1.cell.next.value == 20

    def test_top(self):
        """Test top() functionality."""
        s1 = Stack(6)
        s1.push(45)
        s1.push(50)
        s1.push(30)
        s1.push(40)
        assert s1.top() == 40
        assert s1.cell.value == 40

    def test_pop(self):
        """Test pop() functionality."""
        s1 = Stack(89)
        s1.push(45)
        s1.push(78)
        s1.push(60)
        s1.push(25)
        assert s1.pop() == 25
        assert s1.top() == 60


def main():
    """Test Stack."""
    s1 = Stack(5)
    s1.push(6)
    s1.push(8)
    s1.push(10)
    s1.pop()
    print(s1.pop())


main()
