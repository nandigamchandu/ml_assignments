"""Implementing queue in python."""

import mylinkedlist as ll


class Queue:
    """Queue class implementation."""

    def __init__(self, v):
        """Contruct for queue."""
        self.cell = ll.LinkedCell(v)

    def push(self, v):
        """Add value at top of stack."""
        self.cell = self.cell.prepend(v)
        return self

    def pop(self):
        """Return and remove value at front of the queue."""
        len = self.cell.length
        x = self.cell.seek(len-1)
        self.cell.remove(len-1)
        return x

    def head(self):
        """Return value at front of the queue."""
        len = self.cell.length
        return self.cell.seek(len-1)

    def tail(self):
        """Return value at tail of the queue."""
        return self.cell.seek(0)


class TestQueue(object):
    """Test suit for Queue."""

    def test_push(self):
        """Test push functionality."""
        q1 = Queue(5)
        q1.push(45)
        q1.push(50)
        q1.push(30)
        q1.push(20)
        assert q1.cell.value == 20
        assert q1.cell.next.value == 30

    def test_head(self):
        """Test head functionality."""
        q1 = Queue(45)
        q1.push(65)
        q1.push(78)
        q1.push(17)
        q1.push(74)
        assert q1.head() == 45

    def test_tail(self):
        """Test tail functionality."""
        q1 = Queue(98)
        q1.push(77)
        q1.push(18)
        q1.push(5)
        q1.push(84)
        assert q1.tail() == 84

    def test_pop(self):
        """Test pop functionality."""
        q1 = Queue(87)
        q1.push(26)
        q1.push(59)
        q1.push(19)
        q1.push(17)
        assert q1.pop() == 87


def main():
    """Check queue."""
    q1 = Queue(80)
    q1.push(7)
    q1.push(9)
    q1.push(89)
    q1.push(45)
    print('front->80, 7, 9, 89, 45 <-tail')
    print('head : {}'.format(q1.head()))
    print('tail : {}'.format(q1.tail()))
    print('pop : {}'.format(q1.pop()))
    print('front->7, 9, 89, 45<-tail')
    print('head : {}'.format(q1.head()))
    print('tail : {}'.format(q1.tail()))


main()
