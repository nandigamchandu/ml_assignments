"""Count consecutive duplicates elements only."""

import count_con_dup_ele as ccd


def count_only_con_dup(lst):
    """C."""
    if lst:
        res = []
        x = ccd.count_con_same_ele(lst)
        for i in x:
            if i[0] == 1:
                res.append(i[1])
            else:
                res.append(i)
        return res


# Testing count_only_con_ele function
def test_count_only_con_dup_1():
    """Empty list as input."""
    x = []
    assert count_only_con_dup(x) is None


def test_count_only_con_dup_2():
    """One element in list as input."""
    x = ["a"]
    assert count_only_con_dup(x) == ["a"]


def test_count_only_con_dup_3():
    """Two same elements in list as input."""
    x = ["a", "a"]
    assert count_only_con_dup(x) == [(2, "a")]


def test_count_only_con_dup_4():
    """Two differnt elements in list as input."""
    x = ["b", "a"]
    assert count_only_con_dup(x) == x


def test_count_only_con_dup_5():
    """Testing."""
    x = ["a", "a", "a", "b", "c", "b", "b", "c"]
    res = [(3, "a"), "b", "c", (2, "b"), "c"]
    assert count_only_con_dup(x) == res
