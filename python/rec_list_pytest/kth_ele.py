"""Find the K'th element of a list using recursion."""


def kth_ele(lst, kth):
    """Return Kth element in list."""
    if not lst or len(lst) < kth:
        return None
    if len(lst) == kth:
        return lst[-1]
    return kth_ele(lst[:-1], kth)


# Testing kyh_ele Function
def test_kth_ele_1():
    """Empty list as input."""
    x = []
    assert kth_ele(x, 3) is None


def test_kth_ele_2():
    """One element in list as input."""
    x = [3]
    assert kth_ele(x, 1) == 3


def test_kth_ele_3():
    """Two elements in list as input ."""
    x = [1, 'a']
    assert kth_ele(x, 2) == 'a'


def test_kth_ele_4():
    """If kth value is greater than length of list."""
    x = [1, 5, 'a', 8]
    assert kth_ele(x, 6) is None


def test_kth_ele_5():
    """More elements in list as input."""
    x = [1, 'a', 3, 4, 'c']
    assert kth_ele(x, 4) == 4
