"""quick sort algorithm."""

import numpy as np


def quicksort_r(lst, ll, r):
    """Quicksort recursion function."""
    i, j = ll, r-1
    ti, tj = i, j
    while j >= i:
        if lst[i] < lst[r]:
            i += 1
        if lst[j] >= lst[r]:
            j -= 1
        if i == ti and j == tj:
            lst[i], lst[j] = lst[j], lst[i]
        ti, tj = i, j
    if ll < r and (i <= r or j < ll):
        lst[i], lst[r] = lst[r], lst[i]
        quicksort_r(lst, ll, i-1)
        quicksort_r(lst, i+1, r)


def quicksort(lst):
    """Wrap function."""
    ll = 0
    r = len(lst) - 1
    quicksort_r(lst, ll, r)


# Test case for quicksort
def test_quicksort_1():
    """Empty list as input."""
    x = []
    quicksort(x)
    assert x == []


def test_quicksort_2():
    """Single element list as input."""
    x = [11]
    quicksort(x)
    assert x == [11]


def test_quicksort_3():
    """Same two elements list as input."""
    x = [3, 3]
    quicksort(x)
    assert x == [3, 3]


def test_quicksort_4():
    """Unsorted two elements list as input."""
    x = [45, 27]
    quicksort(x)
    assert x == [27, 45]


def test_quicksort_5():
    """Sorted two elements list as input."""
    x = [27, 61]
    quicksort(x)
    assert x == [27, 61]


def test_quicksort_6():
    """Sorted list as input."""
    x = [1, 2, 3, 4, 5, 6, 7, 8]
    res = [1, 2, 3, 4, 5, 6, 7, 8]
    quicksort(x)
    assert x == res


def test_quicksort_7():
    """Sorted in descending order list as input."""
    x = [52, 51, 50, 49, 48, 47, 46, 45]
    res = sorted([52, 51, 50, 49, 48, 47, 46, 45])
    quicksort(x)
    assert x == res


def test_quicksort_8():
    """Unsorted list as input."""
    x = np.random.randint(-5, 20, 20).tolist()
    res = sorted(x)
    quicksort(x)
    assert x == res


def test_quicksort_9():
    """Nearly sorted list as input."""
    x = [22, 25, 26, 29, 32, 35, 38, 36]
    res = [22, 25, 26, 29, 32, 35, 36, 38]
    quicksort(x)
    assert x == res


def test_quicksort_10():
    """All same elements in list as input."""
    x = [11, 11, 11, 11, 11]
    res = [11, 11, 11, 11, 11]
    quicksort(x)
    assert x == res
