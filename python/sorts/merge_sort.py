"""Merge sort algorithm."""

import numpy as np


def mergesort2(lst, l1, m, r):
    """Merge."""
    if l1 == m:
        lst1 = [lst[l1]]
        lst2 = [lst[r]]
    else:
        lst1 = lst[l1:m+1]
        lst2 = lst[m+1:r+1]
    i = 0
    j = 0
    lst3 = []
    while i < len(lst1) and j < len(lst2):
        if lst1[i] > lst2[j]:
            lst3.append(lst2[j])
            j += 1
        else:
            lst3.append(lst1[i])
            i += 1
    if i >= len(lst1):
        lst3 = lst3 + lst2[j:]
    else:
        lst3 = lst3 + lst1[i:]
    i = 0
    for ii in range(l1, r+1):
        lst[ii] = lst3[i]
        i += 1


def mergesort1(lst, l1=None, r=None):
    """Split."""
    if l1 is None:
        l1 = 0
        r = len(lst) - 1
    if l1 < r:
        m = int((l1+r)/2)
        mergesort1(lst, l1, m)
        mergesort1(lst, m+1, r)
        mergesort2(lst, l1, m, r)


# Test case for merge sort
def test_mergesort_1():
    """Empty list as input."""
    x = []
    mergesort1(x)
    assert x == []


def test_mergesort_2():
    """Unsorted(asc) two element list as input."""
    x = [45, 27]
    mergesort1(x)
    assert x == [27, 45]


def test_mergesort_3():
    """Sorted(asc) two element list as input."""
    x = [5, 41]
    mergesort1(x)
    assert x == [5, 41]


def test_mergesort_4():
    """Unsorted three elements list as input."""
    x = [94, 11, 47]
    mergesort1(x)
    assert x == [11, 47, 94]


def test_mergesort_5():
    """Sorted three elements list as input."""
    x = [11, 48, 76]
    mergesort1(x)
    assert x == [11, 48, 76]


def test_mergesort_6():
    """Repeating elements present in list."""
    x = [45, 1, 8, 69, 11, 5, 11, 6, 11]
    res = sorted(x)
    mergesort1(x)
    assert x == res


def test_mergesort_7():
    """Unsorted elements list as input."""
    x = np.random.randint(1, 48, 20).tolist()
    result = sorted(x)
    mergesort1(x)
    assert x == result


def test_mergesort_8():
    """Negative value elements present in list."""
    x = np.random.randint(-110, 48, 10).tolist()
    res = sorted(x)
    mergesort1(x)
    assert x == res


def test_mergesort_9():
    """All same elements in list."""
    x = [11, 11, 11, 11, 11, 11]
    res = [11, 11, 11, 11, 11, 11]
    mergesort1(x)
    assert x == res


def test_mergesort_10():
    """Nearly sorted list as input."""
    x = [1, 2, 3, 4, 5, 6, 8, 7, 9]
    res = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    mergesort1(x)
    assert x == res
