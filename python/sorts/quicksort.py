"""Quick sort."""

import numpy as np


def quick_sort(lst, s=None, e=None):
    """Partition."""
    if s is None or e is None:
        s = 0
        e = len(lst)
    pvt = s  # pivot index
    lst1 = []
    lst2 = []
    if s != e:
        for i in range(s, e):
            if lst[pvt] > lst[i]:
                lst1.append(lst[i])
            else:
                lst2.append(lst[i])
        lst[s:e] = lst1+lst2
        quick_sort(lst, s, s+len(lst1))
        quick_sort(lst, s+len(lst1)+1, s+len(lst1)+len(lst2))


# Test case for Quick sort
def test_quicksort_1():
    """Empty list as input."""
    x = []
    quick_sort([])
    assert x == []


def test_quicksort_2():
    """Single elements list as input."""
    x = [11]
    quick_sort(x)
    assert x == [11]


def test_quicksort_3():
    """Unsorted two element list as input."""
    x = [45, 27]
    quick_sort(x)
    assert x == [27, 45]


def test_quicksort_4():
    """Sorted two element list as input."""
    x = [45, 99]
    quick_sort(x)
    assert x == [45, 99]


def test_quicksort_5():
    """Sorted three elements list as input."""
    x = [11, 48, 76]
    quick_sort(x)
    assert x == [11, 48, 76]


def test_quicksort_6():
    """Repeating elements present in list."""
    x = [45, 1, 8, 69, 11, 5, 11, 6, 11]
    res = sorted(x)
    quick_sort(x)
    assert x == res


def test_quicksort_7():
    """Unsorted elements list as input."""
    x = np.random.randint(1, 48, 20).tolist()
    result = sorted(x)
    quick_sort(x)
    assert x == result


def test_quicksort_8():
    """Negative value elements present in list."""
    x = np.random.randint(-110, 2, 10).tolist()
    res = sorted(x)
    quick_sort(x)
    assert x == res


def test_quicksort_9():
    """All same elements in list."""
    x = [11, 11, 11, 11, 11, 11]
    res = [11, 11, 11, 11, 11, 11]
    quick_sort(x)
    assert x == res


def test_quicksort_10():
    """Nearly sorted list."""
    x = [1, 2, 3, 4, 5, 6, 8, 7, 9]
    res = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    quick_sort(x)
    assert x == res
