"""Iterator and comprehensions."""

import os
import functools
import operator

# for loop can work on any sequence
for x in [1, 2, 3, 4]:
    print(x**2, end=' ')
for x in [1, 2, 3, 4]:
    print(x**3, end=' ')
for x in 'spam':
    print(x*3, end=' ')

open('script2.py').read()

# file iterator
f = open('script2.py')
f.readline()
f.readline()
f.readline()
f.readline()
f.readline()
f.readline()
f.readline()

# iteration protocol
f = open('script2.py')
f.__next__()
f.__next__()
f.__next__()
f.__next__()
f.__next__()

# this is the best way of reading file
for line in open('script2.py'):
    print(line.upper(), end=' ')

# reading file using readline method less efficiency
f = open('script2.py')
while True:
    line = f.readline()
    if not line:
        break
    print(line.upper(), end='')


L = [1, 2, 3]
# obtain an iterator
i = iter(L)
# call iterator next
i.__next__()
i.__next__()
i.__next__()
i.__next__()

# obtain an iterator is not require for file object
# file object is its own iterator
f = open('script2.py')
iter(f) is f
iter(f) is f.__iter__()
f.__next__()
f.__next__()
f.__next__()
f.__next__()
f.__next__()

# list does not have it's own iterator, so first get iterator
L = [1, 2, 3]
i = iter(L)
i.__next__()
i.__next__()
i.__next__()
i.__next__()
i = iter(L)
i.__next__()
next(i)
next(i)
next(i)

# Automatic iterator
L = [1, 2, 3]
for x in L:
    print(x)

# Munual iterator
i = iter(L)
while True:
    try:
        x = next(i)
    except StopIteration:
        break
    print(x ** 2, end=' ')

# Other Bulit-in Type iterables
D = {'a': 1, 'b': 2, 'c': 3}
for key in D.keys():
    print(key, D[key])

i = iter(D)
next(i)
next(i)
next(i)
next(i)

for key in D:
    print(key, D[key])


p = os.popen('dir')
p.__next__()
p.__next__()
p.__next__()
p.__next__()
next(p)


p = os.popen('dir')
i = iter(p)
next(i)
next(i)
next(i)
next(i)
next(i)
next(i)
next(i)
next(i)
next(i)

R = range(5)
i = iter(R)
i.__next__()
i.__next__()
i.__next__()
i.__next__()
i.__next__()
i.__next__()
i.__next__()

E = enumerate('spam')
i = iter(E)
next(i)
next(i)
next(i)
next(i)
next(i)
list(enumerate('spam'))

L = [1, 2, 3, 4, 5]
for i in range(len(L)):
    L[i] += 10

# List Comprehension Basics more efficiency
L = [x + 10 for x in L]

res = []
for x in L:
    res.append(x + 10)

# Using list comprehensions on Files
f = open('script2.py')
lines = f.readlines()
lines = [line.rstrip() for line in lines]

lines = [line.rstrip() for line in open('script2.py')]
[line.upper() for line in open('script2.py')]
[line.rstrip().upper() for line in open('script2.py')]
[line.replace(' ', '!') for line in open('script2.py')]
[('sys' in line, line[:5]) for line in open('script2.py')]

# Extended list comprehension syntax
# filter clauses: if
lines = [line.rstrip() for line in open('script2.py') if line[0] == 'p']

res = []
for line in open('script2.py'):
    if line[0] == 'p':
        res.append(line.rstrip())

# Nested loops: for
[x + y for x in 'abc' for y in 'lmn']

res = []
for x in 'abc':
    for y in 'lmn':
        res.append(x + y)

# other iteration contexts
map(str.upper, open('script2.py'))
list(map(str.upper, open('script2.py')))
sorted(open('script2.py'))
list(enumerate(open('script2.py')))
list(filter(bool, open('script2.py')))
functools.reduce(operator.add, open('script2.py'))

# Built-in function which accept iterables
list(open('script2.py'))
tuple(open('script2.py'))
'&&'.join(open('script2.py'))

# sequence assignment
a, b, c, d = open('script2.py')
a, *b = open('script2.py')

# membership test
'x = 2/n' in open('script2.py')
'x = 2\n' in open('script2.py')

# slice assignment
L = [11, 22, 33, 44]
L[1:3] = open('script2.py')
L

L = [11]
L.extend(open('script2.py'))
L
L = []
L + open('script2.py')
# append add iterable to a list without iterating
L = [11]
L.append(open('script2.py'))
L
# bulit-in functionn support the iteration
sum([3, 2, 4, 1, 5, 0])
any(['spam', '', 'ni'])
all(['spam', '', 'ni'])
max([3, 2, 5, 1, 4])
max(open('script2.py'))


def f(a, b, c, d):
    """Unpack collection of values into individual arguments."""
    print(a, b, c, d, sep='&')


f(1, 2, 3, 4)
# unpacks into arguments
f(*[1, 2, 3, 4])
f(*open('script2.py'))
# Iterates by lines too
f(*open('script2.py'))
# it's also possible to use the zip built-in to unzip zipped tupple
x = (1, 2)
y = (3, 4)
list(zip(x, y))
a, b = zip(*zip(x, y))
a, b = zip(x, y)
# the range iterable
R = range(10)
# range returns a iterable, not a list
i = iter(R)
next(i)
next(i)
next(i)
list(range(10))
# range support indexing and len function
len(R)
R[0]
R[5]
R[-1]
next(i)
i.__next__()
i.next()

# the map, zip, and filter iterables
M = map(abs, (-1, 0, 1))
next(M)
next(M)
next(M)
list(map(abs, (-1, 0, 1)))

# Multiple versus single pass iterators
R = range(4)
i1, i2 = iter(R), iter(R)
print(next(i1), next(i1), next(i1))
next(i2)

# zip, map and filter do not support multiple active iterators
z = zip((1, 2, 3), (10, 11, 12))
i1 = iter(z)
i2 = iter(z)
next(i1)
next(i1)
next(i2)

M = map(abs, (-1, 0, 1))
i1 = iter(M)
i2 = iter(M)
print(next(i1), next(i1), next(i1))
next(i2)


# generator functions and expressions behave like map and zip
# Dictioary view iterables
D = dict(a=1, b=2, c=3)
# dictionary keys, values and items methods return iterable view
# object that generate result lists all at once in memory
K = D.keys()
next(K)
i = iter(K)
next(i)
next(i)
next(i)
next(i)

for k in D.keys():
    print(k, end=' ')

V = D.values()
list(V)
V[0]
list(V)[0]
D.items()
list(D.items())
for (k, v) in D.items():
    print(k, v, end=' ')
