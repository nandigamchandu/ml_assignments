"""Chapter 28."""


class Person:
    """Class implement."""

    def __init__(self, name, job=None, pay=0):
        """Coding constructor."""
        self.name = name
        self.job = job
        self.pay = pay

# step2: Adding behavior method
    def lastName(self):
        """Behavior method1."""
        return self.name.split()[-1]

    def giveRaise(self, percent):
        """Behavior method2."""
        self.pay = int(self.pay * (1+percent))

# step3: operator overloading
    def __repr__(self):
        """Overloading operator."""
        return '[Person: %s, %s]' % (self.name, self.pay)


# step4 : Customizing Behavior by subclassing
class Manager(Person):
    """Subclass of Person class."""

# step5 : Customizing constructors
    def __init__(self, name, pay):
        """Mangers constructor."""
        Person.__init__(self, name, 'mgr', pay)

    def giveRaise(self, percent, bonus=.10):
        """Customize giveRaise method in subclass."""
        Person.giveRaise(self, percent+bonus)


class Department:
    """Implement department class."""

    def __init__(self, *args):
        """Department constructor."""
        self.members = list(args)

    def addMember(self, person):
        """Implement addmenber function."""
        self.members.append(person)

    def giveRises(self, percent):
        """Implement give rise method."""
        for person in self.members:
            person.giveRaise(percent)

    def showAll(self):
        """Implement Showall method."""
        for person in self.members:
            print(person)


# Test the class
if __name__ == '__main__':
    bob = Person('Bob Smith')
    sue = Person('Sue Jones', job='dev', pay=100000)
    tom = Manager('Tom jones', 50000)

    development = Department(bob, sue)
    development.addMember(tom)
    development.giveRises(0.10)
    development.showAll()
