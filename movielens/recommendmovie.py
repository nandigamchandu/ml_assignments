import numpy as np

# reading data from file 

def read_files(movieratingsfile):
    movieratings = np.loadtxt(movieratingsfile,
                              delimiter= ',',
                              skiprows = 1,)
    return movieratings

# normalise function

def normalise(arr, lowerlimit, upperlimit):
    return (arr - lowerlimit) / (upperlimit - lowerlimit)

# removing missing value function

def remove_missing(arr1, arr2):
    temp = ~np.any([arr1 < 0, arr2 < 0], axis = 0 )
    return arr1[temp], arr2[temp]

# normaliseratings function

def normalise_ratings(ratings):
    moviereviewers = np.array([('imdb', 0, 10), ('rt', 0, 10), ('rv', 0, 4), 
                  ('re', 0, 4), ('mc', 0, 100), ('tg', 0, 5), 
                  ('ac', 0, 5)])
    x = ratings[1:]
    y = moviereviewers[:, 1].astype('f2')
    z = moviereviewers[:, 2].astype('f2')
    return np.apply_along_axis(normalise, 0, x, y, z)

#  pearson correlation function

def pearson_correlation(ar1):
    def pearson_correlation1(ar2):
        arr1, arr2 = remove_missing(ar1, ar2)
        arr1_standard_units = (arr1 - np.mean(arr1)) / np.std(arr1)
        arr2_standard_units = (arr2 - np.mean(arr2)) / np.std(arr2)
        return np.sum(arr1_standard_units * arr2_standard_units) / np.size(arr1)
    return pearson_correlation1  

# function to get matrix of corrlation 

def closest_reviewer(x):
    ratings = read_files('movie_ratings.csv').T
    normaliseratings = normalise_ratings(ratings)
    correlation = np.apply_along_axis(pearson_correlation(normaliseratings[x - 1]), 1, normaliseratings)
    correlation[x - 1] = -1
    return (np.argmax(correlation) + 1, np.max(correlation))