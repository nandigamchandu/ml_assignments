import recommendmovietest as rm 
import numpy as np
import unittest as ut 

class TestNormalise(ut.TestCase):
    def test1(self):
        arr = np.array([1, 2, 6, 8, 7, 9])
        self.assertSequenceEqual(list(rm.normalise(arr, 0, 10)), [0.1, 0.2, 0.6, 0.8, 0.7, 0.9])

if __name__ == '__main__':
    ut.main()


# output

# test1 (__main__.TestNormalise) ... ok
#----------------------------------------------------------------------
# Ran 1 test in 0.000s
# OK