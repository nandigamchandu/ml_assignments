import recommendmovietest as rm 
import numpy as np
import unittest as ut 

class TestPearson(ut.TestCase):

    def test1(self):
        v1 = np.array([74, 66, 68, 69, 73, 70, 60, 63, 67, 70, 70, 70, 75, 62, 75])
        v2 = np.array([193, 133, 155, 147, 175, 128, 100, 128, 170, 182, 178, 118, 227, 115, 211])
        self.assertAlmostEqual(rm.pearson(v1,v2), 0.8321756)

# lengths of input arrays are not equal

    def test2(self):
        v1 = np.array([74, 66, 68, 69, 73, 70, 60, 63, 67, 70, 70, 70, 75, 62, 75])
        v2 = np.array([193, 133, 155, 147, 175, 128, 100, 128, 170, 182, 178, 118, 227, 115])
        with self.assertRaises(ValueError):
            rm.pearson(v1, v2)

if __name__ == '__main__':
    ut.main()

# output

# test1 (__main__.TestPearson) ... ok
# test2 (__main__.TestPearson) ... ok
#----------------------------------------------------------------------
#Ran 2 tests in 0.001s
#OK
