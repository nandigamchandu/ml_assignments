"""Cleaning drity data."""
# %%
import pandas as pd

# %%
# importing data
data = pd.read_csv('~/data/movie_metadata.csv')
data.head()

# %%
# looking for basic stats
data['imdb_score'].describe()
# select a column
data['movie_title']
# select multiple columns
data[['budget', 'gross']]
# select all movies over two hours long
data[data['duration'] > 120]

# %%
# check for nan values
data.isnull().sum()

# %%
# Deal with missing data
# Add default values
# nan is replace by empty string
data.country = data.country.fillna('')
data.country.isnull().any()
data.duration = data.duration.fillna(data.duration.mean())
data.duration.isnull().any()
# %%
# Remove incomplete rows
# dropping all rows with any NaN values is easy
# default axes = 0
data1 = data.dropna()
# drop rows that have all NaN value
drop_all_na = data.dropna(how='all')
# drop subset
drop_sub_na = data.dropna(subset=['title_year'])

# %%
# Deal with error-prone columns
drop_col_all_nan = data.dropna(axis=1, how='all')
drop_col_any_nan = data.dropna(axis=1, how='any')

# %%
# Normalize data types
# data['duration'] has nan we can't load data with
# data['duration'] as int datatype.
data = pd.read_csv('~/data/movie_metadata.csv', dtype={'title_year': str})
data['duration'].dtype
data['title_year'].dtype
# convert to uppercase
data['movie_title'] = data['movie_title'].str.upper()
# removing trailing whitespace
data['movie_title'] = data['movie_title'].str.strip()

# %%
# Rename columns
(data.rename(columns={'title_year': 'release_date',
                      'movie_facebook_likes': 'facebook_likes'}).head())

data = (data.rename(columns={'title_year': 'release_date',
                             'movie_facebook_likes': 'facebook_likes'}))
# Save results
data.to_csv('~/data/cleanfile.csv', encoding='utf-8')
