"""Data structure and algorithms."""
from collections import deque
import heapq
from collections import defaultdict
from collections import OrderedDict
from collections import Counter
from operator import itemgetter
from operator import attrgetter
from itertools import groupby
from collections import namedtuple
from collections import ChainMap

# 1.1 Unpacking a sequence into separate variable
a, b, c = [1, 2, 3]
a, b, *c = [1, 2, 'chandu', 45, 89]
data = ['ACME', 50, 90.1, (2018, 12, 21)]
name, shares, price, (year, month, day) = data
# unpacking string
s = 'Hello'
a, d, c, d, e = s

# 1.2 unpackig elements from iterables of arbitary length
record = ('chnad', 'chandu@technoidentity.com', 9912021371, 855178071)
name, email, *phone_no = record
phone_no
*trailing, current = [5, 6, 8, 9, 6, 8, 5]
avg = sum(trailing)/len(trailing)
avg, current

# example
records = [('foo', 1, 2),
           ('bar', 'hello'),
           ('foo', 3, 4)
           ]


def do_foo(x, y):
    """Foo function."""
    print('fooo', x, y)


def do_bar(s):
    """Bar function."""
    print('bar', s)


for tag, *args in records:
    if tag == 'foo':
        do_foo(*args)
    elif tag == 'bar':
        do_bar(*args)

line = 'nobody:*:-3:-3:Unprivileged user: /var/empty:/usr/bin/false'
uname, *fields, homedir, sh = line.split(':')
homedir
sh

name, *_, (*_, date) = data


def my_sum(lst):
    """Sum elements in list."""
    head, *tail = lst
    return head + my_sum(tail) if tail else head


print(my_sum([5, 6, 8, 9, 1]))


# 1.4 keeping the last N items
# double-ended queue deque
def search(lines, pattern, history=5):
    """Limited history."""
    pre_lines = deque(maxlen=history)
    for line in lines:
        if pattern in line:
            yield line, pre_lines
        pre_lines.append(line)


f = open('text.txt')
for line, prevlines in search(f, 'python', 5):
    for pline in prevlines:
        print(pline, end='')
    print('out print:', line, end='')
    print('-'*20)

# 1.5 finding the largest or smallest N items
# Heap queue algorithm
nums = [8, 9, 45, 6, 78, 19, 20, 23, 1, 2]
print(heapq.nlargest(3, nums))
print(heapq.nsmallest(3, nums))

portfolio = [{'name': 'IBM', 'shares': 100, 'price': 91.1},
             {'name': 'AAPL', 'shares': 50, 'price': 543.22},
             {'name': 'YHOO', 'shares': 45, 'price': 16.35},
             {'name': 'FB', 'shares': 200, 'price': 21.09},
             {'name': 'HPQ', 'shares': 35, 'price': 31.75},
             {'name': 'ACME', 'shares': 75, 'price': 115.65}]


cheap = heapq.nsmallest(3, portfolio, key=lambda s: s['price'])
expensive = heapq.nlargest(3, portfolio, key=lambda s: s['price'])

heap = nums
heapq.heapify(nums)

heap
heapq.heappop(heap)
# heap[0] is always smallest item.
# heappop pops off the first item and replaces it with next smallest item.


# 1.6 implementing a priority queue
class PriorityQueue:
    """Implement priority queue."""

    def __init__(self):
        """Contruct for priority queue."""
        self._queue = []
        self._index = 0

    def push(self, item, priority):
        """Push functionality."""
        heapq.heappush(self._queue, (-priority, self._index, item))
        self._index += 1

    def pop(self):
        """Pop functionality."""
        return heapq.heappop(self._queue)[-1]


class Item:
    """Item class implementation."""

    def __init__(self, name):
        """Contruct for item class."""
        self.name = name

    def __repr__(self):
        """Object."""
        return 'Item({!r})'.format(self.name)


q = PriorityQueue()
q.push(Item('foo'), 1)
q.push(Item('bar'), 2)
q.push(Item('spam'), 10)
q.push(Item('grok'), 2)
q.pop()

s = []
heapq.heappush(s, (-10, 5, 6))
heapq.heappush(s, (-20, 5, 2))
heapq.heappush(s, (1, 5, 6))
heapq.heappop(s)[0]

a = (1, 0, Item('foo'))
b = (1, 1, Item('bar'))
c = (2, 2, Item('spam'))

#  mapping keys to multiple values in dictionary
# you want to make a dictionary that maps keys to more than one value
# it is called as multidict

d = {"a": [1, 2, 3, 2],
     "b": [4, 5]}

d = {'a': {1, 2, 3},
     'b': {4, 5}}

d = defaultdict(list)
d['a'].append(1)
d['a'].append(2)
d['a'].append(3)
d['a'].append(2)
d['b'].append(4)
d['b'].append(5)

# obtain defaultdict operation using regular dictionary
d = {}
d.setdefault('a', []).append(1)
d.setdefault('a', []).append(2)
d.setdefault('a', []).append(3)
d.setdefault('a', []).append(1)
d.setdefault('b', []).append(4)
d.setdefault('b', []).append(5)
d

d = defaultdict(set)
d['a'].add(1)
d['a'].add(2)
d['a'].add(3)
d['a'].add(2)
d['b'].add(4)
d['b'].add(5)
d

pairs = [('a', 1), ('a', 2), ('a', 3), ('a', 1), ('b', 1), ('b', 5)]
d = {}
for key, value in pairs:
    if key not in d:
        d[key] = []
    d[key].append(value)

d1 = defaultdict(list)
for key, value in pairs:
    d1[key].append(value)
d1

# 1.7 keeping dictionaries in order
d = dict()
d['foo'] = 1
d['bar'] = 2
d['grok'] = 4
d['spam'] = 3
d['grok'] = 5
d
d = OrderedDict()
d['foo'] = 1
d['bar'] = 2
d['spam'] = 3
d['grok'] = 4
d['bar'] = 5
d

# 1.8 calculating with dictionries
prices = {'ACME': 45.23,
          'AAPL': 612.78,
          'IBM': 205.55,
          'HPQ': 37.20,
          'FB': 10.75}

# to perform useful calculation on the dictionary invert key and values
min_price = min(zip(prices.values(), prices.keys()))
max_price = max(zip(prices.values(), prices.keys()))
min_price, max_price

prices_sort = sorted(zip(prices.values(), prices.keys()))
prices_sort

# you can get key corresponding to min and max value
min(prices, key=lambda k: prices[k])
max(prices, key=lambda k: prices[k])

# 1.9 finding commonalities in two dictionaries
a = {'x': 1, 'y': 2, 'z': 3}
b = {'w': 10, 'x': 11, 'y': 2}

# find keys in common
a.keys() & b.keys()
# find key in a not in b
a.keys() - b.keys()
# find (key, value) pairs in common
a.items() & b.items()
c = {key: a[key] for key in a.keys() - {'z', 'w'}}
c
# keys(), items() method objects support set operations
# values() method oject doesn't support set operations


# 1.10- removing duplicates from a seq while maintaining order
def dedupe(items):
    """Remove duplicates in sequence."""
    seen = set()
    for item in items:
        if item not in seen:
            yield item
            seen.add(item)


a = [1, 5, 6, 8, 5, 6, 4, 1, 2, 3]
list(dedupe(a))
set(a)


def dedupe(items, key=None):
    """Remove duplicates in a sequence of unhashable type."""
    seen = set()
    for item in items:
        val = item if key is None else key(item)
        if val not in seen:
            yield item
            seen.add(val)


a = [{'x': 1, 'y': 2},
     {'x': 3, 'y': 5},
     {'x': 1, 'y': 3},
     {'x': 1, 'y': 2}]

list(dedupe(a, key=lambda b: (b['x'], b['y'])))
list(dedupe(a, key=lambda b: b['x']))

# 1.11 naming a slice
record = '446899989894848494949494948498949494977894168977946'
cost = record[10:15]

shares = slice(20, 32)
price = slice(40, 48)
cost = record[shares]
record[price]
items = [0, 1, 2, 3, 4, 5, 6, 7]
items[2: 4]
a = slice(2, 4)
del items[a]
items

a = slice(1, 50, 2)
a
a.start
a.step
a.stop

s = "Helloworld"
x = a.indices(len(s))
for i in range(*x):
    print(s[i])

# determining the most frequently occuring items in a sequence
words = ['look', 'into', 'my', 'eyes', 'look', 'into', 'my', 'eyes',
         'the', 'eyes', 'the', 'eyes', 'the', 'eyes', 'not', 'around', 'the',
         'eyes', "don't", 'look', 'around', 'the', 'eyes', 'look', 'into',
         'my', 'eyes', "you're", 'under'
         ]
word_count = Counter(words)
word_count
word_count.most_common(3)
morewords = ['why', 'are', 'you', 'not', 'looking', 'in', 'my', 'eyes']
for word in morewords:
    word_count[word] += 1

Counter(words)
Counter(morewords)
a = Counter(words)
b = Counter(morewords)
c = a + b
c

# 1.13 Sorting a List of Dictionaries by a Common Key
rows = [{'fname': 'Brian', 'lname': 'Jones', 'uid': 1003},
        {'fname': 'David', 'lname': 'Beazley', 'uid': 1002},
        {'fname': 'John', 'lname': 'Cleese', 'uid': 1001},
        {'fname': 'Big', 'lname': 'Jones', 'uid': 1004}
        ]

rows_by_frame = sorted(rows, key=itemgetter('fname'))
rows_by_frame
rows_by_uid = sorted(rows, key=itemgetter('uid'))
rows_by_uid

rows_by_fname = sorted(rows, key=lambda r: r['fname'])
rows_by_lname = sorted(rows, key=lambda r: (r['lname'], r['fname']))
rows_by_lname


# 1.14 sorting objects without native comparison support
class User:
    """Test class."""

    def __init__(self, user_id):
        """Contruct."""
        self.user_id = user_id

    def __repr__(self):
        """Object."""
        return 'User({})'.format(self.user_id)


users = [User(23), User(3), User(99)]
sorted(users, key=lambda u: u.user_id)

sorted(users, key=attrgetter('user_id'))
min(users, key=attrgetter('user_id'))
max(users, key=attrgetter('user_id'))

# 1.15 Grouping Records Together Based on a Field

rows = [
         {'address': '5412 N CLARK', 'date': '07/01/2012'},
         {'address': '5148 N CLARK', 'date': '07/04/2012'},
         {'address': '5800 E 58TH', 'date': '07/02/2012'},
         {'address': '2122 N CLARK', 'date': '07/03/2012'},
         {'address': '5645 N RAVENSWOOD', 'date': '07/02/2012'},
         {'address': '1060 W ADDISON', 'date': '07/02/2012'},
         {'address': '4801 N BROADWAY', 'date': '07/01/2012'},
         {'address': '1039 W GRANVILLE', 'date': '07/04/2012'},
         ]

# Sort by the desired field first
rows.sort(key=itemgetter('date'))
#  iterate in groups
for date, items in groupby(rows, key=itemgetter('date')):
    print(date)
    for i in items:
        print(' ', i)


rows_by_date = defaultdict(list)
for row in rows:
    rows_by_date[row['date']].append(row)
rows_by_date

# 1.16 filtering sequence elements

mylist = [1, 4, -5, 10, -7, 2, 3, -1]
[n for n in mylist if n > 0]
[n for n in mylist if n < 0]

pos = (n for n in mylist if n > 0)
pos
list(pos)

# suppose filter involve some exceptions
value = ['1', '2', '-3', '-', '4', 'N/A', '5']


def is_int(val):
    """Func to find int."""
    try:
        x = int(val)
        return True
    except ValueError:
        return False


list(filter(is_int, value))

# 1.17 extracting a subset of a dictionary
{key: value for key, value in prices.items() if value > 200}
tech_names = {'AAPL', 'IBM'}
{key: value for key, value in prices.items() if key in tech_names}

# 1.18 mapping names of a dictionary
Subscriber = namedtuple('Subscriber', ['addr', 'joined'])
sub = Subscriber('chandu@abc.com', '2018-06-19')
sub.addr
sub.joined
len(sub)
addr, joined = sub
addr
joined
# mapping names to sequence elements
# 1.19 transforming and reducing data at the same time
# reduce example
nums = [1, 2, 3, 4, 5]
s = sum(x * x for x in nums)
s

# transfor example tuple to csv
s = ('ACME', 50, 123.45)
','.join(str(x) for x in s)

# 1.20 combining multiple mappings into a single mapping
a = {'x': 1, 'z': 3}
b = {'y': 2, 'z': 4}
c = ChainMap(a, b)
c['x']
c['y']
c['z']
c.keys()
len(c)  # 3
list(c.keys())
list(c.values())
c['z'] = 10
del c['z']

values = ChainMap()
values['x'] = 1
# add a new mapping
values = values.new_child()
values['x'] = 2
# add a new mapping
values = values.new_child()
values['x'] = 3
values['x']
values.parents['x']
values.parents.parents['x']
values.parents.parents
a = {'x': 1, 'z': 3}
b = {'y': 2, 'z': 4}
merged = dict(b)
merged.update(a)
merged
a['x'] = 13
merged['x']

merged = ChainMap(a, b)
merged['x']
a['x'] = 42
merged['x']
