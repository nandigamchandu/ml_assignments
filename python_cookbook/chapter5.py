"""Files and i/o."""

import os
import io
import gzip
import time
import os.path
import glob
import sys

from functools import partial
# Reading and writing text data
with open('sampleread.txt', 'rt') as f:
    data = f.read()
with open('samplewrite.txt', 'wt') as f:
    f.write(data)

# Redirected print statement
with open('samplewrite.txt', 'wt') as f:
    print('Hello world', file=f)

with open('samplewrite.txt', 'rt') as f:
    print(f.read())

# to append to the end of an existing file, use open() with mode 'at'
with open('samplewrite.txt', 'at') as f:
    print('Hello world line 2', file=f)

with open('samplewrite.txt', 'rt') as f:
    print(f.read())

text = 'ā bē cē	dē 	ē 	ef 	gē 	hā 	ī 	kā 	el 	em 	en 	ō pē qūerestē'
text = text.replace('\t', ' ')

with open('samplewrite.txt', 'at') as f:
    print(text, file=f)

# replace bad char with '\ufffd'
with open('samplewrite.txt', 'rt', encoding='ascii', errors='replace') as f:
    print(f.read())
# ignore bad char
with open('samplewrite.txt', 'rt', encoding='ascii', errors='ignore') as f:
    print(f.read())

# Printing with different separator or line ending
print('ACNE', 45, 9.99)
print('ACNE', 45, 9.99, sep=',')
print('ACNE', 45, 9.99, sep=',', end='!!\n')

# Reading and writing binary data
with open('samplewt.bin', 'wb') as f:
    f.write(b'Hello World')

with open('samplewt.bin', 'rt') as f:
    data = f.read()

with open('samplewt.bin', 'rb') as f:
    data1 = f.read()

for i in data:
    print(i)
for i in data1:
    print(i)

with open('samplewt.bin', 'rb') as f:
    data = f.read()
    text = data.decode('utf-8')

data[0]
text[0]

# write to a file that deosn't already exit
with open('somefile1.txt', 'wt') as f:
    f.write('Hello\n')

# x-mode : below statement try to open file
# if file present it rise exception
os.remove('somefile3.txt')
with open('somefile3.txt', 'xt') as f:
    f.write('Hello\n')

# similar to above operation
if not os.path.exists('somefile3.txt'):
    with open('somefile3.txt', 'wt') as f:
        f.write("Hello\n")
else:
    print("File already exists")

# performing i/o operations on a string
# try to store text or binary to file like objects
s = io.StringIO()
s.write('Hello world \n')
print('add second line.', file=s)
s.getvalue()

s = io.StringIO('Hello world \n second line')
s.read()

# Reading and wirting Compressed Datafiles
# decompress
with gzip.open('gzip.txt.gz', 'rt') as f:
    text = f.read()
text
# compress
with gzip.open('somefile.gz', 'wt') as f:
    f.write("Hello \n world \n hgdsgfuyshuf")
# we can also set compress level using compresslevel parameter in open method

# Iterating over fixed sized records
with open('somefile3.txt', 'rb') as f:
    records = iter(partial(f.read, 70), b'')
    n = 1
    for i in records:
        print('record:', n)
        print(i)
        n += 1

# Reading binary data into mutable buffer
# without creating object read from file to buffer and
# write from buffer to file
# create buffer with size equal to file size
file_size = os.path.getsize('somefile3.txt')
buf = bytearray(file_size)
with open('somefile3.txt', 'rb') as f:
    f.readinto(buf)
buf[0:5]
# write from buffer to file
with open('newfile.txt', 'wb') as f:
    f.write(buf)

f = open('newfile.txt', 'rb')
data = f.read()
f.close()
data

# manipulating pathnames
path = '/home/nandigamchandu/data/babynames/yob1990.txt'
# get last component of the path
os.path.basename(path)
# get the directory name
os.path.dirname(path)
# join path components together
os.path.join(os.path.dirname(path), os.path.basename(path))
# expand home directory
path = '~/data/babynames/yob1990.txt'
os.path.expanduser(path)
# split the file extension
os.path.splitext(path)

# testing for the existence of a file
os.path.exists('somefile3.txt')
os.path.exists('somefile4.txt')
# is a regular file
os.path.isfile('/home/nandigamchandu/data/babynames/yob2004.txt')
# is a directory
os.path.isdir('/home/nandigamchandu/data/babynames/yob2004.txt')
os.path.isdir('/home/nandigamchandu/data/babynames')
# is a symbolic link
os.path.islink('/home/nandigamchandu/data/mybaby')
os.path.islink('/home/nandigamchandu/data/babynames')
# to get meta data
os.path.getsize(path)
x = os.path.getmtime(path)
time.ctime(x)

# getting a directory listing
os.listdir('.')

pyfiles = [name for name in os.listdir('.')
           if name.endswith('.py')]
pyfiles
pyfile = glob.glob('*.py')

filemetadata = [(name, os.stat(name)) for name in pyfile]
for name, meta in filemetadata:
    print(name, time.ctime(meta.st_mtime))

# Bypassing filename encoding
sys.getfilesystemencoding()

# file using unicode in filename
with open('jalape\xf1o.txt', 'wt') as f:
    f.write('Spicy!')

os.listdir('.')

# openning file with raw file name
with open(b'jalape\xc3\xb1o.txt') as f:
    print(f.read())

with open('jalapeño.txt') as f:
    print(f.read())
file = os.listdir('.')
for name in file:
    print(name)
