"""Functions."""
import re
from functools import partial


# writing functions that only accept keyword arguments
def recv(a, *, b):
    """Accept keyword arguments."""
    print(a, b, sep=', ')


recv(100, b=5)
# Note: enforce greater code clarity when
#       specifying optional function arguments.


# Attaching informational metadata to function arguments
def add(x: int, y: int) -> int:
    """Add x, y."""
    return x + y


help(add)
add.__annotations__


# Returning multiple values from a function
def myfun():
    """Test finc."""
    return 1, 2, 3


a, b, c = myfun()
a, b, c


# Defining functions with default arguments
def spam(a, b=42):
    """Test func."""
    return a + b


spam(2)

# defing inline functions
add = (lambda x, y: x+y)
add(1, 2)

name = ['asd6', 'asd1', 'rewf0', 'sdf8', 'fsdf10']
sorted(name, key=lambda name: int(re.findall(r'\d+', name)[0]))

# Capturing variables in anonymous functions
# if you want an anonymous function to
# capture a value at the point of definition

x = 10
a = (lambda y, x=x: y + x)
x = 20
b = (lambda y, x=x: y + x)
a(10)
b(20)

# example
func = [lambda x: x+n for n in range(5)]
for i in func:
    print(i(0))

func = [lambda x, n=n: x+n for n in range(5)]
for i in func:
    print(i(0))


# making an n-argument callabble work as a callable with fewer arguments
# functools.partial()
def spam(a, b, c, d):
    """Test func."""
    print(a, b, c, d)


s1 = partial(spam, 1)
s1(2, 3, 4)
s1(4, 5, 6)
s2 = partial(spam, d=42)
s2(1, 2, 3)


# Replacing single method classes with functions
class abc:
    """Test class."""

    def __init__(self, a):
        """Construct."""
        self.a = a

    def add(self, b):
        """Add func."""
        return self.a + b


x = abc(5)
x.add(3)
x.add(9)


# above class is replace by closure functions
def abc(x):
    """Test func."""
    def add(y):
        """Add func."""
        return x + y
    return add


x = abc(5)
x(3)
x(9)


# Accessing variables define inside a closure
def sample():
    """Test func."""
    n = 0

    def func(value):
        """Func1."""
        nonlocal n
        n = value
        return n
    return func


x = sample()
x(10)


# extend a clsure with functions that allow the inner variables to be
# accessed and modified
def sample():
    """Test func."""
    n = 0

    def func():
        """Func1."""
        print('n=', n)

    def get_n():
        """Get n."""
        return n

    def set_n(value):
        """Set n."""
        nonlocal n
        n = value

    # Attach  as function attributes."""
    func.get_n = get_n
    func.set_n = set_n
    return func


f = sample()
f()
f.get_n()
f.set_n(10)
f.get_n()

n = 0


def abc():
    """Test."""
    print(n)


def abc1(value):
    """Test."""
    global n
    n = value


a = abc
abc.abc1 = abc1
a.abc1(10)
