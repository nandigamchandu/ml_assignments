"""numbers, dates and times."""
from decimal import Decimal
from decimal import localcontext
import cmath
import numpy as np
import math
from fractions import Fraction
import random
from datetime import timedelta
from datetime import datetime
from dateutil.relativedelta import relativedelta
from dateutil.rrule import *
from pytz import timezone

# 3.1 rounding numerical values
round(2.389878, 1)
round(2.389878, 3)
round(2.33, 1)
round(-2.38, 1)

a = 1627731
round(a, -1)
round(a, -2)

x = 1.23456
format(x, '0.2f')
format(x, '0.3f')

'value is {:0.3f}'.format(1.23456)

# 3.2. performing accurate decimal calculations
a = 2.1
b = 4.2
c = a + b
round(c, 2)

b = Decimal('2.1')
a = Decimal('4.2')
c = b + a
c
c == Decimal('6.3')

a = Decimal('1.3')
b = Decimal('1.7')
a / b
# to control no of digits and rounding of decimal by creating local context
with localcontext() as ctx:
    ctx.prec = 3
    print(a / b)

with localcontext() as ctx:
    ctx.prec = 20
    print(a / b)

# 3.3 Formatting numbers of output

x = 1.23256
format(x, '0.2f')
format(x, '>10.1f')
format(x, '<10.1f')
format(x, '^10.1f')

x = 15646479.254
format(x, ',')
format(x, '0,.1f')
format(x, 'e')
format(x, '0.2e')
format(x, '0.2E')
'the value is {:0,.2f}'.format(x)

x1 = format(x, '0,.2f')
swap_separators = {ord('.'): ',', ord(','): '.'}
x1.translate(swap_separators)

# 3.4 working with binary, octal and hexadecimal
x = 1234
bin(x)
oct(x)
hex(x)

format(x, 'b')
format(x, 'o')
format(x, 'x')

x = -1234
format(x ** 32 + x, 'b')
format(x ** 32 + x, 'x')

x = 1234
int(hex(x), 16)
int(oct(x), 8)
int(bin(x), 2)

# 3.5 packing and unpacking large integers from bytes
# 3.6 performing complex valued math
a = complex(2, 4)
b = 1 + 2j
c = a + b
a.real
a.imag
a.conjugate()
a / b
abs(a)
# to perform complex value functions such as sines, cosines or square root
# import cmath
cmath.sin(a)
x = np.array([a, b, c])
np.sin(x)
np.sqrt(x)

cmath.sqrt(-1)

# 3.7 working with infinity and nans
a = float('inf')
b = float('-inf')
c = float('nan')

# to test presence of this no use math.isinf() and math.isnan()
math.isinf(a)
math.isnan(c)

# infinite propagate
a + 23
a * 23

10 / a
# nan output
a + b
a / a
c + 23
c * 2

# nan values is that they never compare as equal
d = float('nan')
c == d
c is d

# 3.8 calculating with Fractions

a = Fraction(4, 5)
b = Fraction(1, 3)

# operations on fractions
a + b
a - b
a / b
a * b

# converting into float
c = a + b
float(c)

# 3.9 calculating with large numerical array
a = [2, 3, 5, 6]
a * 2
b = [5, 6, 9, 10]
a + b

ax = np.array(a)
bx = np.array(b)
ax * 2
ax + bx
np.sqrt(ax)

grid = np.zeros(shape=(10, 10), dtype=float)
grid
grid += 10
grid
np.sin(grid)

# working on 2-d array
a = np.array([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]])
a[1]
a[:, 1]
a + [100, 101, 102, 103]
np.where(a < 10, a, 10)

# 3.10 performing matrix and linear algebra calculations
m = np.matrix([[1, -2, 3], [0, 4, 5], [7, 8, -9]])
m.T
m.I
v = np.matrix([[2], [3], [4]])
m * v
# mx = v
x = np.linalg.solve(m, v)
m * x

# 3.11 picking things at random

values = [1, 2, 3, 4, 5, 6]
random.choice(values)
random.sample(values, 3)
random.shuffle(values)
values
dt1 = timedelta(seconds=3600)
dt2 = timedelta(days=1, hours=1)
x = dt1 + dt2
x.seconds
x.days
x.total_seconds() / 3600

# creating data instance
a = datetime.today()
a + dt1
b = datetime(2018, 6, 22)
b + dt2
(a - b).days

a = datetime(2018, 1, 14)
b = datetime(2018, 5, 1)
c = relativedelta(b, a)
c.days
c.months
datetime.today().weekday()

weekdays = ['mon', 'tue', 'wed', 'thu', 'fir', 'sat', 'sun']

d = datetime.now()
d + relativedelta(weekday=FR(-1))

# 3.15 converting string into datetime
text = '2018-19-09'
# to define input date
a = datetime.strptime(text, '%Y-%d-%m')
b = datetime.now()
a-b
# to define output date
datetime.strftime(b, '%m %d %y')

# 3.16 manipulating dates invovle time zones
d = datetime.now()
india = timezone('Asia/Kolkata')
loc_d = india.localize(d)

# time convert to US/central
loc_d.astimezone(timezone('US/Central'))
