"""Strings and Text."""
import re
import os
from urllib.request import urlopen
from fnmatch import fnmatch, fnmatchcase
from calendar import month_abbr
import unicodedata
import string
import textwrap
import html

# 2.1 splitting strings on any of multiple delimiters
line = 'chhdsh: jkkwe jifj,ejijj,     eiuwew'
re.split(r'[\s:,]\s*', line)

fields = re.split(r'(:|\s|,)\s*', line)
values = fields[::2]
delimiters = fields[1::2] + [' ']
delimiters

' '.join(v+d for v, d in zip(values, delimiters))

# noncapture group""
re.split(r'(?::|\s|,)\s*', line)

# 2.2 matching text at start or end of a string
name = 'hdfsfhdhhh'
name.endswith('hhh')
name.startswith('hdfc')
filenames = os.listdir()
[name for name in filenames if name.endswith('.py')]
any(name.endswith('.py') for name in filenames)


def read_data(name):
    """Read data from web page."""
    if name.startswith(('http:', 'https:', 'ftp:')):
        return urlopen(name).read()
    else:
        with open(name) as f:
            return f.read()


choice = ['https:', 'ftp']
url = 'https://www.kdnuggets.com/2018/index.html'
url.startswith(tuple(choice))

# 2.3 matching string using shell wildcard patterns

fnmatch('gsh.txt', '*.txt')
fnmatch('gsh.txt', '?sh.txt')
names = ['dat1.csv', 'dat2.csv', 'dat45.csv', 'efsd.py', 'fsdf.txt']
[name for name in names if fnmatch(name, 'dat*.csv')]

addresses = ['fsdfsdf ST', 'sdfdsfds st', 'dsfsdf ST', 'sfsdt AVE', 'sdfste']
[addr for addr in addresses if fnmatchcase(addr, '*ST')]
[addr for addr in addresses if fnmatch(addr, '*ST')]

# 2.4 matching and searching for text pattern
text = 'yeah, but no, but yeah, but no, but yeah'

# Exact match
text == 'yeah'
text1 = 'yeah'
text1 == 'yeah'

# match at start and end
text.startswith('yeah')
text.endswith('yeah')

# the location of first occurrance
text.find('yeah')

text1 = '11/06/2018'


def date_match(text1):
    """Find data pattern."""
    if re.match('\d+/\d+/\d+', text1):
        print('yes')
    else:
        print('no')


date_match(text1)
text2 = 'Nov 27, 2012'
date_match(text2)


def date_match(pattern, text1):
    """Find data pattern."""
    if pattern.match(text1):
        print('yes')
    else:
        print('no')


datepat = re.compile(r'\d+/\d+/\d+')
date_match(datepat, text1)

text = 'Today is 11/27/2012. pycon starts 3/13/2013.'

# search text for all occurance of a pattern
datepat.findall(text)

# match() always tries to find the match at the start of the a string
datepat.match(text)

datepat = re.compile('(\d+)/(\d+)/(\d+)')
m = datepat.match('20/06/2018')
m.group(0)
m.group(1)
m.groups()

datepat.findall(text)

# find match iteratively use finditer() method

x = datepat.finditer(text)
for m in x:
    print(m.groups())

# 2.5 searching and replacing text

text = 'yeah, but no, but yeah, but no, but yeah'
text.replace('yeah', 'yep')

text = 'Today is 11/27/2012. pycon starts 3/13/2013.'
re.sub(r'(\d+)/(\d+)/(\d+)', r'\3-\1-\2', text)

datepat = re.compile(r'(\d+)/(\d+)/(\d+)')
datepat.sub(r'\3-\1-\2', text)


def date_change(m):
    """Func to change date format."""
    mon_name = month_abbr[int(m.group(1))]
    return f'{m.group(2)}-{mon_name}-{m.group(3)}'


datepat.sub(date_change, text)

# to get no of replacements in the text use nsub()
newtext, n = datepat.subn(date_change, text)
newtext
n

# 2.6 Searching and replacing case insensitive text
# for case in-sensitive we have to use re.IGNORCASE flag
text = 'PYTHONE, lower python, mixed Python'
re.findall('python', text, flags=re.IGNORECASE)


def matchcase(word):
    """Func to replace."""
    def replace(m):
        text = m.group()
        if text.isupper():
            return word.upper()
        elif text.islower():
            return word.lower()
        elif text[0].isupper():
            return word.capitalize()
    return replace


re.sub('python', matchcase('snake'), text, flags=re.IGNORECASE)

# 2.7. Specifying a regular expression for the shortest match
text = 'computer says "no"'
str_pat = re.compile(r'\"(.*)\"')
str_pat.findall(text)
text = 'hdsuihdh"no" bhsd"gfdshg"'
str_pat.findall(text)
str_pat = re.compile(r'\"(.*?)\"')
# This makes the matching nongreedy, and produces the shortest match instead.
str_pat.findall(text)
# Note:
# Adding the ? right after operators such
# as * or + forces the matching algorithm to look for the shortest
# possible match instead

# 2.8 Writing a regular expression for multiline patterns
# dot(.) to match any character except new line
comment = re.compile(r'/\*(.*?)\*/')
text = '/* this a comment */'
comment.findall(text)
text = '''/* this a comment
        this is comment */'''
comment.findall(text)
comment = re.compile(r'/\*((?:.|\n)*?)\*/')
comment.findall(text)

# re.DOTALL it makes the . in a regular expression
# match all characters, including newline
comment = re.compile(r'/\*(.*?)\*/', re.DOTALL)
comment.findall(text)

# 2.9. Normalizing unicode text to a standard
s1 = 'Spicy jalape\u00f1o'
s2 = 'Spicy jalapen\u0303o'
s1 == s2
len(s1)
len(s2)
t1 = unicodedata.normalize('NFC', s1)
t2 = unicodedata.normalize('NFC', s2)
t1 == t2
print(ascii(t1))
print(ascii(t2))
t3 = unicodedata.normalize('NFD', s1)
t4 = unicodedata.normalize('NFD', s2)
t3 == t4
print(ascii(t3))
print(ascii(t4))

t1 = unicodedata.normalize('NFD', s1)
''.join(c for c in t1 if not unicodedata.combining(c))

# 2.10 working with unicode characters in regular expressions
num = re.compile('\d+')
num.match('123')

num.match('\u0661\u0662\u0663').group(0)
pat = re.compile('stra\u00dfe', re.IGNORECASE)
s = 'straße'
pat.match(s).group(0)
pat.match(s.upper())

# 2.11. Stripping unwanted characters from strings
# strip() can strip charcters from begin and end of string
# lstrip() rstrip()

# Whitespace stripping
s = '      hello world \n'
s.strip()
s.lstrip()
s.rstrip()

# characterstripping
t = '------------ghjfdhghg+++===='
t.strip('-+=')
t.lstrip('-')
t.rstrip('=+')

# to handdle inner space
t = 'dgrf           rgerger'
t.replace(' ', '')
re.sub('\s+', ' ', t)

# 2.12. sanitizing and cleaning up text

s = 'pýtĥöñ\fis\tawesome\r\n'

remap = {ord('\t'): ' ',
         ord('\f'): ' ',
         ord('\r'): None}

a = s.translate(remap)
a
s = s.replace('\f', ' ')
s = s.replace('\t', ' ')
s = s.replace('\r', '')
a == s

# 2.13 alignning text strings
# for basic alignment of strings, the ljust(), rjust() and center()
text = 'Hello World'
text.ljust(20)
text.rjust(20)
text.center(20)
text.ljust(20, '=')
text.rjust(20, '=')
text.center(20, '=')

format(text, '>20')
format(text, '<20')
format(text, '^20')

format(text, '+>20')
format(text, '+<20')
format(text, '+^20')

'{:>10s} {:>10s}'.format('Hello', 'World')
x = 1566.2566
format(x, '>10')
format(x, '^10.2f')

# 2.14 combining and concatenating strings
parts = ['isdf', 'fsd', 'sfesd']
' '.join(parts)
','.join(parts)
''.join(parts)

a = 'Is Chicago'
b = 'Not Chicago'
a + ' ' + b

"{} {}".format(a, b)

a = "hello" "world"

data = ['ACME', 50, 91.1]
','.join(str(d) for d in data)

c = 'dsdffdf'
print(a, b, c, sep=':')

# 2.15 interpolating variables in strings
# format(), combination of format_map() and vars()

name = 'chandu'
value = 0
s = '{name} value is {value}'
s.format(name=name, value=value)

f'{name} value is {value}'

# using format_map() and vars()
s.format_map(vars())


class Info:
    """Test class."""

    def __init__(self, name, value):
        """Contruct."""
        self.name = name
        self.value = value


a = Info('chandu', 0)
s.format_map(vars(a))


class safesub(dict):
    """To handle missing."""

    def __missing__(self, key):
        """Abc."""
        return '{' + key + '}'


del value
s.format_map(safesub(vars()))

s = string.Template('$name value is $value')
s.substitute(vars())

# 2.16 reformatting text to a fixed number of columns
s = "Look into my eyes, look into my eyes, the eyes, the eyes, \
the eyes, not around the eyes, don't look around the eyes, \
look into my eyes, you're under."
len(s)
print(s)

s1 = textwrap.fill(s, 70)
print(s1)
s1 = textwrap.fill(s, 40)
print(s1)

s1 = textwrap.fill(s, 40, initial_indent='       ')
print(s1)
s1 = textwrap.fill(s, 40, subsequent_indent='       ')
print(s1)
