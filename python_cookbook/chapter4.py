"""Iterators and Generators."""
import itertools
from itertools import dropwhile
from itertools import permutations
from itertools import combinations
from itertools import combinations_with_replacement
from itertools import zip_longest
from itertools import chain
import fnmatch
import os
import re
from collections import Iterable
import heapq
# Delegating iteration


class Node:
    """Delegate iteration."""

    def __init__(self, value):
        """Contruct."""
        self._value = value
        self._childern = []

    def addchild(self, node):
        """Add child node."""
        self._childern.append(node)

    def __repr__(self):
        """Return object."""
        return "Node{}".format(self._value)

    def __iter__(self):
        """Implement iterator."""
        return iter(self._childern)

    def depth_first(self):
        """Test."""
        yield self
        for c in self:
            yield from c.depth_first()


root = Node(0)
child1 = Node(1)
child2 = Node(2)
root.addchild(child1)
root.addchild(child2)
root.addchild(Node(3))
root.addchild(Node(4))
for ch in root:
    print(ch)

root = Node(0)
child1 = Node(1)
child2 = Node(2)
root.addchild(child1)
root.addchild(child2)
root.addchild(Node(3))
root.addchild(Node(4))
for ch in root.depth_first():
    print(ch)

# iterating in reverse
a = [1, 2, 3, 4]
for x in reversed(a):
    print(x)

# reversed iteration can be customized on user define classes
# if they implement the __reversed__() method


class Countdown:
    """Count."""

    def __init__(self, start):
        """Contructor."""
        self.start = start

    def __iter__(self):
        """Forword."""
        n = self.start
        while n > 0:
            yield n
            n -= 1

    def __reversed__(self):
        """Reverse."""
        n = 1
        while n <= self.start:
            yield n
            n += 1


x = Countdown(5)
list(x.__reversed__())
x = Countdown(5)
list(x.__iter__())


# taking a slice of an iterator
def count(n):
    """Take a slice."""
    while True:
        yield n
        n += 1


c = count(5)
for i in itertools.islice(c, 10, 20):
    print(i)

# skipping th first part of an iterable
# method 1
with open('python.txt') as f:
    for i in itertools.islice(f, 3, None):
        print(i)

# method 2
with open('python.txt') as f:
    for i in dropwhile(lambda line: line.startswith('#'), f):
        print(i)

# Iterating over all possible combinations or permutations
# permutations
item = ['a', 'b', 'c']
for i in permutations(item):
    print(i)

for i in combinations(item, 3):
    print(i)

for i in permutations(item, 2):
    print(i)

for i in combinations(item, 2):
    print(i)

for i in permutations(item, 1):
    print(i)

for i in combinations(item, 1):
    print(i)


for i in combinations_with_replacement(item, 3):
    print(i)

for i in combinations_with_replacement(item, 2):
    print(i)

for i in combinations_with_replacement(item, 1):
    print(i)

# iterating over the index-value pairs of a sequence

my_list = ['a', 'b', 'c']
for idx, value in enumerate(my_list):
    print((idx, value))

for idx, value in enumerate(my_list, 1):
    print((idx, value))

# iterating over multiple sequences simultaneously
a = ['z', 'y', 'x', 'w']
b = [26, 25, 24, 23, 22, 21, 20]

for i in zip(a, b):
    print(i)

for i in zip_longest(a, b):
    print(i)

for i in zip_longest(a, b, fillvalue='c'):
    print(i)

# iterating on items in seperate containers

for i in chain(a, b):
    print(i)


def gen_find(filepat, top):
    """Find file paths."""
    for path, dir, file in os.walk(top):
        for name in fnmatch.filter(file, filepat):
            yield os.path.join(path, name)


def open_file(files):
    """Open files."""
    for file in files:
        f = open(file)
        yield f


def gen_concatenate(iterators):
    """Con file content."""
    for it in iterators:
        yield from it


def gen_grep(lines):
    """Capture required line."""
    pat = re.compile(r'Zyt.*s')
    for line in lines:
        if pat.search(line):
            yield line


top = "/home/nandigamchandu/data/"
filepat = 'yob2*.txt'
filenames = gen_find(filepat, top)
files = open_file(filenames)
# lines = gen_concatenate(files)
lines = chain(*files)
pylines = gen_grep(lines)
for i in pylines:
    print(i)


# Flattening a nested sequence
def flatten(items):
    """Flattering."""
    for i in items:
        if isinstance(i, Iterable):
            yield from flatten(i)
        else:
            yield i


def flatten1(items):
    """Flattering."""
    for x in items:
        if isinstance(x, Iterable):
            for i in flatten(x):
                yield i
        else:
            yield x


x = [1, 2, [3, 4, [5, 6], 7], 8]
x1 = flatten(x)
for i in x1:
    print(i)
x1 = flatten1(x)
for i in x1:
    print(i)

# iterating in sorted order over merged sorted iterables

a = [1, 6, 12, 52, 78, 90]
b = [0, 33, 45, 60, 100, 103]
c = heapq.merge(a, b)
list(c)
