"""Classes and objects."""

from datetime import date
from abc import ABCMeta, abstractmethod
import collections
from decimal import Decimal
import numbers
from operator import methodcaller
from functools import total_ordering

# problem:
# change the output produce by printing and viewing of instance


class Pair:
    """Test class."""

    def __init__(self, value):
        """Construct."""
        self.value = value

    def __repr__(self):
        """Instance."""
        return "Pair({0.value})".format(self)

    def __str__(self):
        """Instance."""
        return "({})".format(self.value)


Pair(5)
print(Pair(5))
str(Pair(6))
repr(Pair(8))

p = Pair(9)
print('p is {!r}'.format(p))
print(' p is {}'.format(p))

# problem:
# you want an object to support customized formatting through the format()
# function and string method

_formats = {'for': "{s.first}->{s.second}->{s.third}",
            'rev': "{s.third}->{s.second}->{s.first}"}


class MyFormat:
    """Test class."""

    def __init__(self, first, second, third):
        """Construct."""
        self.first = first
        self.second = second
        self.third = third

    def __format__(self, code):
        """Format."""
        if code == '':
            code = 'for'
        fmt = _formats[code]
        return fmt.format(s=self)


a = MyFormat("first", "second", "third")
format(a)
format(a, 'rev')
"print in reverse order---> {:rev}".format(a)

d = date(2018, 6, 23)
format(d, '%d %b %Y')
"Today date is = {:%A, %B %d, %y}".format(d)


# problem
# calling a method on a parent class
class A:
    """Parent."""

    def spam(self):
        """Test."""
        print('A.spam')


class B(A):
    """Child."""

    def spam(self):
        """Test."""
        print('B.sapam')
        super().spam()


A().spam()
B().spam()


class Base:
    """Parent."""

    def __init__(self):
        """Construct."""
        print('Base.__init__')


class A(Base):
    """Child."""

    def __init__(self):
        """Construct."""
        Base.__init__(self)
        print('A.__init__')


class B(Base):
    """Child."""

    def __init__(self):
        """Contruct."""
        Base.__init__(self)
        print('B.__init__')


class C(A, B):
    """Contrct."""

    def __init__(self):
        """Construct."""
        A.__init__(self)
        B.__init__(self)
        print('C.__init__')


c = C()


# Defining an interface or abstract base class
class Abc(metaclass=ABCMeta):
    """Test class."""

    @abstractmethod
    def read(self):
        """Test."""
        pass

    @abstractmethod
    def write(self):
        """Test."""
        pass


Abc()
# Note: abstract class cannot instantiated directly. Here we get Type error:
# stating can't instantiate abstract class


class Abc1(Abc):
    """Implement abstract methods."""

    def read(self):
        """Test."""
        print("abstract method read implement")

    def write(self):
        """Test."""
        print("abstract methon write implement")


Abc1().read()
Abc1().write()

collections.Sequence()
collections.Iterable()
collections.Mapping()

d = Decimal('7.5')
isinstance(d, numbers.Real)


# Delegating Attribute Access alternative to inheritance
class A:
    """Test class."""

    def spam(self):
        """Spam."""
        print('class A Spam')

    def foo(self):
        """Foo."""
        print('class A foo')


class B:
    """Test class."""

    def __init__(self):
        """Contruct."""
        self.a = A()

    def spam(self):
        """Spam."""
        return self.a.spam()

    def foo(self):
        """Foo."""
        return self.a.foo()


B().spam()
B().foo()


# method2:
class B:
    """Test class."""

    def __init__(self):
        """Contruct."""
        self.a = A()

    def __getattr__(self, name):
        """Get attr."""
        return getattr(self.a, name)


c = B()
c.spam()
c.foo()


# Defininng more than one constructor in a class
class MySum:
    """Test class."""

    def __init__(self, value):
        """Construct."""
        self.a = value

    @classmethod
    def geta(cls, a):
        """Alternate constructor."""
        return cls(a)

    def mysum(self, b):
        """Sum."""
        return self.a + b


a = MySum(5)
a.mysum(3)

c = MySum.geta(6)
c.mysum(4)


class MySum1(MySum):
    """Test."""

    pass


MySum1.geta(5).mysum(5)


# Creating an instance with invoking init
a = MySum.__new__(MySum)
a.a  # It rise error
c = a.geta(6)  # alternative contructor
c.mysum(7)


# Calling a method on an object given the name as string
a = MySum(6)
getattr(a, 'mysum')(3)
methodcaller('mysum', 4)(a)


# Making classes Support Comparison operations
class Room:
    """Room class."""

    def __init__(self, name, length, width):
        """Constructor."""
        self.name = name
        self.length = length
        self.width = width
        self.square_feet = self.length * self.width


@total_ordering
class House:
    """House class."""

    def __init__(self, name, style):
        """Contructor."""
        self.name = name
        self.style = style
        self.room = list()

    @property
    def living_space_footage(self):
        """Measure area."""
        return sum(r.square_feet for r in self.room)

    def add_room(self, room):
        """Add room."""
        self.room.append(room)

    def __str__(self):
        """Instance in string."""
        return '{}: {} sqft {}'.format(self.name,
                                       self.living_space_footage,
                                       self.style)

    def __eq__(self, other):
        """Equal."""
        return self.living_space_footage == other.living_space_footage

    def __lt__(self, other):
        """Lesser than."""
        return self.living_space_footage < other.living_space_footage

    def __gt__(self, other):
        """Greater than."""
        return self.living_space_footage > other.living_space_footage


h1 = House('h1', 'Cape')
h1.add_room(Room('living room', 14, 12))
h1.add_room(Room('Master Bedroom', 18, 20))
h1.add_room(Room('office', 12, 12))
h1.add_room(Room('Kitchen', 12, 16))
h1.living_space_footage
print(h1)

h2 = House('h2', 'Ranch')
h2.add_room(Room('living room', 15, 20))
h2.add_room(Room('Kitchen', 10, 10))
h2.add_room(Room('Master', 12, 13))
print(h2)

h3 = House('h3', 'Split')
h3.add_room(Room('Master Bedroom', 14, 21))
h3.add_room(Room('living room', 18, 20))
h3.add_room(Room('office', 12, 16))
h3.add_room(Room('kitchen', 15, 17))
print(h3)

h1 > h2
h1 < h3
h3 == h2
h2 >= h3


# Encapsulating name in a class
class A:
    """Test."""

    def __init__(self):
        """Contructor."""
        self.___a = "class A variable"

    def __spam(self):
        """Test."""
        print('_A__spam method=========')

    def myfun(self):
        """Test."""
        print('class A myfun')


class B(A):
    """Test."""

    def __init__(self):
        """Constructor."""
        super().__init__()
        self.___a = "class B variable"

    def __spam(self):
        """Test."""
        print('_B__spam method++++++++')

    def myfun(self):
        """Test."""
        print('class B myfun')


c = B()
c.myfun()
c._B__spam()
a = A()
a._A__spam()
# note: Here, the private name __a and __spam renames to _B__a, _B__spam,
# which are different than the mangled names in the base class b


# creating managed attributes
class Person:
    """Person class."""

    def __init__(self):
        """Constructor."""
        self.first_name = ''

    @property
    def name(self):
        """Return first name."""
        return self.first_name

    @name.setter
    def name(self, value):
        """Set first name."""
        self.first_name = value

    @name.deleter
    def name(self):
        """Delete first name."""
        raise AttributeError("Con't delete attribute")


a = Person()
a.first_name
a.name = 'chandu'
a.name
del a.name
a.name


class Square:
    """Square."""

    def __init__(self, side):
        """Constructor."""
        self.side = side

    @property
    def area(self):
        """Area of square."""
        return self.side * self.side

    @property
    def perimeter(self):
        """Perimeter of square."""
        return self.side * 4


s = Square(5)
s.area
s.perimeter
