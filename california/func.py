import pandas as pd
import numpy as np

def categorise_income(df):
    income_categories = df['median_income'] // 1.5
    income_categories[income_categories >= 5] = 5
    return income_categories

from sklearn.model_selection import StratifiedShuffleSplit
def stratified_train_test(features, label):
    split = StratifiedShuffleSplit(n_splits=1, test_size=0.2, random_state=42)
    for training_indices, test_indices in split.split(features, label):
        train_set = features.loc[training_indices]
        test_set = features.loc[test_indices]
    return train_set, test_set

def split_features_labels(df):
    features = df.drop('median_house_value', axis=1)
    labels = df['median_house_value']
    return features, labels

def split_num_cat_features(df):
    numeric = list(df.select_dtypes( np.number))
    categoric = list(df.select_dtypes( exclude=np.number))
    return numeric, categoric

# Create a class to select numerical or categorical columns 
# since Scikit-Learn doesn't handle DataFrames yet

from sklearn.base import BaseEstimator, TransformerMixin

class ColumnSelector(BaseEstimator, TransformerMixin):
    def __init__(self, column_names):
        self.column_names = column_names
    def fit(self, X, y=None):
        return self
    def transform(self, X):
        return X[self.column_names].values

class AttributeAdder(BaseEstimator, TransformerMixin):
    def fit(self, X, y=None):
        return self
    def transform(self, X):
        rph = X[:, 3] / X[:, 6]
        bph = X[:, 4] / X[:, 6]
        bpr = X[:, 4] / X[:, 3]
        pph = X[:, 5] / X[:, 6]
        return np.c_[X, rph, bph, bpr, pph]

from sklearn.metrics import mean_squared_error
def rms_error(expected, predicted):
    mse = mean_squared_error(expected, predicted)
    return np.sqrt(mse)