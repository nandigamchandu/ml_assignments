# In[]:
from sklearn.datasets import fetch_mldata
import numpy as np

from sklearn import model_selection as ms
from dask_searchcv import GridSearchCV, RandomizedSearchCV
import matplotlib.pyplot as pyplot
import seaborn as sns
from sklearn.linear_model import LogisticRegression

# In[]:
mnist = fetch_mldata('MNIST original')
X, y = mnist.data, mnist.target

# In[]:
param_grid = {
    "C": [1e-5, 1e-3, 1e-1, 1],
    "fit_intercept": [True, False],
    "penalty": ["l1", "l2"]
}

clf = LogisticRegression()
# In[]:

dk_grid_search = GridSearchCV(clf, param_grid=param_grid, n_jobs=-1)
sk_grid_search = ms.GridSearchCV(clf, param_grid=param_grid, n_jobs=-1)

# In[]:
%%time
dk_grid_search.fit(X,y)
