"""Improving efficency."""

# %%
import pandas as pd
import re


# %%
def split_space_seperate(x):
    """Split space seperated values."""
    x = x[0].str.split(' ', expand=True)
    return x


def tweet_message(x):
    """Split tweet message."""
    x = x[0].str.split(' ', 1, expand=True)
    return x


def tweet_type(x):
    """Find tweet type."""
    x1 = re.findall('(RT|DM)', x)
    if x1:
        return x1[0]
    else:
        return 'REG'


follow = (pd.read_table('~/data/twitter/follows.txt', header=None)
            .pipe(split_space_seperate)
            .rename(columns={0: 'user'}))

stream = (pd.read_table('~/data/twitter/stream.txt', header=None)
            .pipe(tweet_message)
            .rename(columns={0: 'author', 1: 'tweet'}))


followers = (follow.set_index('user')
                   .stack()
                   .reset_index()
                   .groupby(0)['user']
                   .unique()
                   .rename_axis('user', axis=0)
                   .rename('followers')
                   .reset_index()
                   .assign(foll_cnt=lambda x: x['followers'].transform(len)))

stream = (stream.assign(mention=lambda x: x['tweet']
                        .str.findall('@([A-Za-z\d]+)'))
                .assign(nmention=lambda x: x['mention'].transform(len))
                .assign(original_aut=lambda x: x['tweet']
                        .str.extract('RT @([A-Za-z\d]+)'))
                .assign(tweet_typ=lambda x: x['tweet'].transform(tweet_type))
                .assign(tweet_msg=lambda x: x['tweet']
                        .str.extract('RT @[A-Za-z\d]+ (.+)'))
                .assign(original_aut=lambda x: x['original_aut']
                        .where(x['original_aut'].notnull(), x['author']))
                .assign(tweet_msg=lambda x: x['tweet_msg']
                        .where(x['tweet_msg'].notnull(), x['tweet'])))

col = ['author', 'tweet_msg', 'tweet_typ', 'original_aut',
       'followers', 'foll_cnt', 'mention', 'nmention']
stream = stream.merge(followers, left_on='author', right_on='user')[col]
