"""method chaining."""

import numpy as np
import pandas as pd
import seaborn as sns


# %%
# method chaining
def extract_city_name(df):
    """Extract city names from origin_city_name and dest_city_name."""
    cols = ['origin_city_name', 'dest_city_name']
    city = df[cols].apply(lambda x: x.str.extract("(.*), \w{2}", expand=False))
    df = df.copy()
    df[['origin_city_name', 'dest_city_name']] = city
    return df


# %%
df = (pd.read_csv('~/data/one_time.csv')
        .rename(columns=str.lower)
        .drop(['year', 'quarter', 'month', 'unnamed: 37'], axis=1)
        .pipe(extract_city_name)
        .assign(fl_date=lambda x: pd.to_datetime(x['fl_date']),
                unique_carrier=lambda x: pd.Categorical(x['unique_carrier']),
                airline_id=lambda x: pd.Categorical(x['airline_id']),
                carrier=lambda x: pd.Categorical(x['carrier']),
                cancelled=lambda x: pd.Categorical(x['cancelled'])))
df.info()

# %%
# what does the daily flight pattern look like?
daily_flight = (df.loc[df['unique_carrier']
                       .isin(df['unique_carrier'].value_counts().index[:5])]
                  .groupby(['unique_carrier', 'fl_date'])['fl_num'].count()
                  .unstack(0)
                  .fillna(0)
                  .rename_axis('Fights per day', axis=1))
daily_flight.plot()
sns.despine()


# %%
# between which cities more no of flights are present
(df.groupby(['origin_city_name'])['dest_city_name']
   .apply(lambda x: x.value_counts().head(1))
   .rename('number of flights')
   .sort_values(ascending=False)
   .reset_index()
   .rename(columns={'level_1': 'dest_city_name'})
   .head(10))


# %%
# compare dep_delay flight
def delay_count(x):
    """Count delay."""
    x1 = np.where(x['dep_delay'] >= 0, 'ontime', 'delay')
    return pd.Series(x1)


(df.pipe(delay_count)
   .pipe(sns.countplot))
