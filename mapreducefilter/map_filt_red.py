"""Using map function in python."""
from functools import reduce


# Implementing my map function
def my_map(fun, lst):
    """Implement map func."""
    return [fun(i) for i in lst]


# example1:
# increment each element by 5
def inc_by_5(i):
    """Increment by five."""
    return i + 5


my_map(inc_by_5, [1, 2, 3, 4, 5, 6])
list(map(inc_by_5, [1, 2, 3, 4, 5, 6]))


# example2:
# find length of each string
lst = ['fsdfs', 'srferg', 'sdfe', 'drtr', 'frdgggh']
my_map(len, lst)
list(map(len, lst))


# example3:
def sum_ele_lst(lst):
    """Sum all elements in list."""
    return sum(lst)


my_map(sum_ele_lst, [[5, 6, 9], [4, 5, 3], [9, 8, 5, 7]])
list(map(sum_ele_lst, [[5, 6, 9], [4, 5, 3], [9, 8, 5, 7]]))


# Implementing filter function
def my_filter(pred, lst):
    """Implement filter function."""
    return [i for i in lst if pred(i)]


# Example1:
sports = ['Archery', 'Cricket', 'Curling', 'Athletics',
          'Badminton', 'Ice Hockey', 'Taekwondo', 'Athletics', 'Badminton',
          'Boxing', 'Diving', 'Hockey', 'Rowing', 'Squash', 'Tennis']
# return sorts name having 7 characters
my_filter(lambda x: len(x) == 7, sports)
list(filter(lambda x: len(x) == 7, sports))
my_filter(lambda x: len(x) < 7, sports)
list(filter(lambda x: len(x) < 7, sports))

# return sorts name if it as y in their spell
my_filter(lambda x: 'y' in x, sports)
list(filter(lambda x: 'y' in x, sports))


# implement reduce functions
def my_reduce(fun, init, lst):
    """Reduce implement."""
    x = init
    for i in lst:
        x = fun(x, i)
    return x


# Example1:
my_reduce(lambda x, y: x+y, 0, [5, 6, 8, 9])
reduce(lambda x, y: x+y, [5, 6, 8, 9])

# Example2:
my_reduce(lambda x, y: x*y, 1, [5, 6, 8, 9])
reduce(lambda x, y: x*y, [5, 6, 8, 9])

# implement map fuction using reduce function

# Example1:
# square each element in the list
my_map(lambda x: x**2, [1, 2, 3, 4, 5])
my_reduce(lambda x, y: x + [y**2], [], [1, 2, 3, 4, 5])

# implement filter function using reduce function

# Example 1:
# return list of  elements whose values is greater than the 3
my_reduce(lambda x, y: x + [y] if y > 3 else [], [], [1, 2, 3, 4, 5])
my_filter(lambda x: x > 3, [1, 2, 3, 4, 5])
