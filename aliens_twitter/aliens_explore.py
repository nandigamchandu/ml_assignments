"""Explore aliens twitter dataset."""
# %%
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import re

# %%
# space seperated values, no of columns are less
#  than no of columns in remainind observation
follow = pd.read_table('~/data/twitter/large/follows.txt', header=None)
follow = follow[0].str.split(' ', expand=True)

# %%
# find each user followers
user = follow[0].rename('user')


def find_follower(x):
    """Find follower."""
    followers = follow.loc[np.any(follow.iloc[:, 1:] == x, axis=1), 0]
    return followers.values


followers = user.map(find_follower).rename('followers')
user_follow = pd.concat([user, followers], axis=1)

# %%
# who is most popular user
user_follow['foll_count'] = user_follow['followers'].apply(lambda x: len(x))
max_follower = np.max(user_follow['foll_count'])
user_follow.loc[user_follow['foll_count'] == max_follower, 'user']

# %%
# Who are the top parrots?
stream = pd.read_table('~/data/twitter/large/stream.txt', header=None)
stream = stream[0].str.split(' ', 1, expand=True)
stream = stream.rename(columns={0: 'auther', 1: 'tweet'})


def tweet_type(x):
    """Find tweet type."""
    x1 = re.findall('(RT|DM)', x)
    if x1:
        return x1[0]
    else:
        return 'REG'


stream['tweet_typ'] = stream['tweet'].apply(tweet_type)
parrot = stream.groupby(('tweet_typ', 'auther')).size().loc['RT']
x = parrot.nlargest(10)
plt.xticks(rotation=90)
sns.barplot(x.index, x.values)
plt.ylabel('no of retweet done by each user')
plt.title('Top parrot users')


# %%
# Who are the Most worst trolls?
def mention_user(x):
    """Find menstion user names."""
    x1 = re.findall('@([A-Za-z\d]+)', x)
    return x1


stream['mention_user'] = stream['tweet'].apply(mention_user)
stream['nmention'] = (stream['tweet'].apply(
                                lambda x: len(re.findall('@[A-Za-z\d]+', x))))

stream = (stream.merge(user_follow, left_on='auther', right_on='user')
                .drop(columns='user'))
troll = stream.groupby('auther')['nmention'].sum()
x = troll.nlargest(10)
plt.xticks(rotation=90)
sns.barplot(x.index, x.values)
plt.ylabel('no of time author mention other')
plt.title("worst trolls")


# %%
# Who are the biggest influencers?
def retweet_mes(x):
    """Extract retweet message."""
    x1 = re.findall('RT @[A-Za-z\d]+ (.+)', x)
    if x1:
        return x1[0]


def retweet_aut(x):
    """Extract retweet message original auther."""
    x2 = re.findall('RT @([A-Za-z\d]+)', x)
    if x2:
        return x2[0]


stream['original_aut'] = stream['tweet'].apply(retweet_aut)
stream['tweet_mes'] = stream['tweet'].apply(retweet_mes)
stream['original_aut'] = (stream['original_aut'].where(
                        stream['original_aut'].notnull(), stream['auther']))
stream['tweet_mes'] = stream['tweet_mes'].where(
                             stream['tweet_mes'].notnull(), stream['tweet'])


def users_seen(x):
    """Tweet seen by."""
    x1 = re.findall('^(DM)', x['tweet_mes'])
    if x1:
        return x['mention_user']
    return list(set(x['followers']).union(set(x['mention_user'])))


stream['seen_by'] = stream.apply(users_seen, axis=1)
stream['nseen_by'] = stream['seen_by'].apply(lambda x: len(x))


def original_tweet_seen(x):
    """Original message seen dy."""
    x1 = len(set(x['seen_by']).intersection(set([x['original_aut']])))
    return x['nseen_by'] - x1


stream['noriginal_seen'] = stream.apply(original_tweet_seen, axis=1)

x = (stream.groupby(['original_aut', 'tweet_mes'])['noriginal_seen']
           .sum().reset_index())
y = (stream[stream['tweet_typ'] == 'RT']
     .groupby(['auther', 'tweet_mes'])['nseen_by'].sum().reset_index())
x = x.rename(columns={'original_aut': 'auther', 'noriginal_seen': 'seen'})
y = y.rename(columns={'nseen_by': 'seen'})
tweet_count = pd.concat([x, y], ignore_index=True)
tweet_count = tweet_count.groupby('auther')['seen'].mean()
ii = tweet_count.nlargest(10)
# Bar plot for Top influencers
plt.xticks(rotation=90)
sns.barplot(ii.index, ii.values)
plt.ylabel('average number of times each tweet was seen')
plt.title('Top influencers')
