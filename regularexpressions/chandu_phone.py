import re

# In[1]:
# function to check  mobile number
def is_mobile_no(x):
    if re.search(r'( |^)(\(?(\d )?)(\d{3})[-\) ]?(\d{3}-? ?)\d{4}', x):
        return True
    return False

# In[2]:
# function to check gmail
def is_email(x):
    if re.search(r'( |^)([\w\d\.]+)\+?([\w\d\.]+)\@\w+(\.com)', x):
        return True
    return False

# In[3]:
# function to find image file
def is_image(x):
    if re.search(r'^([\w\d\_]+)\.(jpg|png|gif)$',x):
        return True
    return False

# In[4]:
# function to Trim whitespace from start and end of line using.
def my_trim(x):
    return re.sub(r'\s*$',r'', re.sub(r'^\s*',r'',x))

# In[5]:
# function to extract filename, method name and line number.
def stack_trace(x):
    y = re.search(r'(\w*)\((\w*\.\w*):(\d+)\)?', x)
    if y:
        return y.group(1), y.group(2), y.group(3)
    return False

# In[6]:
# function to extract the protocol, host and port.
def protocol_host_port(x):
    y = re.search(r'(\w*)://([\w\-\_]*(.com)?):?(\d*)',x)
    if y:
        return y.group(1), y.group(2), y.group(4)
    return False
