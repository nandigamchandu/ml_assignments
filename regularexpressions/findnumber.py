import re

def is_number(x):
    if re.search(r'^(\+|-)?(\d+,?)+(\.)?\d*(e\+?|e-?)?\d+$',x):
        return True
    else:
        return False