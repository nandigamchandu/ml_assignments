import chandu_phone as ch
import unittest as ut

class TestIsNumber(ut.TestCase):
    # test function is_mobile_no
    def test1(self):
        self.assertEqual(ch.is_mobile_no('9912021371'), True)

    def test2(self):
        self.assertEqual(ch.is_mobile_no('415-555-1234'), True)

    def test3(self):
        self.assertEqual(ch.is_mobile_no('(416)555-3456'), True)

    def test4(self):
        self.assertEqual(ch.is_mobile_no('202 555 4567'), True)

    def test5(self):
        self.assertEqual(ch.is_mobile_no('1 416 555 9292'), True)

    def test6(self):
        self.assertEqual(ch.is_mobile_no('my mobile no is 9912021371'), True)

    def test7(self):
        self.assertEqual(ch.is_mobile_no('9912021371 is my mobile no'), True)

    def test8(self):
        self.assertEqual(ch.is_mobile_no('my no9912021371'), False)

    def test9(self):
        self.assertEqual(ch.is_mobile_no('202 55 4567'), False)

    def test10(self):
        self.assertEqual(ch.is_mobile_no('(4165)55-3456'), False)

 # test is_email function
    def test11(self):
        self.assertEqual(ch.is_email('chandu@gmail.com'), True)

    def test12(self):
        self.assertEqual(ch.is_email('nandigamchandu@technoidentity.com'), True)

    def test13(self):
        self.assertEqual(ch.is_email('nandigam+chandu@gmail.com'), True)

    def test14(self):
        self.assertEqual(ch.is_email('nandigam613@outlook.com'), True)

    def test15(self):
        self.assertEqual(ch.is_email('nandi....A@hotmail.com'), True)

    def test16(self):
        self.assertEqual(ch.is_email('my email is nandigam.chandu611+technoidentity@gmail.com'), True)

    def test17(self):
        self.assertEqual(ch.is_email('nandiga+mchan+du@gmail.com'), False)

    def test18(self):
        self.assertEqual(ch.is_email('my email is nandigam.chandu+611+technoidentity@gmail.com'), False)

# test is_image function
    def test19(self):
        self.assertEqual(ch.is_image('workspace.jpg'), True)

    def test20(self):
        self.assertEqual(ch.is_image('favicon.gif'), True)

    def test21(self):
        self.assertEqual(ch.is_image('worksp234.jpg'), True)

    def test22(self):
        self.assertEqual(ch.is_image('favicon.html'), False)

    def test23(self):
        self.assertEqual(ch.is_image('favicon.gif.temp'), False)

# test my_trim

    def test24(self):
        self.assertEqual(ch.my_trim('             chandu'), 'chandu')

    def test25(self):
        self.assertEqual(ch.my_trim('chandu       '), 'chandu')

    def test26(self):
        self.assertEqual(ch.my_trim('   chandu prasad     nandigam   '), 'chandu prasad     nandigam')

# test stack_trace function

    def test27(self):
        self.assertEqual(ch.stack_trace('E/( 1553): at widget.List.makeView(ListView.java:1727)'),
                                        ('makeView', 'ListView.java','1727'))

    def test28(self):
        self.assertEqual(ch.stack_trace('E/( 1553): at widget.List.fillDown(ListView.java:652)'),
                                        ('fillDown', 'ListView.java','652'))

    def test29(self):
        self.assertEqual(ch.stack_trace('E/( 1553): at widget.List.fillFrom(ListView.java:709)'),
                                        ('fillFrom', 'ListView.java', '709'))

    def test30(self):
        self.assertEqual(ch.stack_trace('W/dalvikvm( 1553): threadid=1: uncaught exception'),
                                        False)

    def test31(self):
        self.assertEqual(ch.stack_trace('E/( 1553): FATAL EXCEPTION: main'), False )

    def test32(self):
        self.assertEqual(ch.stack_trace('E/( 1553): java.lang.StringIndexOutOfBoundsException'), False)

# test function protocol_host_port

    def test33(self):
        self.assertEqual(ch.protocol_host_port('https://s3cur3-server.com:9999/'),
                                                ('https', 's3cur3-server.com','9999'))

    def test34(self):
        self.assertEqual(ch.protocol_host_port('file://localhost:4040/zip_file'),
                                                ('file', 'localhost', '4040'))

   def test35(self):
        self.assertEqual(ch.protocol_host_port('ftp://file_server.com:21/top_secret/life_changing_plans.pdf'),
                                                ('ftp', 'file_server.com', '21'))


if __name__ == '__main__':
    ut.main()
#---------------------------------------------------------------------
#Ran 26 tests in 0.002s

#OK
