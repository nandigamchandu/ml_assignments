TED Main Dataset

name:               The official name of the TED Talk. Includes the title and the speaker.
title:              The title of the talk
description:        A blurb of what the talk is about.
main_speaker:       The first named speaker of the talk.
speaker_occupation: The occupation of the main speaker.
num_speaker:        The number of speakers in the talk.
duration:           The duration of the talk in seconds.
event:              The TED/TEDx event where the talk took place.
film_date:          The Unix timestamp of the filming.
published_date:     The Unix timestamp for the publication of the talk on TED.com
comments:           The number of first level comments made on the talk.
tags:               The themes associated with the talk.
languages:          The number of languages in which the talk is available.
ratings:            A stringified dictionary of the various ratings given to the talk (inspiring, fascinating, jaw dropping, etc.)
related_talks:      A list of dictionaries of recommended talks to watch next.
url:                The URL of the talk.
views:              The number of views on the talk.
TED Transcripts Dataset

url: The URL of the talk
transcript: The official English transcript of the talk.
Acknowledgements

The data has been scraped from the official TED Website and is available under the Creative Commons License.

Inspiration

I've always been fascinated by TED Talks and the immense diversity of content that it provides for free. I was also thoroughly inspired by a TED Talk that visually explored TED Talks stats and I was motivated to do the same thing, albeit on a much less grander scale.

Some of the questions that can be answered with this dataset: 1. How is each TED Talk related to every other TED Talk? 2. Which are the most viewed and most favorited Talks of all time? Are they mostly the same? What does this tell us? 3. What kind of topics attract the maximum discussion and debate (in the form of comments)? 4. Which months are most popular among TED and TEDx chapters? 5. Which themes are most popular amongst TEDsters?
