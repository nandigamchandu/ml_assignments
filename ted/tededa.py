"""EDA ted dataset."""
# %%
import pandas as pd
import matplotlib.pyplot as plt
from biokit.viz import corrplot
import seaborn as sns
import ast
import re

# %%
# import ted dataset
ted_main = pd.read_csv('~/data/ted/ted_main.csv')

# check size of dataset
ted_main.shape  # (2550, 17)

# %%
# is duplicates are accepted or not
# NO
# checking duplicates of observations
ted_main.duplicated().sum()  # no duplicate observations
# checking duplicates of columns
ted_main.T.duplicated().sum()

# %%
# checking missing values
ted_main.isnull().sum()  # speaker occupation had 6 miss value
# why this missing values are present
miss = ted_main.loc[ted_main['speaker_occupation'].isnull()]
# i don't understand why it missing occupation field

# %%
# extracting ratings column into seperate dataframe
rating = ted_main['ratings'].apply(lambda x: ast.literal_eval(x))
rating = (rating.apply(lambda x: pd.DataFrame(x).set_index('name')).apply(
                                                         lambda x: x['count']))
max_rating = pd.DataFrame()
max_rating['max_rate_name'] = rating.T.idxmax()
max_rating['max_rate_value'] = rating.T.max()
max_rating['rate_sum'] = rating.sum(axis=1)
max_rating

# %%
# finding max correlation of rating with other rating
rating_correlation_matrix = abs(rating.corr())
len1 = len(rating_correlation_matrix['Beautiful'])
rating_correlation_matrix.iloc[range(len1), range(len1)] = 0
max_corr = pd.DataFrame()
max_corr['corr_with'] = rating_correlation_matrix.apply(lambda x: x.idxmax())
max_corr['value'] = rating_correlation_matrix.apply(lambda x: x.max())
max_corr

# %%
# ploting correlating matrix of rating
c = corrplot.Corrplot(rating_correlation_matrix)
_, ax = plt.subplots(figsize=(10, 8))
c.plot(method='circle', cmap=('white', 'blue'), ax=ax, fontsize=10)
plt.show()
# seaborn headmap
sns.heatmap(rating_correlation_matrix, cmap='Blues')

# %%
# get top talk main speaker of corresponding rating
ted_main = ted_main.merge(max_rating, left_index=True, right_index=True)
top_rate_cors = ted_main.groupby('max_rate_name')['max_rate_value'].idxmax()
x = ted_main.loc[top_rate_cors.values]
x[['main_speaker', 'max_rate_name', 'max_rate_value']]

# %%
# check correlation between views, comments, rating_sum and duration
vcd = ted_main[['views', 'duration', 'comments', 'rate_sum']].corr()
sns.heatmap(vcd, annot=True, cmap='Blues')
# comments and views are mediumly correlated
# rate_sum with views and rate_sum with comments correlated

# %%
# distribution plots of views, comments, rate_sum and duration
fig, ax = plt.subplots(4, 1, figsize=(6, 15))
sns.distplot(ted_main['views'], ax=ax[0])
sns.distplot(ted_main['comments'], ax=ax[1])
sns.distplot(ted_main['rate_sum'], ax=ax[2])
sns.distplot(ted_main['duration'], ax=ax[3])

# %%
# check for outliers for views columns
plt.figure(figsize=(5, 10))
sns.boxplot(data=ted_main['views'], orient='v')

# %%
# check for outliers for comments and duration columns
plt.figure(figsize=(10, 10))
sns.boxplot(data=ted_main[['comments', 'duration', 'rate_sum']], orient='v')

# %%
# check views colums outlier observations
views_outliers = ted_main['views'].nlargest(10)
ted_main.loc[views_outliers.index]

# %%
# extracting all tags
tag = ted_main['tags'].apply(lambda x: ast.literal_eval(x))
tag = tag.apply(lambda x: pd.Series(x))
tag.isnull().sum()
# ted talks having only one tag
tag[2].isnull().sum()
ted_main.loc[tag[tag[1].isnull()].index,
             ['title', 'main_speaker', 'speaker_occupation', 'tags', 'views']]

# %%
# top 10 mostly used tags
top_10_tags = tag.stack().value_counts()[:10]
plt.xticks(rotation=90)
sns.barplot(top_10_tags.index, top_10_tags.values)

# %%
# what type of talks mostly published by the ted
ted_main.groupby('max_rate_name').size().sort_values(ascending=False)

# %%
# publisher vs most talk type
x = ted_main['event'].apply(lambda x: re.findall('[A-Za-z]+', x)[0])
x[x.str.contains('TEDx')] = 'TEDx'
ted_main['publisher'] = x
x = ted_main.groupby(['publisher', 'max_rate_name']).size().rename('count')
x1 = x.reset_index().groupby(['publisher'])['count'].agg(lambda x: x.idxmax())
x.reset_index().loc[x1.values].sort_values('count', ascending=False).head(10)

# %%
# no of publication vs publisher
pub_ntalk = ted_main['publisher'].value_counts().head(10)
plt.xticks(rotation=90)
sns.barplot(pub_ntalk.index, pub_ntalk.values)

# %%
# no of publication vs publisher according to type of talk
x = ted_main.groupby(['publisher', 'max_rate_name']).size().rename('count')
x = x.unstack().loc[pub_ntalk.index]
x.plot.bar(stacked=True, figsize=(10, 8))

# %%
# no of views vs publisher according to type of talk
x = (ted_main.groupby(
             ['publisher', 'max_rate_name'])['views'].sum().rename('count'))
x = x.unstack().loc[pub_ntalk.index]
x.plot.bar(stacked=True, figsize=(10, 8))

# %%
# top 10 no of talk vs occupaton
top_talk_occ = ted_main.groupby('speaker_occupation').size().nlargest(10)

# %%
# no of talks vs occupation in each publisher
x = (ted_main.groupby(
     ['publisher', 'speaker_occupation']).size().rename('count'))
x = x.unstack().loc[pub_ntalk.index, top_talk_occ.index]
x.plot.bar(stacked=True, figsize=(10, 8))

# %%
# no of talks from top 10 publisher vs year
ted_main['date'] = pd.to_datetime(ted_main['published_date'], unit='s')
year = ted_main['date'].dt.year.rename('year')
x = ted_main.groupby([year, ted_main['publisher']]).size().rename('count')
x = x.unstack()
x = x[pub_ntalk.index]
x.plot.bar(stacked=True, figsize=(10, 6))

# %%
# no of talks from top 10 occupation vs year
x = (ted_main.groupby(
            [year, ted_main['speaker_occupation']]).size().rename('count'))
x = x.unstack()
x = x[top_talk_occ.index]
x.plot.bar(stacked=True, figsize=(10, 6))
