"""Matoltlib tutorial."""
# %%
import numpy as np
import matplotlib.pyplot as plt

# %%
# simple plot
X = np.linspace(-np.pi, np.pi, 256, endpoint=True)
C, S = np.cos(X), np.sin(X)
plt.plot(X, C)
plt.plot(X, S)
plt.show()

# %%
# instantiating defalts
# create a new figure of size 8*6 points, using 100 dots per inch
plt.figure(figsize=(8, 6), dpi=80)

# create a new subplot from a grid of 1*1
plt.subplot(111)

# plot cosine using blue color with continuous line of width 1 pixels
plt.plot(X, C, color='blue', linewidth=1.0, linestyle="-")

# plot sine using green color with a continuous line of width 1 pixels
plt.plot(X, S, color='green', linewidth=1.0, linestyle="-")

# Set x ticks
plt.xticks(np.linspace(4, 4, 9, endpoint=True))

# set y xticks
plt.ylim(-1, 1)

# set y xticks
plt.yticks(np.linspace(-1, 1, 5, endpoint=True))

# show result on screen
plt.show()

# %%
# changing color and line widths
plt.figure(figsize=(10, 6), dpi=80)
plt.plot(X, C, color='blue', linewidth=2.5, linestyle='-')
plt.plot(X, S, color="red", linewidth=2.5, linestyle='-')

# setting limits
plt.xlim(X.min()*1.1, X.max()*1.1)
plt.ylim(C.min()*1.1, C.max()*1.1)

# setting ticks
plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi])
plt.yticks([-1, 0, 1])

# setting tick labels
(plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi],
            [r'$-\pi$', r'$-\pi/2$', r'$0$', r'$+\pi/2$', r'$+\pi$']))
(plt.yticks([-1, 0, 1],
            [r'$-1$', r'$0$', r'$-1$']))

# Moving spines
ax = plt.gca()
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.spines['bottom'].set_position(('data', 0))
ax.yaxis.set_ticks_position('left')
ax.spines['left'].set_position(('data', 0))

# Adding legend
plt.plot(X, C, color="blue", linewidth=2.5, linestyle="-", label="cosine")
plt.plot(X, S, color="red",  linewidth=2.5, linestyle="-", label="sine")
plt.legend(loc='upper left', frameon=False)

# Annotate some point
t = 2*np.pi/3
plt.plot([t, t], [0, np.cos(t)], color='blue', linewidth=1.5, linestyle="--")
plt.scatter([t, ], [np.cos(t), ], 50, color='blue')

(plt.annotate(r'$\sin(\frac{2\pi}{3})=\frac{\sqrt{3}}{2}$',
              xy=(t, np.sin(t)), xycoords='data',
              xytext=(+10, +30), textcoords='offset points', fontsize=16,
              arrowprops=dict(arrowstyle="->", connectionstyle="arc3,rad=.2")))

plt.plot([t, t], [0, np.sin(t)], color='red', linewidth=1.5, linestyle="--")
plt.scatter([t, ], [np.sin(t), ], 50, color='red')

(plt.annotate(r'$\cos(\frac{2\pi}{3})=-\frac{1}{2}$',
              xy=(t, np.cos(t)), xycoords='data',
              xytext=(-90, -50), textcoords='offset points', fontsize=16,
              arrowprops=dict(arrowstyle="->", connectionstyle="arc3,rad=.2")))

# Devil is in the datails
for label in ax.get_xticklabels() + ax.get_yticklabels():
    label.set_fontsize(16)
    label.set_bbox(dict(facecolor='white', edgecolor='None', alpha=0.65))
plt.show()
