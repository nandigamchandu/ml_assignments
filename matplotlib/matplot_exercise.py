"""Regular plot."""
# %%
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.ticker import MultipleLocator

# %%
# Regular plot
n = 256
X = np.linspace(-np.pi, np.pi, n, endpoint=True)
Y = np.sin(2*X)

plt.plot(X, Y+1, color='blue', alpha=1.00)
plt.fill_between(X, 1, Y+1, alpha=0.25)
plt.plot(X, Y-1, color='blue', alpha=1.00)
plt.fill_between(X, -1, Y-1, (Y-1) > -1, color='blue', alpha=0.25)
plt.fill_between(X, -1, Y-1, (Y-1) < -1, color='red', alpha=0.25)
plt.show()

# %%
# Scatter plot
n = 1024
X = np.random.normal(0, 1, n)
Y = np.random.normal(0, 1, n)
Z = np.arctan2(Y, X)
plt.axes([0.025, 0.025, 0.95, 0.95])
plt.scatter(X, Y, c=Z, alpha=0.5, s=50, cmap='hsv')
plt.xticks([]), plt.yticks([])
plt.xlim(-2, 2), plt.ylim(-2, 2)
plt.show()

# %%
# Barplot
n = 12
X = np.arange(n)
Y1 = (1-X/float(n)) * np.random.uniform(0.5, 1.0, n)
Y2 = (1-X/float(n)) * np.random.uniform(0.5, 1.0, n)
plt.axes([0.025, 0.025, 0.95, 0.95])
plt.bar(X, +Y1, facecolor='#9999ff', edgecolor='white')
plt.bar(X, -Y2, facecolor='#ff9999', edgecolor='white')

for x, y in zip(X, Y1):
    plt.text(x, y+0.05, '%.2f' % y, ha='center', va='bottom')
plt.ylim(-1.25, +1.25)
for x, y in zip(X, -Y2):
    plt.text(x, y-0.1, "%.2f" % y, horizontalalignment='center',
             verticalalignment='bottom')
plt.xticks([]), plt.yticks([])
plt.show()


# %%
# Contur plots
def f(x, y):
    """Ploynomial function."""
    return (1-x/2+x**5+y**3)*np.exp(-x**2-y**2)


n = 256
x = np.linspace(-3, 3, n)
y = np.linspace(-3, 3, n)
X, Y = np.meshgrid(x, y)
plt.figure()
plt.axes([0.025, 0.025, 0.95, 0.95])
plt.contourf(X, Y, f(X, Y), 8, alpha=.75, cmap='hot')
C = plt.contour(X, Y, f(X, Y), 8, colors='black', linewidth=.5)
plt.clabel(C, inline=True, fontsize=8)
plt.xticks([]), plt.yticks([])
plt.show()

# %%
# imshow
n = 10
x = np.linspace(-3, 3, 4*n)
y = np.linspace(-3, 3, 3*n)
X, Y = np.meshgrid(x, y)
plt.axes([0.025, 0.025, 0.95, 0.95])
plt.imshow(f(X, Y), cmap=matplotlib.cm.bone)
plt.xticks([]), plt.yticks([])
cax = plt.axes([0.95, 0.02, 0.04, 0.95])
plt.colorbar(cax=cax)
plt.show()

# %%
# pie plot
n = 20
z = np.zeros(n)
z[0] = 0.06
Z = np.random.uniform(0, 1, n)
color = np.linspace(0.0, 1.0, n, endpoint=False).astype('str')
ax = plt.gca()
ax.set_aspect('equal')
plt.axes([0.025, 0.025, 0.95, 0.95])
plt.pie(Z, radius=1.5, shadow=False, colors=color, explode=z)
plt.show()

# %%
# multi subplots
plt.subplot(211)
plt.xticks([]), plt.yticks([])
plt.subplot(234)
plt.xticks([]), plt.yticks([])
plt.subplot(235)
plt.xticks([]), plt.yticks([])
plt.subplot(236)
plt.xticks([]), plt.yticks([])
plt.show()

# %%
# 3D plot
fig = plt.figure()
ax = Axes3D(fig)
X = np.arange(-4, 4, 0.25)
Y = np.arange(-4, 4, 0.25)
X, Y = np.meshgrid(X, Y)
R = np.sqrt(X**2 + Y**2)
Z = np.sin(R)
ax.set_zlim(-2, 2)
ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap='hot')
ax.contourf(X, Y, Z, offset=-2, cmap='hot')
plt.show()

# %%
# grid
ax = plt.gca()
ax.set_xlim(0, 4)
ax.set_ylim(0, 3)
minor = MultipleLocator(0.1)
major = MultipleLocator(1.0)
ax.yaxis.set_minor_locator(minor)
ax.xaxis.set_minor_locator(minor)
ax.yaxis.set_major_locator(major)
ax.xaxis.set_major_locator(major)
ax.grid(which='major', axis='x', linewidth=0.75, linestyle='-', color='0.75')
ax.grid(which='minor', axis='x', linewidth=0.25, linestyle='-', color='0.75')
ax.grid(which='major', axis='y', linewidth=0.75, linestyle='-', color='0.75')
ax.grid(which='minor', axis='y', linewidth=0.25, linestyle='-', color='0.75')
ax.set_xticklabels([])
ax.set_yticklabels([])
plt.show()
