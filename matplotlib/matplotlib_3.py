"""Matplolib exercise 3."""
# %%
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

# %%
# An empty figure with no axes
fig = plt.figure()
fig.suptitle('No axes on this figure')

# %%
# A figure with 2*2 geid axes
fig, ax = plt.subplots(2, 2)

# %%
# Types of inputs to plotting functions
a = pd.DataFrame(np.random.rand(4, 5), columns=list('abcde'))
a_asndarray = a.values

# %%
# coding styles
# style 1
x = np.arange(0, 10, 0.2)
y = np.sin(x)
fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(x, y)
plt.show()


# %%
# style 2
def my_plotter(ax, data1, data2, param_dict):
    """Plotter function."""
    out = ax.plot(data1, data2, **param_dict)
    return out


data1, data2, data3, data4 = np.random.randn(4, 100)
fig, ax = plt.subplots(1, 1)
my_plotter(ax, data1, data2, {'marker': 'x'})
# if you wanted to have 2 sub_plot
fig, (ax1, ax2) = plt.subplots(1, 2)
my_plotter(ax1, data1, data2, {'color': 'r', 'marker': 'x'})
my_plotter(ax2, data3, data4, {'color': 'g', 'marker': 'o'})
plt.show()
