"""Matplotlib practice stage 1."""
# %%
import matplotlib .pyplot as plt
import numpy as np
from matplotlib.ticker import NullFormatter


# %%
plt.plot([1, 2, 3, 4])
plt.ylabel('some numbers')
plt.show()

# %%
plt.plot([1, 2, 3, 4], [8, 6, 9, 6])

# %%
# formatting the style of your plot
plt.plot([1, 8, 9, 4], [8, 8, 6, 9], 'r:')
plt.axis([0, 6, 4, 12])

# %%
t = np.arange(0, 5, 0.2)
plt.plot(t, t, 'r--', t, t**2, 'bs', t, t**3, 'g^')

# %%
# plotting with keyword strings
data = {"a": np.arange(50),
        "b": np.random.randn(50),
        "c": np.random.randint(1, 50, 50),
        "d": np.random.randn(50)*100}

plt.scatter(x='a', y='b', c='c', s='d', data=data)
plt.xlabel('x_value')
plt.ylabel('y_value')

# %%
# plot with categorical variables
name = ['group_a', 'group_b', 'group_c']
values = [45, 81, 60]
plt.figure(figsize=(10, 5))
plt.subplot(131)
plt.bar(name, values)
plt.subplot(132)
plt.scatter(name, values)
plt.subplot(133)
plt.plot(name, values)

# %%
# controling line properties
x1 = np.linspace(0, 50, 50)
x2 = x1 + 2
y1 = x1/2 + 10
y2 = x2
line1, line2 = plt.plot(x1, y1, x2, y2)
plt.setp(line1, 'color', 'k', 'linewidth', 2.0)
plt.setp(line2, 'linestyle', ':', 'color', 'r', 'linewidth', 2.0)

# %%
# to get a list of settable properties
lines = plt.plot([1, 2, 3])
plt.setp(lines)


# %%
# working with multiple figures and axes
def f(t):
    """Polynomial function."""
    return np.exp(-t) * np.cos(2*np.pi*t)


t1 = np.arange(0, 5, 0.1)
t2 = np.arange(0, 5, 0.02)
plt.subplot(211)
plt.plot(t1, f(t1), 'bo', t2, f(t2), 'k')

plt.subplot(212)
plt.plot(t2, np.sin(t2), 'r:')

# %%
# plt.figure()
plt.figure(1)
plt.subplot(311)
plt.plot(t1, f(t1), 'b^')
plt.subplot(313)
plt.plot(t2, f(t2), 'rs')

plt.figure(2)
plt.plot(t2, np.cos(t2), 'k:')

plt.figure(1)
plt.subplot(312)
plt.plot(t2, np.sin(t2), 'g--')

# %%
# working with text
mu, sigma = 100, 15
x = mu + sigma * np.random.randn(10000)

# the histogram of the data
n, bins, patches = plt.hist(x, 50, density=1, alpha=0.75, facecolor='g')
plt.xlabel('Smarts')
plt.ylabel('Pernbabuility')
plt.title('Histogram of IQ')
plt.text(60, 0.025, r'$\mu=100,\ \sigma=15$')
plt.axis([40, 160, 0, 0.03])
plt.grid(True)

# %%
# Annotating text
ax = plt.subplot(111)
t = np.arange(0, 5, 0.01)
s = np.cos(2*np.pi*t)
line, = plt.plot(t, s, lw=2)
(plt.annotate('local max', xy=(2, 1), xytext=(3, 1.5),
              arrowprops=dict(facecolor='green', shrink=0.05)))
plt.ylim(-2, 2)
plt.show()

# %%
# Fixing random state for reproductionality
np.random.seed(52)
y = np.random.normal(loc=0.5, scale=0.4, size=1000)
t = y[(y > 0) & (y < 1)]
y.sort()
x = np.arange(len(y))

# plot with various axes scales
# linear
plt.subplot(221)
plt.plot(x, y)
plt.yscale('linear')
plt.title('linear')
plt.grid(True)

# log
plt.subplot(222)
plt.plot(x, y)
plt.yscale('log')
plt.title('log')
plt.grid(True)

# symmetric log
plt.subplot(223)
plt.plot(x, y-y.mean())
plt.yscale('symlog', linthreshy=0.01)
plt.title('symlog')
plt.grid(True)

# logit
plt.subplot(224)
plt.plot(x, y)
plt.yscale('logit')
plt.title('logit')
plt.grid(True)
plt.gca().yaxis.set_minor_formatter(NullFormatter())
plt.subplots_adjust(top=0.92, bottom=0.08, left=0.10, right=0.95,
                    hspace=0.25, wspace=0.35)
