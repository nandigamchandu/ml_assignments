"""Matplotlib practice stage 2."""

# %%
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
plt.style.use('ggplot')
data = np.random.randn(50)

# %%
# To list all avilable styles use
print(plt.style.available)

# %%
# Defining your own style
# default location of my styles is ~/.config/matplotlib
# we can get configdir by calling get_configdir() method on matplotlib
mpl.get_configdir()

# %%
name = ['g_%d' % i for i in np.arange(10)]
value = np.linspace(0, 50, 10)**2
plt.title('presentation style')
plt.plot(name, value)
plt.show()

# %%
# using my style sheet
# temporary styling
with plt.style.context(('presentation')):
    plt.title('presentation style')
    plt.plot(name, value)
plt.show()

# %%
# combining list of styles
with plt.style.context(('dark_background', 'presentation')):
    plt.title('presentation style')
    plt.plot(name, value)
plt.show()

# %%
# Dynamic rc settings
# matplotlib.rcParams
mpl.rcParams['lines.linewidth'] = 4
mpl.rcParams['lines.color'] = 'r'
plt.plot(name, value)
plt.show()

# %%
# To modify multiple settings
mpl.rcParams('lines', linewidth=4, color='g')
plt.plot(name, value)
plt.show()

# %%
# set to  Default values
mpl.rcdefaults()
plt.plot(name, value)
plt.show()

# %%
# to display current active matplotlibrc file
mpl.matplotlib_fname()
