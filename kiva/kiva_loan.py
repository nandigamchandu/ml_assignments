"""EDA kiva dataset."""

# %%
import pandas as pd
import numpy as np
import re
import matplotlib.pyplot as plt
import seaborn as sns

# %%
# importing dataset
kiva_loan = pd.read_csv('~/data/kiva_dataset/kivadhsv4.csv')
mpi_region = pd.read_csv('~/data/kiva_dataset/dhscluster.csv')

# %%
kiva_loan.shape

# %%
# check for missing values
kiva_miss = kiva_loan.isnull().sum()
kiva_miss = kiva_miss[kiva_miss > 0]
kiva_miss.value_counts()  # Among 101 columns 94 columns miss only one value
# column having high missing values
col_more_miss = kiva_miss.nlargest(1)
kiva_loan[col_more_miss.index].dropna()

# %%
# row having more miss value
row_miss = kiva_loan.T.isnull().sum()
row_miss.value_counts()  # one row has 100 miss values
row_miss.idxmax()
# check why 5123 obdservation has more missing values
kiva_loan.iloc[5123]
# drop observation having high missing values`
kiva_loan = kiva_loan.drop(5123)

# check no columns having miss values
kiva_miss = kiva_loan.isnull().sum()
kiva_miss[kiva_miss > 0]

# %%
# check for duplicate rows
np.any(kiva_loan.duplicated())

# %%
# verify column geocode and geo are same or not
kiva_loan['geocode'].head()
# it has string values
kiva_loan['geocode'] = (kiva_loan['geocode'].apply(
                                  lambda x: re.findall('\(.*\)', x)[0]))
np.all(kiva_loan['geocode'] == kiva_loan['geo'])

# both geocode and geo columns has same values so we can drop one column
kiva_loan = kiva_loan.drop(columns='geocode')

nunique_col = kiva_loan.nunique()
# check column had only one value
one_value_col = nunique_col[nunique_col == 1]
# drop one_value_col
kiva_loan = kiva_loan.drop(columns=one_value_col.index)
nunique_col[nunique_col <= 10]
kiva_loan['DHSYEAR.x'].value_counts()


# %%
# convert string geo to numeric
# covert string geocode_old to numeric
def geo_numeric(x):
    """To numeric."""
    geo = re.findall('(-?\d+\.?\d*)', x)
    return np.float(geo[0]), np.float(geo[1])


kiva_loan['geo'] = kiva_loan['geo'].apply(geo_numeric)
kiva_loan['geocode_old'] = kiva_loan['geocode_old'].dropna().apply(geo_numeric)
lat = kiva_loan['geo'].apply(lambda x: x[0])
long = kiva_loan['geo'].apply(lambda x: x[1])
old_lat = kiva_loan['geocode_old'].dropna().apply(lambda x: x[0])
old_long = kiva_loan['geocode_old'].dropna().apply(lambda x: x[1])
plt.scatter(lat, long, alpha=0.2)
plt.scatter(old_lat, old_long, alpha=0.8)

# %%
# which region people morely applied for loan
region = kiva_loan['country'].value_counts()
sns.barplot(region.index, region.values)

# %%
# reason for appling for loan
reason = kiva_loan['Loan.Theme.Type'].value_counts()
plt.figure(figsize=(10, 5))
plt.xticks(rotation=90)
sns.barplot(reason.index, reason.values)

# %%
# loan applied by urban area vs rural area
u_r = kiva_loan['URBAN_RURA'].value_counts()
sns.barplot(u_r.index, u_r.values)

# %%
# no of loans applied vs year
x = kiva_loan['DHSYEAR.x'].value_counts()
sns.barplot(x.index, x.values)

# %%
x = kiva_loan.groupby('country')['URBAN_RURA'].value_counts()
(sns.barplot(x.index.get_level_values(0), x.values,
             hue=x.index.get_level_values(1)))
